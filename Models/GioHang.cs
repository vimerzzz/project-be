﻿using System;

namespace Project.Models
{
    public class GioHang : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int SoLuong { get; set; }
        public int DonGia { get; set; }
        public int GiamGia { get; set; }
        public Guid UserId { get; set; }
        public Guid? SanPhamId { get; set; }
        public Guid? SanPhamDauGiaId { get; set; }
        public User User { get; set; }
        public SanPham SanPham { get; set; }
        public SanPhamDauGia SanPhamDauGia { get; set; }
    }
}
