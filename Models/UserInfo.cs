﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project.Models
{
    public class UserInfo : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Ho { get; set; }
        public string TenDem { get; set; }
        public string Ten { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string GioiTinh { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime NgaySinh { get; set; }
        public string AnhDaiDien { get; set; }
        public bool CoAnhDaiDien { get; set; }
        public bool CoShop { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
