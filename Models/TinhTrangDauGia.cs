﻿using System;
using System.Collections.Generic;

namespace Project.Models
{
    public class TinhTrangDauGia : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TinhTrang { get; set; }
        public List<SanPhamDauGia> SanPhamDauGia { get; set; }
    }
}
