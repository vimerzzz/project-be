﻿using System;

namespace Project.Models
{
    public class NhanTinUser : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string NoiDung { get; set; }
        public Guid UserNhanId { get; set; }
        public Guid UserGuiId { get; set; }
        public bool DaXem { get; set; }
        public bool IsFile { get; set; }
        public bool IsImage { get; set; }
        public string FileName { get; set; }
        public Guid ChatBoxId { get; set; }
        public User User { get; set; }
    }
}
