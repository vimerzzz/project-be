﻿using System;

namespace Project.Models
{
    public class NhanTinShop : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string NoiDung { get; set; }
        public Guid UserId { get; set; }
        public Guid ShopId { get; set; }
        public Guid NguoiGuiId { get; set; }
        public bool DaXem { get; set; }
        public bool IsFile { get; set; }
        public bool IsImage { get; set; }
        public string FileName { get; set; }
        public Guid ChatBoxId { get; set; }
        public User User { get; set; }
        public Shop Shop { get; set; }
    }
}
