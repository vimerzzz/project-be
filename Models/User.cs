﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Project.Models
{
    public class User : IdentityUser<Guid>, Base
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool HoatDong { get; set; }
        public UserInfo UserInfo { get; set; }
        public List<SoDiaChi> SoDiaChi { get; set; }
        public List<ThongBao> ThongBao { get; set; }
        public Shop Shop { get; set; }
        public List<GioHang> GioHang { get; set; }
        public List<DonHang> DonHang { get; set; }
        public List<NhanTinUser> NhanTinUser { get; set; }
        public List<NhanTinShop> NhanTinShop { get; set; }
        public List<DanhGiaSanPham> DanhGiaSanPham { get; set; }
        public List<DanhGiaShop> DanhGiaShop { get; set; }
        public List<SoTheoDoi> SoTheoDoi { get; set; }
        public List<SanPhamDauGia> SanPhamDauGia { get; set; }
        public List<DanhGiaSanPhamHuuIch> DanhGiaSanPhamHuuIch { get; set; }
        public List<DanhGiaShopHuuIch> DanhGiaShopHuuIch { get; set; }
        public List<TroChuyenDauGia> TroChuyenDauGia { get; set; }
        public List<BaoCaoViPham> BaoCaoViPham { get; set; }
    }
}
