﻿using System;
using System.Collections.Generic;

namespace Project.Models
{
    public class SanPham : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TenSanPham { get; set; }
        public int DonGia { get; set; }
        public int SoLuong { get; set; }
        public string ThongTinKhac { get; set; }
        public int GiamGia { get; set; }
        public int SoLuongDaBan { get; set; }
        public int SoNguoiDanhGia { get; set; }
        public int MotSao { get; set; }
        public int HaiSao { get; set; }
        public int BaSao { get; set; }
        public int BonSao { get; set; }
        public int NamSao { get; set; }
        public float DanhGiaChung { get; set; }
        public float NongDo { get; set; }
        public string XuatXu { get; set; }
        public int Nam { get; set; }
        public string MauSac { get; set; }
        public float DungTich { get; set; }
        public Guid ShopId { get; set; }
        public Guid LoaiSanPhamId { get; set; }
        public Shop Shop { get; set; }
        public LoaiSanPham LoaiSanPham { get; set; }
        public List<HinhAnhSanPham> HinhAnhSanPham { get; set; }
        public List<GioHang> GioHang { get; set; }
        public List<DanhGiaSanPham> DanhGiaSanPham { get; set; }
    }
}
