﻿using System;

namespace Project.Models
{
    public class HinhAnhSanPhamDauGia : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string HinhAnh { get; set; }
        public int ThuTu { get; set; }
        public Guid SanPhamDauGiaId { get; set; }
        public SanPhamDauGia SanPhamDauGia { get; set; }
    }
}
