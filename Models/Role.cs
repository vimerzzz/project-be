﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Project.Models
{
    public class Role : IdentityRole<Guid>, Base
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
