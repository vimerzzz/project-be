﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Models
{
    public class DonHang : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MaDonHang { get; set; }
        public int ThanhTien { get; set; }
        public string TenDayDu { get; set; }
        public string TenDayDuNguoiDat { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public string SoDienThoaiNguoiDat { get; set; }
        public string GhiChu { get; set; }
        public string LyDoHuy { get; set; }
        public bool XuatHoaDon { get; set; }
        public bool DatHo { get; set; }
        public int SoSanPham { get; set; }
        public Guid UserId { get; set; }
        public Guid TinhTrangDonHangId { get; set; }
        public Guid ShopId { get; set; }
        public Shop Shop { get; set; }
        public User User { get; set; }
        public TinhTrangDonHang TinhTrangDonHang { get; set; }
        public List<DonHangSanPham> DonHangSanPham { get; set; }
    }
}
