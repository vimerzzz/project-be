﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Models
{
    public class SanPhamDauGia : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TenSanPham { get; set; }
        public int GiaKhoiDiem { get; set; }
        public int KhoangGiaToiThieu { get; set; }
        public int GiaHienTai { get; set; }
        public string ThongTinKhac { get; set; }
        public float NongDo { get; set; }
        public string XuatXu { get; set; }
        public int Nam { get; set; }
        public string MauSac { get; set; }
        public float DungTich { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ThoiGianBatDau { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ThoiGianKetThuc { get; set; }
        public bool YeuCauKetThuc { get; set; }
        public Guid ShopId { get; set; }
        public Guid LoaiSanPhamId { get; set; }
        public Guid TinhTrangDauGiaId { get; set; }
        public Guid? UserId { get; set; }
        public TinhTrangDauGia TinhTrangDauGia { get; set; }
        public Shop Shop { get; set; }
        public LoaiSanPham LoaiSanPham { get; set; }
        public User User { get; set; }
        public List<HinhAnhSanPhamDauGia> HinhAnhSanPhamDauGia { get; set; }
        public List<TroChuyenDauGia> TroChuyenDauGia { get; set; }
        public List<GioHang> GioHang { get; set; }
    }
}
