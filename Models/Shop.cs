﻿using System;
using System.Collections.Generic;

namespace Project.Models
{
    public class Shop : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TenShop { get; set; }
        public string MoTaShop { get; set; }
        public int SoNguoiTheoDoi { get; set; }
        public string AnhDaiDien { get; set; }
        public bool CoAnhDaiDien { get; set; }
        public int TongSoSanPham { get; set; }
        public int SoNguoiDanhGia { get; set; }
        public int MotSao { get; set; }
        public int HaiSao { get; set; }
        public int BaSao { get; set; }
        public int BonSao { get; set; }
        public int NamSao { get; set; }
        public float DanhGiaChung { get; set; }
        public bool HoatDong { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public List<SanPham> SanPham { get; set; }
        public List<SanPhamDauGia> SanPhamDauGia { get; set; }
        public List<DonHang> DonHang { get; set; }
        public List<NhanTinShop> NhanTinShop { get; set; }
        public List<DanhGiaShop> DanhGiaShop { get; set; }
        public List<SoTheoDoi> SoTheoDoi { get; set; }
        public List<BaoCaoViPham> BaoCaoViPham { get; set; }
    }
}
