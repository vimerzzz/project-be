﻿using System;
using System.Collections.Generic;

namespace Project.Models
{
    public class LoaiSanPham : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TenLoaiSanPham { get; set; }
        public string MoTaLoaiSanPham { get; set; }
        public List<SanPham> SanPham { get; set; }
        public List<SanPhamDauGia> SanPhamDauGia { get; set; }
    }
}
