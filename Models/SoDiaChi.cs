﻿using System;

namespace Project.Models
{
    public class SoDiaChi : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string DiaChi { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
