﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project.Models
{
    public class ThongBao : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TieuDeThongBao { get; set; }
        public string NoiDungThongBao { get; set; }
        public bool DaDoc { get; set; }
        public bool DaXem { get; set; }
        public bool CoLichHen { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ThoiGianHen { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
