﻿using System;

namespace Project.Models
{
    public class BaoCaoViPham : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string LyDoBaoCao { get; set; }
        public string NoiGuiBaoCao { get; set; }
        public Guid NguoiBaoCaoId { get; set; }
        public Guid? UserId { get; set; }
        public Guid? ShopId { get; set; }
        public bool DaXem { get; set; }
        public User User { get; set; }
        public Shop Shop { get; set; }
    }
}
