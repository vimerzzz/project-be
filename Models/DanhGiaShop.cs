﻿using System;
using System.Collections.Generic;

namespace Project.Models
{
    public class DanhGiaShop : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int DanhGia { get; set; }
        public string NhanXet { get; set; }
        public int HuuIch { get; set; }
        public Guid UserId { get; set; }
        public Guid ShopId { get; set; }
        public User User { get; set; }
        public Shop Shop { get; set; }
        public List<DanhGiaShopHuuIch> DanhGiaShopHuuIch { get; set; }
    }
}
