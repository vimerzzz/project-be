﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Models
{
    public class DanhGiaSanPham : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int DanhGia { get; set; }
        public string NhanXet { get; set; }
        public int HuuIch { get; set; }
        public Guid UserId { get; set; }
        public Guid SanPhamId { get; set; }
        public User User { get; set; }
        public SanPham SanPham { get; set; }
        public List<DanhGiaSanPhamHuuIch> DanhGiaSanPhamHuuIch { get; set; }
    }
}
