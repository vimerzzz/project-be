﻿using System;

namespace Project.Models
{
    public class TroChuyenDauGia : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string NoiDung { get; set; }
        public bool IsSystem { get; set; }
        public bool IsShopOwner { get; set; }
        public Guid? UserId { get; set; }
        public Guid SanPhamDauGiaId { get; set; }
        public User User { get; set; }
        public SanPhamDauGia SanPhamDauGia { get; set; }
    }
}
