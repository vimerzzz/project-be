﻿using System;

namespace Project.Models
{
    public class DanhGiaSanPhamHuuIch : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid DanhGiaSanPhamId { get; set; }
        public Guid UserId { get; set; }
        public DanhGiaSanPham DanhGiaSanPham { get; set; }
        public User User { get; set; }
    }
}
