﻿using System;

namespace Project.Models
{
    public class SoTheoDoi : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid UserId { get; set; }
        public Guid ShopId { get; set; }
        public User User { get; set; }
        public Shop Shop { get; set; }
    }
}
