﻿using System;

namespace Project.Models
{
    public class HinhAnhSanPham : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string HinhAnh { get; set; }
        public int ThuTu { get; set; }
        public Guid SanPhamId { get; set; }
        public SanPham SanPham { get; set; }
    }
}
