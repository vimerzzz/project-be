﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Models
{
    public class DonHangSanPham : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TenSanPham { get; set; }
        public string HinhAnhSanPham { get; set; }
        public int DonGia { get; set; }
        public int SoLuong { get; set; }
        public int GiamGia { get; set; }
        public bool SanPhamDauGia { get; set; }
        public Guid SanPhamGocId { get; set; }
        public Guid DonHangId { get; set; }
        public DonHang DonHang { get; set; }
    }
}
