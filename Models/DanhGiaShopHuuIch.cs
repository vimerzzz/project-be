﻿using System;

namespace Project.Models
{
    public class DanhGiaShopHuuIch : Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid DanhGiaShopId { get; set; }
        public Guid UserId { get; set; }
        public DanhGiaShop DanhGiaShop { get; set; }
        public User User { get; set; }
    }
}
