﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Project.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Project.Utilities.Converters;

namespace Project.DataContext
{
    public class ApplicationDBContext : IdentityDbContext<User, Role, Guid>
    {
        private readonly IntegerToGuid converter = new IntegerToGuid();
        public DbSet<UserInfo> UserInfos { get; set; }
        public DbSet<SoDiaChi> SoDiaChis { get; set; }
        public DbSet<ThongBao> ThongBaos { get; set; }
        public DbSet<HinhAnhSanPham> HinhAnhSanPhams { get; set; }
        public DbSet<LoaiSanPham> LoaiSanPhams { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<SanPham> SanPhams { get; set; }
        public DbSet<GioHang> GioHangs { get; set; }
        public DbSet<DonHang> DonHangs { get; set; }
        public DbSet<TinhTrangDonHang> TinhTrangDonHangs { get; set; }
        public DbSet<DonHangSanPham> DonHangSanPhams { get; set; }
        public DbSet<NhanTinUser> NhanTinUsers { get; set; }
        public DbSet<NhanTinShop> NhanTinShops { get; set; }
        public DbSet<DanhGiaSanPham> DanhGiaSanPhams { get; set; }
        public DbSet<DanhGiaShop> DanhGiaShops { get; set; }
        public DbSet<SoTheoDoi> SoTheoDois { get; set; }
        public DbSet<SanPhamDauGia> SanPhamDauGias { get; set; }
        public DbSet<TinhTrangDauGia> TinhTrangDauGias { get; set; }
        public DbSet<HinhAnhSanPhamDauGia> HinhAnhSanPhamDauGias { get; set; }
        public DbSet<DanhGiaSanPhamHuuIch> DanhGiaSanPhamHuuIchs { get; set; }
        public DbSet<DanhGiaShopHuuIch> DanhGiaShopHuuIchs { get; set; }
        public DbSet<TroChuyenDauGia> TroChuyenDauGias { get; set; }
        public DbSet<BaoCaoViPham> BaoCaoViPhams { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserInfo>()
                .HasOne(s => s.User)
                .WithOne(to => to.UserInfo)
                .HasForeignKey<UserInfo>(to => to.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<SoDiaChi>()
                .HasOne(s => s.User)
                .WithMany(to => to.SoDiaChi)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ThongBao>()
                .HasOne(s => s.User)
                .WithMany(to => to.ThongBao)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<SanPham>()
                .HasOne(s => s.LoaiSanPham)
                .WithMany(to => to.SanPham)
                .HasForeignKey(to => to.LoaiSanPhamId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<SanPham>()
                .HasOne(s => s.Shop)
                .WithMany(to => to.SanPham)
                .HasForeignKey(to => to.ShopId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<SanPhamDauGia>()
                .HasOne(s => s.LoaiSanPham)
                .WithMany(to => to.SanPhamDauGia)
                .HasForeignKey(to => to.LoaiSanPhamId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<SanPhamDauGia>()
                .HasOne(s => s.TinhTrangDauGia)
                .WithMany(to => to.SanPhamDauGia)
                .HasForeignKey(to => to.TinhTrangDauGiaId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<SanPhamDauGia>()
                .HasOne(s => s.User)
                .WithMany(to => to.SanPhamDauGia)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<SanPhamDauGia>()
                .HasOne(s => s.Shop)
                .WithMany(to => to.SanPhamDauGia)
                .HasForeignKey(to => to.ShopId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<HinhAnhSanPham>()
                .HasOne(s => s.SanPham)
                .WithMany(to => to.HinhAnhSanPham)
                .HasForeignKey(to => to.SanPhamId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<HinhAnhSanPhamDauGia>()
                .HasOne(s => s.SanPhamDauGia)
                .WithMany(to => to.HinhAnhSanPhamDauGia)
                .HasForeignKey(to => to.SanPhamDauGiaId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Shop>()
                .HasOne(s => s.User)
                .WithOne(to => to.Shop)
                .HasForeignKey<Shop>(to => to.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<GioHang>()
                .HasOne(s => s.SanPham)
                .WithMany(to => to.GioHang)
                .HasForeignKey(to => to.SanPhamId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<GioHang>()
                .HasOne(s => s.SanPhamDauGia)
                .WithMany(to => to.GioHang)
                .HasForeignKey(to => to.SanPhamDauGiaId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<GioHang>()
                .HasOne(s => s.User)
                .WithMany(to => to.GioHang)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DonHang>()
                .HasOne(s => s.User)
                .WithMany(to => to.DonHang)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DonHang>()
                .HasOne(s => s.Shop)
                .WithMany(to => to.DonHang)
                .HasForeignKey(to => to.ShopId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<DonHang>()
                .HasOne(s => s.TinhTrangDonHang)
                .WithMany(to => to.DonHang)
                .HasForeignKey(to => to.TinhTrangDonHangId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<DonHangSanPham>()
                .HasOne(s => s.DonHang)
                .WithMany(to => to.DonHangSanPham)
                .HasForeignKey(to => to.DonHangId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<NhanTinUser>()
                .HasOne(s => s.User)
                .WithMany(to => to.NhanTinUser)
                .HasForeignKey(to => to.UserGuiId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<NhanTinUser>()
                .HasOne(s => s.User)
                .WithMany(to => to.NhanTinUser)
                .HasForeignKey(to => to.UserNhanId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<NhanTinShop>()
                .HasOne(s => s.User)
                .WithMany(to => to.NhanTinShop)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<NhanTinShop>()
                .HasOne(s => s.Shop)
                .WithMany(to => to.NhanTinShop)
                .HasForeignKey(to => to.ShopId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DanhGiaSanPham>()
                .HasOne(s => s.User)
                .WithMany(to => to.DanhGiaSanPham)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<DanhGiaSanPham>()
                .HasOne(s => s.SanPham)
                .WithMany(to => to.DanhGiaSanPham)
                .HasForeignKey(to => to.SanPhamId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DanhGiaShop>()
                .HasOne(s => s.User)
                .WithMany(to => to.DanhGiaShop)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<DanhGiaShop>()
                .HasOne(s => s.Shop)
                .WithMany(to => to.DanhGiaShop)
                .HasForeignKey(to => to.ShopId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<SoTheoDoi>()
                .HasOne(s => s.User)
                .WithMany(to => to.SoTheoDoi)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<SoTheoDoi>()
                .HasOne(s => s.Shop)
                .WithMany(to => to.SoTheoDoi)
                .HasForeignKey(to => to.ShopId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<DanhGiaSanPhamHuuIch>()
                .HasOne(s => s.DanhGiaSanPham)
                .WithMany(to => to.DanhGiaSanPhamHuuIch)
                .HasForeignKey(to => to.DanhGiaSanPhamId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DanhGiaSanPhamHuuIch>()
                .HasOne(s => s.User)
                .WithMany(to => to.DanhGiaSanPhamHuuIch)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<DanhGiaShopHuuIch>()
                .HasOne(s => s.DanhGiaShop)
                .WithMany(to => to.DanhGiaShopHuuIch)
                .HasForeignKey(to => to.DanhGiaShopId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DanhGiaShopHuuIch>()
                .HasOne(s => s.User)
                .WithMany(to => to.DanhGiaShopHuuIch)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<TroChuyenDauGia>()
                .HasOne(s => s.User)
                .WithMany(to => to.TroChuyenDauGia)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<TroChuyenDauGia>()
                .HasOne(s => s.SanPhamDauGia)
                .WithMany(to => to.TroChuyenDauGia)
                .HasForeignKey(to => to.SanPhamDauGiaId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<BaoCaoViPham>()
                .HasOne(s => s.User)
                .WithMany(to => to.BaoCaoViPham)
                .HasForeignKey(to => to.NguoiBaoCaoId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<BaoCaoViPham>()
                .HasOne(s => s.User)
                .WithMany(to => to.BaoCaoViPham)
                .HasForeignKey(to => to.UserId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<BaoCaoViPham>()
                .HasOne(s => s.Shop)
                .WithMany(to => to.BaoCaoViPham)
                .HasForeignKey(to => to.ShopId)
                .OnDelete(DeleteBehavior.NoAction);

            SeedRoles(modelBuilder);
            SeedUsers(modelBuilder);
            SeedUsersRoles(modelBuilder);
            SeedUserInfos(modelBuilder);
            SeedTinhTrangDonHangs(modelBuilder);
            SeedThongBaos(modelBuilder);
            SeedTinhTrangDauGias(modelBuilder);
            SeedLoaiSanPhams(modelBuilder);
        }

        //Data seed
        private void SeedRoles(ModelBuilder modelBuilder)
        {
            Role newAdminRole = new Role
            {
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Id = converter.IntToGuid(1),
                Name = "Admin"
            };
            Role newCustomerRole = new Role
            {
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Id = converter.IntToGuid(2),
                Name = "User"
            };
            Role newDeliverRole = new Role
            {
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Id = converter.IntToGuid(3),
                Name = "Deliver"
            };
            Role newExpertRole = new Role
            {
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Id = converter.IntToGuid(4),
                Name = "Expert"
            };
            modelBuilder.Entity<Role>().HasData(newAdminRole);
            modelBuilder.Entity<Role>().HasData(newCustomerRole);
            // modelBuilder.Entity<Role>().HasData(newDeliverRole);
            modelBuilder.Entity<Role>().HasData(newExpertRole);
        }
        private void SeedUsers(ModelBuilder modelBuilder)
        {
            User newUser = new User
            {
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Id = converter.IntToGuid(1),
                UserName = "admin",
                Email = "thai.bq183981@sis.hust.edu.vn",
                HoatDong = true,
                SecurityStamp = Guid.NewGuid().ToString(),
                EmailConfirmed = true
            };
            PasswordHasher<User> password = new PasswordHasher<User>();
            newUser.PasswordHash = password.HashPassword(newUser, "admin");
            modelBuilder.Entity<User>().HasData(newUser);
        }
        private void SeedUsersRoles(ModelBuilder modelBuilder)
        {
            IdentityUserRole<Guid> newUserRoles = new IdentityUserRole<Guid>
            {
                UserId = converter.IntToGuid(1),
                RoleId = converter.IntToGuid(1)
            };
            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(newUserRoles);
        }
        private void SeedUserInfos(ModelBuilder modelBuilder)
        {
            UserInfo newUserInfo = new UserInfo
            {
                Id = converter.IntToGuid(1),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Email = "thai.bq183981@sis.hust.edu.vn",
                NgaySinh = DateTime.Now,
                UserId = converter.IntToGuid(1),
                AnhDaiDien = "Statics\\Images\\Users\\blank_avatar.png",
                CoAnhDaiDien = false
            };
            modelBuilder.Entity<UserInfo>().HasData(newUserInfo);
        }
        private void SeedTinhTrangDonHangs(ModelBuilder modelBuilder)
        {
            TinhTrangDonHang choXacNhan = new TinhTrangDonHang
            {
                Id = converter.IntToGuid(1),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Chờ xác nhận"
            };
            TinhTrangDonHang daXacNhan = new TinhTrangDonHang
            {
                Id = converter.IntToGuid(2),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đã xác nhận"
            };
            TinhTrangDonHang dangGiao = new TinhTrangDonHang
            {
                Id = converter.IntToGuid(3),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đang giao"
            };
            TinhTrangDonHang daGiao = new TinhTrangDonHang
            {
                Id = converter.IntToGuid(4),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Giao thành công"
            };
            TinhTrangDonHang daHuy = new TinhTrangDonHang
            {
                Id = converter.IntToGuid(5),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đã hủy"
            };
            TinhTrangDonHang daHuyViGiaoKhongThanhCong = new TinhTrangDonHang
            {
                Id = converter.IntToGuid(6),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đã hủy vì giao không thành công"
            };
            modelBuilder.Entity<TinhTrangDonHang>().HasData(choXacNhan);
            modelBuilder.Entity<TinhTrangDonHang>().HasData(daXacNhan);
            modelBuilder.Entity<TinhTrangDonHang>().HasData(dangGiao);
            modelBuilder.Entity<TinhTrangDonHang>().HasData(daGiao);
            modelBuilder.Entity<TinhTrangDonHang>().HasData(daHuy);
            modelBuilder.Entity<TinhTrangDonHang>().HasData(daHuyViGiaoKhongThanhCong);
        }
        private void SeedThongBaos(ModelBuilder modelBuilder)
        {
            ThongBao newThongBao = new ThongBao
            {
                Id = converter.IntToGuid(1),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TieuDeThongBao = "Cần xác thực tài khoản",
                NoiDungThongBao = "Email của bạn chưa được xác thực, hãy đến phần <a name=\"notify-link\" href=\"/information/setting\">Cài đặt tài khoản</a> để xác thực email của bạn",
                UserId = converter.IntToGuid(1)
            };
            modelBuilder.Entity<ThongBao>().HasData(newThongBao);
        }

        private void SeedTinhTrangDauGias(ModelBuilder modelBuilder)
        {
            TinhTrangDauGia choKiemDuyet = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(1),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Chờ kiểm định"
            };
            TinhTrangDauGia khongKiemDuyet = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(2),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Không kiểm định"
            };
            TinhTrangDauGia quaHanKiemDuyet = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(3),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Quá hạn kiểm định"
            };
            TinhTrangDauGia choDauGia = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(4),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Chờ đấu giá"
            };
            TinhTrangDauGia dangDauGia = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(5),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đang đấu giá"
            };
            TinhTrangDauGia dauGiaThanhCong = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(6),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đấu giá thành công - Chờ thanh toán"
            };
            TinhTrangDauGia dauGiaKhongThanhCong = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(7),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đấu giá thất bại"
            };
            TinhTrangDauGia huyDauGia = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(8),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Hủy đấu giá"
            };
            TinhTrangDauGia daThanhToan = new TinhTrangDauGia
            {
                Id = converter.IntToGuid(9),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TinhTrang = "Đấu giá thành công - Đã thanh toán"
            };
            modelBuilder.Entity<TinhTrangDauGia>().HasData(choKiemDuyet);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(khongKiemDuyet);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(quaHanKiemDuyet);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(choDauGia);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(dangDauGia);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(dauGiaThanhCong);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(dauGiaKhongThanhCong);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(huyDauGia);
            modelBuilder.Entity<TinhTrangDauGia>().HasData(daThanhToan);
        }

        private void SeedLoaiSanPhams(ModelBuilder modelBuilder)
        {
            LoaiSanPham ruouKhac = new LoaiSanPham
            {
                Id = converter.IntToGuid(1),
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TenLoaiSanPham = "Rượu khác"
            };
            modelBuilder.Entity<LoaiSanPham>().HasData(ruouKhac);
        }

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }

        public ApplicationDBContext()
        {

        }
    }
}
