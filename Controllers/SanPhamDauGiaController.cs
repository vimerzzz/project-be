﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SanPhamDauGiaController : ControllerBase
    {
        private readonly ISanPhamDauGiaService _service;
        private readonly IValidateService _validateService;

        public SanPhamDauGiaController(ISanPhamDauGiaService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public ResponseWithPaginationView GetAllSanPhamDauGiaByShopId([FromQuery] SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            var res = _service.GetAllSanPhamDauGiaByShopId(sanPhamDauGiaParamView);
            return res;
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public SanPhamDauGiaView GetSanPhamDauGiaById([FromQuery] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _service.GetSanPhamDauGiaById(sanPhamDauGiaView);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateSanPhamDauGia([FromBody] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamDauGiaView.UserId, token);
            if (validate)
            {
                var res = _service.CreateSanPhamDauGia(sanPhamDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UpdateSanPhamDauGia([FromBody] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamDauGiaView.UserId, token);
            if (validate)
            {
                var res = _service.UpdateSanPhamDauGia(sanPhamDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UpdateTinhHinhDauGia([FromBody] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamDauGiaView.UserId, token);
            if (validate)
            {
                var res = await _service.UpdateTinhHinhDauGia(sanPhamDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        [Authorize(Policy = "Expert")]
        public async Task<ResponsePostView> UpdateTinhTrangDauGia([FromBody] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = await _service.UpdateTinhTrangDauGia(sanPhamDauGiaView);
            return res;
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CancelSanPhamDauGia([FromBody] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamDauGiaView.UserId, token);
            if (validate)
            {
                var res = _service.CancelSanPhamDauGia(sanPhamDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> YeuCauKetThucDauGia([FromBody] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamDauGiaView.UserId, token);
            if (validate)
            {
                var res = await _service.YeuCauKetThucDauGia(sanPhamDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> DeleteSanPhamDauGia([FromQuery] SanPhamDauGiaView sanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamDauGiaView.UserId, token);
            if (validate)
            {
                var res = _service.DeleteSanPhamDauGia(sanPhamDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetNewestAllSanPhamDauGiaWithHinhAnhByShopId([FromQuery] SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            var res = _service.GetNewestAllSanPhamDauGiaWithHinhAnhByShopId(sanPhamDauGiaParamView);
            return res;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllSanPhamDauGia([FromQuery] SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            var res = _service.GetAllSanPhamDauGia(sanPhamDauGiaParamView);
            return res;
        }

        [HttpGet]
        public SanPhamDauGiaWithHinhAnhView GetSanPhamDauGiaWithHinhAnhByName([FromQuery] string tenShop, string tenSanPham)
        {
            var res = _service.GetSanPhamDauGiaWithHinhAnhByName(tenShop, tenSanPham);
            return res;
        }

        [HttpGet]
        public List<TinhTrangDauGiaView> GetAllTinhTrangDauGia()
        {
            var res = _service.GetAllTinhTrangDauGia();
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponseNoiDungPostView> UploadImageGioiThieuSanPham([FromQuery] Guid userId, Guid sanPhamDauGiaId)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var param = new SanPhamDauGiaView
                {
                    Id = sanPhamDauGiaId
                };
                var sanPham = _service.GetSanPhamDauGiaById(param);
                if (sanPham != null)
                {
                    var file = Request.Form.Files[0];
                    var folderName = Path.Combine("Statics", "Images", "Auctions", sanPham.ShopId.ToString(), sanPhamDauGiaId.ToString());
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    Directory.CreateDirectory(pathToSave);
                    if (file.Length > 0)
                    {
                        var fileName = file.FileName;
                        var name = "";
                        for (int i = 0; i < fileName.Split('.').Count(); i++)
                        {
                            if (i < fileName.Split('.').Count() - 1)
                            {
                                name += fileName.Split('.')[i];
                            }
                        }
                        var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                        var fullPath = Path.Combine(pathToSave, fileRename);
                        //var dbPath = Path.Combine(folderName, fileRename);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        return new ResponseNoiDungPostView(MessageConstants.CREATE_SUCCESS, 200, fileRename, null);
                    }
                    else
                    {
                        return new ResponseNoiDungPostView(MessageConstants.FILE_NOT_FOUND, 404, null, null);
                    }
                }
                else
                {
                    return new ResponseNoiDungPostView(MessageConstants.NOT_FOUND, 404, null, null);
                }
            }
            else
            {
                return new ResponseNoiDungPostView(MessageConstants.VALIDATE_FAILED, 400, null, null);
            }
        }
    }
}
