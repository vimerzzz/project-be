﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Policy = "Customer")]
    public class SoDiaChiController : ControllerBase
    {
        private readonly ISoDiaChiService _service;
        private readonly IValidateService _validateService;

        public SoDiaChiController(ISoDiaChiService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllDiaChi([FromQuery] SoDiaChiParamView soDiaChiParamView)
        {
            var res = _service.GetAllDiaChi(soDiaChiParamView);
            return res;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateDiaChi([FromBody] SoDiaChiView soDiaChiView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(soDiaChiView.UserId, token);
            if (validate)
            {
                var res = _service.CreateDiaChi(soDiaChiView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        public async Task<ResponsePostView> UpdateDiaChi([FromBody] SoDiaChiView soDiaChiView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(soDiaChiView.UserId, token);
            if (validate)
            {
                var res = _service.UpdateDiaChi(soDiaChiView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        public async Task<ResponsePostView> DeleteDiaChi([FromQuery] SoDiaChiView soDiaChiView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(soDiaChiView.UserId, token);
            if (validate)
            {
                var res = _service.DeleteDiaChi(soDiaChiView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }
    }
}
