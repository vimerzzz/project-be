﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project.Services;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Policy = "Admin")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _service;

        public RoleController(IRoleService service)
        {
            _service = service;
        }

        [HttpGet]
        public List<RoleView> GetAllRole()
        {
            var res = _service.GetAllRole();
            return res;
        }

        [HttpPost]
        public ResponsePostView CreateRole([FromBody] RoleView roleView)
        {
            var res = _service.CreateRole(roleView);
            return res;
        }

        [HttpPut]
        public ResponsePostView UpdateRole([FromBody] RoleView roleView)
        {
            var res = _service.UpdateRole(roleView);
            return res;
        }

        [HttpDelete]
        public ResponsePostView DeleteRole([FromQuery] Guid id)
        {
            var res = _service.DeleteRole(id);
            return res;
        }
    }
}
