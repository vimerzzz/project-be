﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Policy = "Customer")]
    public class DonHangController : ControllerBase
    {
        private readonly IDonHangService _service;
        private readonly IValidateService _validateService;

        public DonHangController(IDonHangService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllDonHang([FromQuery] DonHangParamView donHangParamView)
        {
            var res = _service.GetAllDonHang(donHangParamView);
            return res;
        }

        [HttpGet]
        public DonHangView GetDonHangByMaDonHang([FromQuery] DonHangView donHangView)
        {
            var res = _service.GetDonHangByMaDonHang(donHangView);
            return res;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateDonHang([FromBody] DonHangView donHangView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(donHangView.UserId, token);
            if (validate)
            {
                var res = await _service.CreateDonHang(donHangView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        public async Task<ResponsePostView> UpdateDonHang([FromBody] DonHangView donHangView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(donHangView.UserId, token);
            if (validate)
            {
                var res = _service.UpdateDonHang(donHangView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        public async Task<ResponsePostView> UpdateTinhTrangDonHang([FromQuery] Guid userId, [FromBody] DonHangView donHangView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = await _service.UpdateTinhTrangDonHang(donHangView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }
    }
}
