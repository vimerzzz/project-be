﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TroChuyenDauGiaController : ControllerBase
    {
        private readonly ITroChuyenDauGiaService _service;
        private readonly IValidateService _validateService;

        public TroChuyenDauGiaController(ITroChuyenDauGiaService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateTroChuyenDauGia([FromQuery] Guid userId, [FromBody] TroChuyenDauGiaView troChuyenDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = await _service.CreateTroChuyenDauGia(troChuyenDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllTroChuyenDauGia([FromQuery] TroChuyenDauGiaParamView troChuyenDauGiaParamView)
        {
            var res = _service.GetAllTroChuyenDauGia(troChuyenDauGiaParamView);
            return res;
        }
    }
}
