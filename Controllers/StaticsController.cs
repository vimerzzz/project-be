﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project.Services;
using System;
using System.IO;
using System.Linq;

namespace Project.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StaticsController : ControllerBase
    {
        [HttpGet("Images/Users/{userId}/{anhDaiDien}")]
        public object GetUserImage(string userId, string anhDaiDien)
        {
            var folderName = Path.Combine("Statics", "Images", "Users");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, userId, anhDaiDien);
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, "image/png");
            }
            return null;
        }

        [HttpGet("Images/Users/{anhDaiDien}")]
        public object GetUserImage(string anhDaiDien)
        {
            var folderName = Path.Combine("Statics", "Images", "Users");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, anhDaiDien);
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, "image/png");
            }
            return null;
        }

        [HttpGet("Images/Shops/{shopId}/{anhDaiDien}")]
        public object GetShopImage(string shopId, string anhDaiDien)
        {
            var folderName = Path.Combine("Statics", "Images", "Shops");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, shopId, anhDaiDien);
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, "image/png");
            }
            return null;
        }

        [HttpGet("Images/Shops/{anhDaiDien}")]
        public object GetShopImage(string anhDaiDien)
        {
            var folderName = Path.Combine("Statics", "Images", "Shops");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, anhDaiDien);
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, "image/png");
            }
            return null;
        }

        [HttpGet("Images/Items/{shopId}/{itemId}/{anhDaiDien}")]
        public object GetItemImage(string shopId, string itemId, string anhDaiDien)
        {
            var folderName = Path.Combine("Statics", "Images", "Items");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, shopId, itemId, anhDaiDien);
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, "image/png");
            }
            return null;
        }

        [HttpGet("Images/Orders/{userId}/{orderId}/{anhDaiDien}")]
        public object GetOrderImage(string userId, string orderId, string anhDaiDien)
        {
            var folderName = Path.Combine("Statics", "Images", "Orders");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, userId, orderId, anhDaiDien);
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, "image/png");
            }
            return null;
        }

        [HttpGet("Images/Auctions/{shopId}/{itemId}/{anhDaiDien}")]
        public object GetAuctionImage(string shopId, string itemId, string anhDaiDien)
        {
            var folderName = Path.Combine("Statics", "Images", "Auctions");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, shopId, itemId, anhDaiDien);
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, "image/png");
            }
            return null;
        }

        [HttpGet("Files/Chats/{chatBoxId}/{fileName}")]
        public object GetChatFile(string chatBoxId, string fileName)
        {
            var folderName = Path.Combine("Statics", "Files", "Chats");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, chatBoxId, fileName);
            var extend = fileName.Split(".").Last();
            var contentType = "";
            switch (extend)
            {
                case "avif":
                    {
                        contentType = "image/avif";
                        break;
                    }
                case "bmp":
                    {
                        contentType = "image/bmp";
                        break;
                    }
                case "gif":
                    {
                        contentType = "image/gif";
                        break;
                    }
                case "ico":
                    {
                        contentType = "image/vnd.microsoft.icon";
                        break;
                    }
                case "jpeg":
                    {
                        contentType = "image/jpeg";
                        break;
                    }
                case "jpg":
                    {
                        contentType = "image/jpeg";
                        break;
                    }
                case "png":
                    {
                        contentType = "image/png";
                        break;
                    }
                case "svg":
                    {
                        contentType = "image/svg+xml";
                        break;
                    }
                case "webp":
                    {
                        contentType = "image/webp";
                        break;
                    }
                case "avi":
                    {
                        contentType = "video/x-msvideo";
                        break;
                    }
                case "bz":
                    {
                        contentType = "application/x-bzip";
                        break;
                    }
                case "bz2":
                    {
                        contentType = "application/x-bzip2";
                        break;
                    }
                case "cda":
                    {
                        contentType = "application/x-cdf";
                        break;
                    }
                case "csv":
                    {
                        contentType = "text/csv";
                        break;
                    }
                case "doc":
                    {
                        contentType = "application/msword";
                        break;
                    }
                case "docx":
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        break;
                    }
                case "epub":
                    {
                        contentType = "application/epub+zip";
                        break;
                    }
                case "gz":
                    {
                        contentType = "application/gzip";
                        break;
                    }
                case "mid":
                    {
                        contentType = "audio/midi";
                        break;
                    }
                case "midi":
                    {
                        contentType = "audio/x-midi";
                        break;
                    }
                case "mp3":
                    {
                        contentType = "audio/mpeg";
                        break;
                    }
                case "mp4":
                    {
                        contentType = "video/mp4";
                        break;
                    }
                case "mpeg":
                    {
                        contentType = "video/mpeg";
                        break;
                    }
                case "odp":
                    {
                        contentType = "application/vnd.oasis.opendocument.presentation";
                        break;
                    }
                case "ods":
                    {
                        contentType = "application/vnd.oasis.opendocument.spreadsheet";
                        break;
                    }
                case "odt":
                    {
                        contentType = "application/vnd.oasis.opendocument.text";
                        break;
                    }
                case "oga":
                    {
                        contentType = "audio/ogg";
                        break;
                    }
                case "ogv":
                    {
                        contentType = "video/ogg";
                        break;
                    }
                case "ogx":
                    {
                        contentType = "application/ogg";
                        break;
                    }
                case "opus":
                    {
                        contentType = "audio/opus";
                        break;
                    }
                case "pdf":
                    {
                        contentType = "application/pdf";
                        break;
                    }
                case "ppt":
                    {
                        contentType = "application/vnd.ms-powerpoint";
                        break;
                    }
                case "pptx":
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                        break;
                    }
                case "rar":
                    {
                        contentType = "application/vnd.rar";
                        break;
                    }
                case "rtf":
                    {
                        contentType = "application/rtf";
                        break;
                    }
                case "tar":
                    {
                        contentType = "application/x-tar";
                        break;
                    }
                case "ts":
                    {
                        contentType = "video/mp2t";
                        break;
                    }
                case "txt":
                    {
                        contentType = "text/plain";
                        break;
                    }
                case "wav":
                    {
                        contentType = "audio/wav";
                        break;
                    }
                case "vsd":
                    {
                        contentType = "application/vnd.visio";
                        break;
                    }
                case "weba":
                    {
                        contentType = "audio/webm";
                        break;
                    }
                case "webm":
                    {
                        contentType = "video/webm";
                        break;
                    }
                case "xls":
                    {
                        contentType = "application/vnd.ms-excel";
                        break;
                    }
                case "xlsx":
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        break;
                    }
                case "zip":
                    {
                        contentType = "application/x-zip-compressed";
                        break;
                    }
                case "7z":
                    {
                        contentType = "application/x-7z-compressed";
                        break;
                    }
                default:
                    {
                        return null;
                    }
            }
            if (System.IO.File.Exists(fullPath))
            {
                var file = System.IO.File.ReadAllBytes(fullPath);
                return File(file, contentType);
            }
            return null;
        }
    }
}
