﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.DataContext;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize(Policy = "Customer")]
    [ApiController]
    public class HinhAnhSanPhamDauGiaController : ControllerBase
    {
        private readonly IHinhAnhSanPhamDauGiaService _service;
        private readonly IValidateService _validateService;
        private readonly ApplicationDBContext _context;

        public HinhAnhSanPhamDauGiaController(IHinhAnhSanPhamDauGiaService service, IValidateService validateService, ApplicationDBContext context)
        {
            _service = service;
            _validateService = validateService;
            _context = context;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateHinhAnhSanPhamDauGia([FromQuery] HinhAnhSanPhamDauGiaView hinhAnhSanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(hinhAnhSanPhamDauGiaView.UserId, token);
            if (validate)
            {
                var sanPham = _context.SanPhamDauGias.Where(s => s.Id == hinhAnhSanPhamDauGiaView.SanPhamDauGiaId).FirstOrDefault();
                if (sanPham == null)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                var shop = _context.Shops.Where(s => s.Id == sanPham.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if (shop.UserId != hinhAnhSanPhamDauGiaView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                var files = Request.Form.Files;
                var folderName = Path.Combine("Statics", "Images", "Auctions", shop.Id.ToString(), sanPham.Id.ToString());
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                Directory.CreateDirectory(pathToSave);
                if (files.Count > 0)
                {
                    var hinhAnhSanPhamDauGiaViews = new List<HinhAnhSanPhamDauGiaView>();
                    foreach (var file in files)
                    {
                        if (file.Length > 0)
                        {
                            var fileName = file.FileName;
                            var name = "";
                            for (int i = 0; i < fileName.Split('.').Count(); i++)
                            {
                                if (i < fileName.Split('.').Count() - 1)
                                {
                                    name += fileName.Split('.')[i];
                                }
                            }
                            var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                            var fullPath = Path.Combine(pathToSave, fileRename);
                            var dbPath = Path.Combine(folderName, fileRename);
                            using (var stream = new FileStream(fullPath, FileMode.Create))
                            {
                                file.CopyTo(stream);
                            }
                            var hinhAnhSanPhamDauGia = new HinhAnhSanPhamDauGiaView();
                            hinhAnhSanPhamDauGia.HinhAnh = dbPath;
                            hinhAnhSanPhamDauGia.SanPhamDauGiaId = hinhAnhSanPhamDauGiaView.SanPhamDauGiaId;
                            hinhAnhSanPhamDauGiaViews.Add(hinhAnhSanPhamDauGia);
                        }
                    }
                    var res = _service.CreateHinhAnhSanPhamDauGia(hinhAnhSanPhamDauGiaViews);
                    return res;
                }
                else
                {
                    return new ResponsePostView(MessageConstants.FILE_NOT_FOUND, 404);
                }
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        public async Task<ResponsePostView> DeleteHinhAnhSanPhamDauGia([FromQuery] List<Guid> hinhAnhSanPhamDauGiaId, [FromQuery] Guid userId)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = _service.DeleteHinhAnhSanPhamDauGia(hinhAnhSanPhamDauGiaId, userId);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public List<HinhAnhSanPhamDauGiaView> GetAllHinhAnhSanPhamDauGiaBySanPhamId([FromQuery] Guid sanPhamDauGiaId)
        {
            var res = _service.GetAllHinhAnhSanPhamDauGiaBySanPhamId(sanPhamDauGiaId);
            return res;
        }

        [HttpPut]
        public async Task<ResponsePostView> UpdateHinhAnhSanPhamDauGia([FromBody] List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(hinhAnhSanPhamDauGiaView[0].UserId, token);
            if (validate)
            {
                var res = _service.UpdateHinhAnhSanPhamDauGia(hinhAnhSanPhamDauGiaView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }
    }
}
