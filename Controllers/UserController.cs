﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly IValidateService _validateService;

        public UserController(IUserService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        [Authorize(Policy = "Admin")]
        public ResponseWithPaginationView GetAllUser([FromQuery] UserParamView userParamView)
        {
            var res = _service.GetAllUser(userParamView);
            return res;
        }

        [HttpGet]
        public UserView GetUserById([FromQuery] Guid id)
        {
            var res = _service.GetUserById(id);
            return res;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateUser([FromBody] UserView userView)
        {
            var res = await _service.CreateUser(userView);
            return res;
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UpdateUser([FromBody] UserView userView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userView.Id, token);
            if (validate)
            {
                var res = _service.UpdateUser(userView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPost]
        public ResponsePostView CheckExistedUserName([FromBody] UserView userView)
        {
            var res = _service.CheckExistedUserName(userView);
            return res;
        }

        [HttpPost]
        public ResponsePostView CheckExistedEmail([FromBody] UserView userView)
        {
            var res = _service.CheckExistedEmail(userView);
            return res;
        }

        [HttpPost]
        public ResponsePostView CheckPassword([FromBody] UserView userView)
        {
            var res = _service.CheckPassword(userView);
            return res;
        }

        [HttpPut]
        [Authorize(Policy = "Admin")]
        public ResponsePostView SetStatusAndRoleForUser([FromBody] UserView userView)
        {
            var res = _service.SetStatusAndRoleForUser(userView);
            return res;
        }
    }
}
