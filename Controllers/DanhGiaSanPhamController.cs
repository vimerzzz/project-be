﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DanhGiaSanPhamController : ControllerBase
    {
        private readonly IDanhGiaSanPhamService _service;
        private readonly IValidateService _validateService;

        public DanhGiaSanPhamController(IDanhGiaSanPhamService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateDanhGia([FromBody] DanhGiaSanPhamView danhGiaSanPhamView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(danhGiaSanPhamView.UserId, token);
            if (validate)
            {
                var res = _service.CreateDanhGia(danhGiaSanPhamView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllDanhGia([FromQuery] DanhGiaSanPhamParamView danhGiaSanPhamParamView)
        {
            var res = _service.GetAllDanhGia(danhGiaSanPhamParamView);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateDanhGiaHuuIch([FromBody] DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(danhGiaSanPhamHuuIchView.UserId, token);
            if (validate)
            {
                var res = _service.CreateDanhGiaHuuIch(danhGiaSanPhamHuuIchView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> DeleteDanhGiaHuuIch([FromQuery] DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(danhGiaSanPhamHuuIchView.UserId, token);
            if (validate)
            {
                var res = _service.DeleteDanhGiaHuuIch(danhGiaSanPhamHuuIchView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public List<DanhGiaSanPhamHuuIchView> GetDanhGiaHuuIchByUserId([FromQuery] Guid userId)
        {
            var res = _service.GetDanhGiaHuuIchByUserId(userId);
            return res;
        }
    }
}
