﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaoCaoViPhamController : ControllerBase
    {
        private readonly IBaoCaoViPhamService _service;
        private readonly IValidateService _validateService;

        public BaoCaoViPhamController(IBaoCaoViPhamService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        [Authorize(Policy = "Admin")]
        public ResponseWithPaginationView GetAllBaoCao([FromQuery] BaoCaoViPhamParamView baoCaoParamView)
        {
            var res = _service.GetAllBaoCao(baoCaoParamView);
            return res;
        }

        [HttpGet]
        [Authorize(Policy = "Admin")]
        public List<BaoCaoViPhamView> GetAllNguoiBaoCao()
        {
            var res = _service.GetAllNguoiBaoCao();
            return res;
        }

        [HttpGet]
        [Authorize(Policy = "Admin")]
        public List<BaoCaoViPhamView> GetAllNguoiBiBaoCao()
        {
            var res = _service.GetAllNguoiBiBaoCao();
            return res;
        }

        [HttpGet]
        [Authorize(Policy = "Admin")]
        public List<BaoCaoViPhamView> GetAllShopBiBaoCao()
        {
            var res = _service.GetAllShopBiBaoCao();
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateBaoCao([FromQuery] Guid userId, [FromBody] BaoCaoViPhamView baoCaoView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = await _service.CreateBaoCao(baoCaoView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        [Authorize(Policy = "Admin")]
        public async Task<ResponsePostView> UpdateBaoCao([FromBody] BaoCaoViPhamView baoCaoView)
        {
            var res = await _service.UpdateBaoCao(baoCaoView);
            return res;
        }

        [HttpDelete]
        [Authorize(Policy = "Admin")]
        public async Task<ResponsePostView> DeleteBaoCao([FromQuery] BaoCaoViPhamView baoCaoView)
        {
            var res = await _service.DeleteBaoCao(baoCaoView);
            return res;
        }
    }
}
