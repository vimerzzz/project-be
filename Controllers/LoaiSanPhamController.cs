﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project.Services;
using Project.ViewModels;
using System;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoaiSanPhamController : ControllerBase
    {
        private readonly ILoaiSanPhamService _service;

        public LoaiSanPhamController(ILoaiSanPhamService service)
        {
            _service = service;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllLoaiSanPham([FromQuery] LoaiSanPhamParamView loaiSanPhamParamView)
        {
            var res = _service.GetAllLoaiSanPham(loaiSanPhamParamView);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Admin")]
        public ResponsePostView CreateLoaiSanPham([FromBody] LoaiSanPhamView loaiSanPhamView)
        {
            var res = _service.CreateLoaiSanPham(loaiSanPhamView);
            return res;
        }

        [HttpPut]
        [Authorize(Policy = "Admin")]
        public ResponsePostView UpdateLoaiSanPham([FromBody] LoaiSanPhamView loaiSanPhamView)
        {
            var res = _service.UpdateLoaiSanPham(loaiSanPhamView);
            return res;
        }

        [HttpDelete]
        [Authorize(Policy = "Admin")]
        public ResponsePostView DeleteLoaiSanPham([FromQuery] Guid id)
        {
            var res = _service.DeleteLoaiSanPham(id);
            return res;
        }
    }
}
