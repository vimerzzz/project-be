﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserInfoController : ControllerBase
    {
        private readonly IUserInfoService _service;
        private readonly IValidateService _validateService;

        public UserInfoController(IUserInfoService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        public UserInfoView GetUserInfoByUserId([FromQuery] Guid userId)
        {
            var res = _service.GetUserInfoByUserId(userId);
            return res;
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UpdateUserInfo([FromBody] UserInfoView userInfoView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userInfoView.UserId, token);
            if (validate)
            {
                var res = _service.UpdateUserInfo(userInfoView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UploadAnhDaiDien([FromQuery] Guid userId)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var user = _service.GetUserInfoByUserId(userId);
                if(user != null)
                {
                    var file = Request.Form.Files[0];
                    var folderName = Path.Combine("Statics", "Images", "Users", userId.ToString());
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    Directory.CreateDirectory(pathToSave);
                    if (file.Length > 0)
                    {
                        var fileName = file.FileName;
                        var name = "";
                        for (int i = 0; i < fileName.Split('.').Count(); i++)
                        {
                            if (i < fileName.Split('.').Count() - 1)
                            {
                                name += fileName.Split('.')[i];
                            }
                        }
                        var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                        var fullPath = Path.Combine(pathToSave, fileRename);
                        var dbPath = Path.Combine(folderName, fileRename);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        if (user.CoAnhDaiDien)
                        {
                            string removePath = "";
                            if (user.AnhDaiDien != null && user.AnhDaiDien != "Statics\\Images\\Users\\blank_avatar.png")
                            {
                                removePath = Path.Combine(pathToSave, user.AnhDaiDien.Split("\\").Last());
                            }
                            if (System.IO.File.Exists(removePath))
                            {
                                System.IO.File.Delete(removePath);
                            }
                        }
                        else
                        {
                            user.CoAnhDaiDien = true;
                        }
                        user.AnhDaiDien = dbPath;
                        var res = _service.UpdateUserInfo(user);
                        return res;
                    }
                    else
                    {
                        return new ResponsePostView(MessageConstants.FILE_NOT_FOUND, 404);
                    }
                }
                else
                {
                    return new ResponsePostView(MessageConstants.USER_NOT_FOUND, 404);
                }
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }
    }
}
