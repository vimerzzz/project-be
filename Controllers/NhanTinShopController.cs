﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Policy = "Customer")]
    public class NhanTinShopController : ControllerBase
    {
        private readonly INhanTinShopService _service;
        private readonly IValidateService _validateService;

        public NhanTinShopController(INhanTinShopService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateNhanTinShop([FromQuery] Guid userId, [FromBody] NhanTinShopView nhanTinShopView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = await _service.CreateNhanTinShop(nhanTinShopView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllNhanTinShop([FromQuery] NhanTinShopParamView nhanTinParamView)
        {
            var res = _service.GetAllNhanTinShop(nhanTinParamView);
            return res;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllShopId([FromQuery] NhanTinShopParamView nhanTinParamView)
        {
            var res = _service.GetAllShopId(nhanTinParamView);
            return res;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllUserId([FromQuery] NhanTinShopParamView nhanTinParamView)
        {
            var res = _service.GetAllUserId(nhanTinParamView);
            return res;
        }

        [HttpPut]
        public async Task<ResponsePostView> UpdateNhanTinShop([FromQuery] Guid userId, [FromBody] List<NhanTinShopView> nhanTinShopView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = await _service.UpdateNhanTinShop(nhanTinShopView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPost]
        public async Task<ResponseNoiDungPostView> UploadFile([FromQuery] Guid userLogId, [FromQuery] NhanTinShopView nhanTinShopView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userLogId, token);
            if (validate)
            {
                var files = Request.Form.Files;
                var res = await _service.UploadFile(nhanTinShopView, files);
                return res;
            }
            else
            {
                return new ResponseNoiDungPostView(MessageConstants.VALIDATE_FAILED, 400, null, null);
            }
        }
    }
}
