﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SanPhamController : ControllerBase
    {
        private readonly ISanPhamService _service;
        private readonly IValidateService _validateService;

        public SanPhamController(ISanPhamService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateSanPham([FromBody] SanPhamView sanPhamView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamView.UserId, token);
            if (validate)
            {
                var res = _service.CreateSanPham(sanPhamView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> DeleteSanPham([FromQuery] SanPhamView sanPhamView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamView.UserId, token);
            if (validate)
            {
                var res = _service.DeleteSanPham(sanPhamView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public ResponseWithPaginationView GetAllSanPhamByShopId([FromQuery] SanPhamParamView sanPhamParamView)
        {
            var res = _service.GetAllSanPhamByShopId(sanPhamParamView);
            return res;
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UpdateSanPham([FromBody] SanPhamView sanPhamView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(sanPhamView.UserId, token);
            if (validate)
            {
                var res = _service.UpdateSanPham(sanPhamView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetNewestAllSanPhamWithHinhAnhByShopId([FromQuery] SanPhamParamView sanPhamParamView)
        {
            var res = _service.GetNewestAllSanPhamWithHinhAnhByShopId(sanPhamParamView);
            return res;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllSanPham([FromQuery] SanPhamParamView sanPhamParamView)
        {
            var res = _service.GetAllSanPham(sanPhamParamView);
            return res;
        }

        [HttpGet]
        public SanPhamWithHinhAnhView GetSanPhamWithHinhAnhByName([FromQuery] string tenShop, string tenSanPham)
        {
            var res = _service.GetSanPhamWithHinhAnhByName(tenShop, tenSanPham);
            return res;
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public SanPhamView GetSanPhamById([FromQuery] SanPhamView sanPhamView)
        {
            var res = _service.GetSanPhamById(sanPhamView);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponseNoiDungPostView> UploadImageGioiThieuSanPham([FromQuery] Guid userId, Guid sanPhamId)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var param = new SanPhamView
                {
                    Id = sanPhamId
                };
                var sanPham = _service.GetSanPhamById(param);
                if (sanPham != null)
                {
                    var file = Request.Form.Files[0];
                    var folderName = Path.Combine("Statics", "Images", "Items", sanPham.ShopId.ToString(), sanPhamId.ToString());
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    Directory.CreateDirectory(pathToSave);
                    if (file.Length > 0)
                    {
                        var fileName = file.FileName;
                        var name = "";
                        for (int i = 0; i < fileName.Split('.').Count(); i++)
                        {
                            if (i < fileName.Split('.').Count() - 1)
                            {
                                name += fileName.Split('.')[i];
                            }
                        }
                        var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                        var fullPath = Path.Combine(pathToSave, fileRename);
                        //var dbPath = Path.Combine(folderName, fileRename);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        return new ResponseNoiDungPostView(MessageConstants.CREATE_SUCCESS, 200, fileRename, null);
                    }
                    else
                    {
                        return new ResponseNoiDungPostView(MessageConstants.FILE_NOT_FOUND, 404, null, null);
                    }
                }
                else
                {
                    return new ResponseNoiDungPostView(MessageConstants.NOT_FOUND, 404, null, null);
                }
            }
            else
            {
                return new ResponseNoiDungPostView(MessageConstants.VALIDATE_FAILED, 400, null, null);
            }
        }
    }
}
