﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Policy = "Customer")]
    public class GioHangController : ControllerBase
    {
        private readonly IGioHangService _service;
        private readonly IValidateService _validateService;

        public GioHangController(IGioHangService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateGioHang([FromBody] GioHangView gioHangView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(gioHangView.UserId, token);
            if (validate)
            {
                var res = _service.CreateGioHang(gioHangView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        public async Task<ResponsePostView> DeleteGioHang([FromQuery] GioHangView gioHangView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(gioHangView.UserId, token);
            if (validate)
            {
                var res = _service.DeleteGioHang(gioHangView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public List<GioHangWithListSanPhamView> GetAllGioHangDauGia([FromQuery] GioHangView gioHangView)
        {
            var res = _service.GetAllGioHangDauGia(gioHangView);
            return res;
        }

        [HttpGet]
        public List<GioHangWithListSanPhamView> GetAllGioHang([FromQuery] GioHangView gioHangView)
        {
            var res = _service.GetAllGioHang(gioHangView);
            return res;
        }

        [HttpPut]
        public async Task<ResponsePostView> UpdateGioHang([FromBody] GioHangView gioHangView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(gioHangView.UserId, token);
            if (validate)
            {
                var res = _service.UpdateGioHang(gioHangView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }
    }
}
