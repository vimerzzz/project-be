﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Project.Models;
using Project.Utilities.Constants;
using Project.Utilities.Verifications;
using Project.ViewModels;
using Project.DataContext;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Project.Services;
using Serilog;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class VerifyController : ControllerBase
    {
        private readonly ApplicationDBContext _context;
        private readonly UserManager<User> _userManager;
        private readonly SendVerificationEmail emailVerification = new SendVerificationEmail();
        private readonly SendRecoverPasswordEmail recoverPasswordVerification = new SendRecoverPasswordEmail();

        public VerifyController(ApplicationDBContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> GetEmailVerification([FromQuery] Guid id, string urlClient)
        {
            try
            {
                var user = _context.Users.Where(u => u.Id == id).FirstOrDefault();
                if(user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                emailVerification.SendMail(VerifyConstants.FromAddress, user.Email, token, VerifyConstants.SmtpPassword, urlClient);
                return new ResponsePostView(MessageConstants.DONE, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
            }
        }

        [HttpPost]
        public async Task<ResponsePostView> VerifyEmail([FromBody] VerifyUserView verifyUserView)
        {
            try
            {
                var user = _context.Users.Where(u => u.Email == verifyUserView.Email).FirstOrDefault();
                if (user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var result = await _userManager.ConfirmEmailAsync(user, verifyUserView.Token);
                if (result.Succeeded)
                {
                    await _userManager.UpdateSecurityStampAsync(user);
                    return new ResponsePostView(MessageConstants.DONE, 200);
                }
                else
                {
                    return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
                }
            }
            catch(Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
            }
        }

        [HttpGet]
        public async Task<ResponsePostView> GetRecoverPassword([FromQuery] string email, string urlClient)
        {
            try
            {
                var user = _context.Users.Where(u => u.Email == email).FirstOrDefault();
                if (user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                recoverPasswordVerification.SendMail(VerifyConstants.FromAddress, user.Email, token, VerifyConstants.SmtpPassword, urlClient);
                return new ResponsePostView(MessageConstants.DONE, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
            }
        }

        [HttpPost]
        public async Task<ResponsePostView> RecoverPassword([FromBody] VerifyUserView verifyUserView)
        {
            try
            {
                var user = _context.Users.Where(u => u.Email == verifyUserView.Email).FirstOrDefault();
                if (user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var result = await _userManager.ConfirmEmailAsync(user, verifyUserView.Token);
                if (result.Succeeded)
                {
                    user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, verifyUserView.Password);
                    await _userManager.UpdateSecurityStampAsync(user);
                    return new ResponsePostView(MessageConstants.DONE, 200);
                }
                else
                {
                    return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
            }
        }
    }
}
