﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DanhGiaShopController : ControllerBase
    {
        private readonly IDanhGiaShopService _service;
        private readonly IValidateService _validateService;

        public DanhGiaShopController(IDanhGiaShopService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateDanhGia([FromBody] DanhGiaShopView danhGiaShopView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(danhGiaShopView.UserId, token);
            if (validate)
            {
                var res = _service.CreateDanhGia(danhGiaShopView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllDanhGia([FromQuery] DanhGiaShopParamView danhGiaShopParamView)
        {
            var res = _service.GetAllDanhGia(danhGiaShopParamView);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateDanhGiaHuuIch([FromBody] DanhGiaShopHuuIchView danhGiaShopHuuIchView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(danhGiaShopHuuIchView.UserId, token);
            if (validate)
            {
                var res = _service.CreateDanhGiaHuuIch(danhGiaShopHuuIchView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> DeleteDanhGiaHuuIch([FromQuery] DanhGiaShopHuuIchView danhGiaShopHuuIchView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(danhGiaShopHuuIchView.UserId, token);
            if (validate)
            {
                var res = _service.DeleteDanhGiaHuuIch(danhGiaShopHuuIchView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public List<DanhGiaShopHuuIchView> GetDanhGiaHuuIchByUserId([FromQuery] Guid userId)
        {
            var res = _service.GetDanhGiaHuuIchByUserId(userId);
            return res;
        }
    }
}
