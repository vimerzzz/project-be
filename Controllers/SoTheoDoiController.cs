﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Policy = "Customer")]
    public class SoTheoDoiController : ControllerBase
    {
        private readonly ISoTheoDoiService _service;
        private readonly IValidateService _validateService;

        public SoTheoDoiController(ISoTheoDoiService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateSoTheoDoi([FromBody] SoTheoDoiView soTheoDoiView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(soTheoDoiView.UserId, token);
            if (validate)
            {
                var res = _service.CreateSoTheoDoi(soTheoDoiView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        public async Task<ResponsePostView> DeleteSoTheoDoi([FromQuery] SoTheoDoiView soTheoDoiView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(soTheoDoiView.UserId, token);
            if (validate)
            {
                var res = _service.DeleteSoTheoDoi(soTheoDoiView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllSoTheoDoi([FromQuery] SoTheoDoiParamView soTheoDoiParamView)
        {
            var res = _service.GetAllSoTheoDoi(soTheoDoiParamView);
            return res;
        }
    }
}
