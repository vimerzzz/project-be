﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ThongBaoController : ControllerBase
    {
        private readonly IThongBaoService _service;
        private readonly IValidateService _validateService;

        public ThongBaoController(IThongBaoService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public ResponseWithPaginationView GetAllThongBao([FromQuery] ThongBaoParamView thongBaoParamView)
        {
            var res = _service.GetAllThongBao(thongBaoParamView);
            return res;
        }

        [HttpGet]
        [Authorize(Policy = "Admin")]
        public ResponseWithPaginationView GetAllThongBaoCoLichHen([FromQuery] ThongBaoParamView thongBaoParamView)
        {
            var res = _service.GetAllThongBaoCoLichHen(thongBaoParamView);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Admin")]
        public ResponsePostView CreateThongBao([FromBody] List<ThongBaoView> thongBaoView)
        {
            var res = _service.CreateThongBao(thongBaoView);
            return res;
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UpdateThongBao([FromBody] List<ThongBaoView> thongBaoView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(thongBaoView[0].UserId, token);
            if (validate)
            {
                var res = _service.UpdateThongBao(thongBaoView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpDelete]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> DeleteThongBao([FromQuery] List<Guid> id, [FromQuery] Guid userId)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = _service.DeleteThongBao(id);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }
    }
}
