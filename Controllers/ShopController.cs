﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ShopController : ControllerBase
    {
        private readonly IShopService _service;
        private readonly IValidateService _validateService;

        public ShopController(IShopService service, IValidateService validateService)
        {
            _service = service;
            _validateService = validateService;
        }

        [HttpGet]
        [Authorize(Policy = "Admin")]
        public ResponseWithPaginationView GetAllShop([FromQuery] ShopParamView shopParamView)
        {
            var res = _service.GetAllShop(shopParamView);
            return res;
        }

        [HttpGet]
        public ShopView GetShopByShopName([FromQuery] string shopName)
        {
            var res = _service.GetShopByShopName(shopName);
            return res;
        }

        [HttpGet]
        public ShopView GetShopByShopId([FromQuery] Guid shopId)
        {
            var res = _service.GetShopByShopId(shopId);
            return res;
        }

        [HttpGet]
        [Authorize(Policy = "Customer")]
        public ShopView GetShopByUserId([FromQuery] Guid userId)
        {
            var res = _service.GetShopByUserId(userId);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> CreateShop([FromBody] ShopView shopView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(shopView.UserId, token);
            if (validate)
            {
                var res = _service.CreateShop(shopView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UpdateShop([FromBody] ShopView shopView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(shopView.UserId, token);
            if (validate)
            {
                var res = _service.UpdateShop(shopView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPut]
        [Authorize(Policy = "Admin")]
        public ResponsePostView UpdateStatusShop([FromBody] ShopView shopView)
        {
            var res = _service.UpdateStatusShop(shopView);
            return res;
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponsePostView> UploadAnhDaiDien([FromQuery] Guid userId)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var shop = _service.GetShopByUserId(userId);
                if (shop != null)
                {
                    var file = Request.Form.Files[0];
                    var folderName = Path.Combine("Statics", "Images", "Shops", shop.Id.ToString());
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    Directory.CreateDirectory(pathToSave);
                    if (file.Length > 0)
                    {
                        var fileName = file.FileName;
                        var name = "";
                        for (int i = 0; i < fileName.Split('.').Count(); i++)
                        {
                            if (i < fileName.Split('.').Count() - 1)
                            {
                                name += fileName.Split('.')[i];
                            }
                        }
                        var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                        var fullPath = Path.Combine(pathToSave, fileRename);
                        var dbPath = Path.Combine(folderName, fileRename);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        if (shop.CoAnhDaiDien)
                        {
                            string removePath = "";
                            if (shop.AnhDaiDien != null && shop.AnhDaiDien != "Statics\\Images\\Shops\\blank_avatar.png")
                            {
                                removePath = Path.Combine(pathToSave, shop.AnhDaiDien.Split("\\").Last());
                            }
                            if (System.IO.File.Exists(removePath))
                            {
                                System.IO.File.Delete(removePath);
                            }
                        }
                        else
                        {
                            shop.CoAnhDaiDien = true;
                        }
                        shop.AnhDaiDien = dbPath;
                        var res = _service.UpdateShop(shop);
                        return res;
                    }
                    else
                    {
                        return new ResponsePostView(MessageConstants.FILE_NOT_FOUND, 404);
                    }
                }
                else
                {
                    return new ResponsePostView(MessageConstants.USER_NOT_FOUND, 404);
                }
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<ResponseNoiDungPostView> UploadImageGioiThieuShop([FromQuery] Guid userId)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var shop = _service.GetShopByUserId(userId);
                if (shop != null)
                {
                    var file = Request.Form.Files[0];
                    var folderName = Path.Combine("Statics", "Images", "Shops", shop.Id.ToString());
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    Directory.CreateDirectory(pathToSave);
                    if (file.Length > 0)
                    {
                        var fileName = file.FileName;
                        var name = "";
                        for (int i = 0; i < fileName.Split('.').Count(); i++)
                        {
                            if (i < fileName.Split('.').Count() - 1)
                            {
                                name += fileName.Split('.')[i];
                            }
                        }
                        var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                        var fullPath = Path.Combine(pathToSave, fileRename);
                        //var dbPath = Path.Combine(folderName, fileRename);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        return new ResponseNoiDungPostView(MessageConstants.CREATE_SUCCESS, 200, fileRename, null);
                    }
                    else
                    {
                        return new ResponseNoiDungPostView(MessageConstants.FILE_NOT_FOUND, 404, null, null);
                    }
                }
                else
                {
                    return new ResponseNoiDungPostView(MessageConstants.SHOP_NOT_FOUND, 404, null, null);
                }
            }
            else
            {
                return new ResponseNoiDungPostView(MessageConstants.VALIDATE_FAILED, 400, null, null);
            }
        }
    }
}
