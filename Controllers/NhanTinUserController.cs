﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Project.DataContext;
using Project.Services;
using Project.Utilities.Constants;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Policy = "Customer")]
    public class NhanTinUserController : ControllerBase
    {
        private readonly INhanTinUserService _service;
        private readonly IValidateService _validateService;
        private readonly ApplicationDBContext _context;

        public NhanTinUserController(INhanTinUserService service, IValidateService validateService, ApplicationDBContext context)
        {
            _service = service;
            _validateService = validateService;
            _context = context;
        }

        [HttpPost]
        public async Task<ResponsePostView> CreateNhanTinUser([FromQuery] Guid userId, [FromBody] NhanTinUserView nhanTinUserView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = await _service.CreateNhanTinUser(nhanTinUserView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllNhanTinUser([FromQuery] NhanTinUserParamView nhanTinParamView)
        {
            var res = _service.GetAllNhanTinUser(nhanTinParamView);
            return res;
        }

        [HttpGet]
        public ResponseWithPaginationView GetAllUserNhanId([FromQuery] NhanTinUserParamView nhanTinParamView)
        {
            var res = _service.GetAllUserNhanId(nhanTinParamView);
            return res;
        }

        [HttpPut]
        public async Task<ResponsePostView> UpdateNhanTinUser([FromQuery] Guid userId, [FromBody] List<NhanTinUserView> nhanTinUserView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userId, token);
            if (validate)
            {
                var res = await _service.UpdateNhanTinUser(nhanTinUserView);
                return res;
            }
            else
            {
                return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
            }
        }

        [HttpPost]
        public async Task<ResponseNoiDungPostView> UploadFile([FromQuery] Guid userLogId, [FromQuery] NhanTinUserView nhanTinUserView)
        {
            var token = Request.Headers[HeaderNames.Authorization];
            var validate = await _validateService.ValidateBearerToken(userLogId, token);
            if (validate)
            {
                var files = Request.Form.Files;
                var res = await _service.UploadFile(nhanTinUserView, files);
                return res;
            }
            else
            {
                return new ResponseNoiDungPostView(MessageConstants.VALIDATE_FAILED, 400, null, null);
            }
        }
    }
}
