﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class LoaiSanPhamView
    {
        public Guid Id { get; set; }
        public string TenLoaiSanPham { get; set; }
        public string MoTaLoaiSanPham { get; set; }
    }

    public class LoaiSanPhamParamView : PaginationView
    {

    }

    public class LoaiSanPhamListView : ResponseWithPaginationView
    {
        public List<LoaiSanPhamView> Data { get; set; }
        public LoaiSanPhamListView(List<LoaiSanPhamView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
