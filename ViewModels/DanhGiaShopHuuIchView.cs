﻿using System;

namespace Project.ViewModels
{
    public class DanhGiaShopHuuIchView
    {
        public Guid Id { get; set; }
        public Guid DanhGiaShopId { get; set; }
        public Guid UserId { get; set; }
    }
}
