﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class TroChuyenDauGiaView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string NoiDung { get; set; }
        public bool IsSystem { get; set; }
        public bool IsShopOwner { get; set; }
        public Guid? UserId { get; set; }
        public Guid SanPhamDauGiaId { get; set; }
        public UserView User { get; set; }
    }

    public class TroChuyenDauGiaParamView : PaginationView
    {
        public Guid? SanPhamDauGiaId { get; set; }
    }

    public class TroChuyenDauGiaListView : ResponseWithPaginationView
    {
        public List<TroChuyenDauGiaView> Data { get; set; }
        public TroChuyenDauGiaListView(List<TroChuyenDauGiaView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
