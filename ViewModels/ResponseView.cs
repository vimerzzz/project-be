﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.ViewModels
{
    public class ResponsePostView
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }

        public ResponsePostView(string Message, int StatusCode)
        {
            this.Message = Message;
            this.StatusCode = StatusCode;
        }
    }

    public class ResponseWithPaginationView
    {
        public int TotalRecord { get; set; }
        public int StatusCode { get; set; }

        public ResponseWithPaginationView(int TotalRecord, int StatusCode)
        {
            this.TotalRecord = TotalRecord;
            this.StatusCode = StatusCode;
        }
    }

    public class ResponseLoginView
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public string Token { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }

        public ResponseLoginView(Guid UserId, Guid RoleId, string Token, string Message, int StatusCode)
        {
            this.UserId = UserId;
            this.Token = Token;
            this.Message = Message;
            this.StatusCode = StatusCode;
            this.RoleId = RoleId;
        }
    }

    public class ResponseIdPostView : ResponsePostView
    {
        public Guid? Id { get; set; }

        public ResponseIdPostView(string Message, int StatusCode, Guid? Id) : base(Message, StatusCode)
        {
            this.Id = Id;
        }
    }

    public class ResponseMaDonHangPostView : ResponsePostView
    {
        public int? Id { get; set; }

        public ResponseMaDonHangPostView(string Message, int StatusCode, int? Id) : base(Message, StatusCode)
        {
            this.Id = Id;
        }
    }

    public class ResponseNoiDungPostView : ResponsePostView
    {
        public string NoiDung { get; set; }
        public Guid? Id { get; set; }

        public ResponseNoiDungPostView(string Message, int StatusCode, string NoiDung, Guid? Id) : base(Message, StatusCode)
        {
            this.NoiDung = NoiDung;
            this.Id = Id;
        }
    }
}
