﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class SoTheoDoiView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid UserId { get; set; }
        public Guid ShopId { get; set; }
        public ShopView Shop { get; set; }
    }

    public class SoTheoDoiParamView : PaginationView
    {
        public Guid? UserId { get; set; }
        public Guid? ShopId { get; set; }
    }

    public class SoTheoDoiListView : ResponseWithPaginationView
    {
        public List<SoTheoDoiView> Data { get; set; }
        public SoTheoDoiListView(List<SoTheoDoiView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
