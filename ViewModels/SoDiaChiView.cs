﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class SoDiaChiView
    {
        public Guid Id { get; set; }
        public string DiaChi { get; set; }
        public Guid UserId { get; set; }
    }

    public class SoDiaChiParamView : PaginationView
    {
        public Guid? UserId { get; set; }
    }

    public class SoDiaChiListView : ResponseWithPaginationView
    {
        public List<SoDiaChiView> Data { get; set; }
        public SoDiaChiListView(List<SoDiaChiView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
