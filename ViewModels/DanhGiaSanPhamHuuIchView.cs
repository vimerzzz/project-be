﻿using System;

namespace Project.ViewModels
{
    public class DanhGiaSanPhamHuuIchView
    {
        public Guid Id { get; set; }
        public Guid DanhGiaSanPhamId { get; set; }
        public Guid UserId { get; set; }
    }
}
