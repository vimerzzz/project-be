﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class ThongBaoView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string TieuDeThongBao { get; set; }
        public string NoiDungThongBao { get; set; }
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public bool DaDoc { get; set; }
        public bool DaXem { get; set; }
        public bool CoLichHen { get; set; }
        public DateTime ThoiGianHen { get; set; }
    }

    public class ThongBaoParamView : PaginationView
    {
        public Guid? UserId { get; set; }
        public bool? DaXem { get; set; }
        public bool? DaDoc { get; set; }
        public DateTime? ThoiGianHenBatDau { get; set; }
        public DateTime? ThoiGianHenKetThuc { get; set; }
    }

    public class ThongBaoListView : ResponseWithPaginationView
    {
        public List<ThongBaoView> Data { get; set; }
        public ThongBaoListView(List<ThongBaoView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }

    public class IdThongBaoCoLichHenView
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public Guid? RoleId { get; set; }
        public bool DaDoc { get; set; }
        public bool DaXem { get; set; }
    }

    public class ThongBaoCoLichHenView
    {
        public string TieuDeThongBao { get; set; }
        public string NoiDungThongBao { get; set; }
        public DateTime ThoiGianHen { get; set; }
        public bool CoLichHen { get; set; }
        public List<IdThongBaoCoLichHenView> IdThongBao { get; set; }
    }

    public class ThongBaoCoLichHenListView : ResponseWithPaginationView
    {
        public List<ThongBaoCoLichHenView> Data { get; set; }
        public ThongBaoCoLichHenListView(List<ThongBaoCoLichHenView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
