﻿using System;

namespace Project.ViewModels
{
    public class TinhTrangDauGiaView
    {
        public Guid Id { get; set; }
        public string TinhTrang { get; set; }
    }
}
