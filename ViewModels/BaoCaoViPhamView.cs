﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class BaoCaoViPhamView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string LyDoBaoCao { get; set; }
        public string NoiGuiBaoCao { get; set; }
        public Guid NguoiBaoCaoId { get; set; }
        public Guid? UserId { get; set; }
        public Guid? ShopId { get; set; }
        public bool DaXem { get; set; }
        public UserView NguoiBaoCao { get; set; }
        public UserView User { get; set; }
        public ShopView Shop { get; set; }
    }

    public class BaoCaoViPhamParamView : PaginationView
    {
        public Guid? NguoiBaoCaoId { get; set; }
        public Guid? UserId { get; set; }
        public Guid? ShopId { get; set; }
        public bool? DaXem { get; set; }
    }

    public class BaoCaoViPhamListView : ResponseWithPaginationView
    {
        public List<BaoCaoViPhamView> Data { get; set; }
        public BaoCaoViPhamListView(List<BaoCaoViPhamView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
