﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class NhanTinUserView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string NoiDung { get; set; }
        public Guid UserNhanId { get; set; }
        public Guid UserGuiId { get; set; }
        public bool DaXem { get; set; }
        public bool IsFile { get; set; }
        public bool IsImage { get; set; }
        public string FileName { get; set; }
    }

    public class NhanTinUserParamView : PaginationView
    {
        public Guid? UserGuiId { get; set; }
        public Guid? UserNhanId { get; set; }
        public bool? DaXem { get; set; }
    }

    public class NhanTinUserListView : ResponseWithPaginationView
    {
        public List<NhanTinUserView> Data { get; set; }
        public NhanTinUserListView(List<NhanTinUserView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
