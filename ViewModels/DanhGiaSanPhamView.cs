﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class DanhGiaSanPhamView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int DanhGia { get; set; }
        public string NhanXet { get; set; }
        public int HuuIch { get; set; }
        public Guid UserId { get; set; }
        public Guid SanPhamId { get; set; }
        public UserView User { get; set; }
    }

    public class DanhGiaSanPhamParamView : PaginationView
    {
        public Guid? SanPhamId { get; set; }
        public List<int?> DanhGia { get; set; }
    }

    public class DanhGiaSanPhamListView : ResponseWithPaginationView
    {
        public List<DanhGiaSanPhamView> Data { get; set; }
        public DanhGiaSanPhamListView(List<DanhGiaSanPhamView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
