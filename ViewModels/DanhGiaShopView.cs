﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class DanhGiaShopView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int DanhGia { get; set; }
        public string NhanXet { get; set; }
        public int HuuIch { get; set; }
        public Guid UserId { get; set; }
        public Guid ShopId { get; set; }
        public UserView User { get; set; }
    }

    public class DanhGiaShopParamView : PaginationView
    {
        public Guid? ShopId { get; set; }
        public List<int?> DanhGia { get; set; }
    }

    public class DanhGiaShopListView : ResponseWithPaginationView
    {
        public List<DanhGiaShopView> Data { get; set; }
        public DanhGiaShopListView(List<DanhGiaShopView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
