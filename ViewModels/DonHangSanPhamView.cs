﻿using System;

namespace Project.ViewModels
{
    public class DonHangSanPhamView
    {
        public Guid Id { get; set; }
        public string TenSanPham { get; set; }
        public string HinhAnhSanPham { get; set; }
        public int DonGia { get; set; }
        public int SoLuong { get; set; }
        public int GiamGia { get; set; }
        public bool SanPhamDauGia { get; set; }
        public Guid SanPhamGocId { get; set; }
        public Guid DonHangId { get; set; }
    }
}
