﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class SanPhamView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string TenSanPham { get; set; }
        public int DonGia { get; set; }
        public int SoLuong { get; set; }
        public string ThongTinKhac { get; set; }
        public int GiamGia { get; set; }
        public int SoLuongDaBan { get; set; }
        public int SoNguoiDanhGia { get; set; }
        public int MotSao { get; set; }
        public int HaiSao { get; set; }
        public int BaSao { get; set; }
        public int BonSao { get; set; }
        public int NamSao { get; set; }
        public float DanhGiaChung { get; set; }
        public float NongDo { get; set; }
        public string XuatXu { get; set; }
        public int Nam { get; set; }
        public string MauSac { get; set; }
        public float DungTich { get; set; }
        public Guid ShopId { get; set; }
        public Guid LoaiSanPhamId { get; set; }
        public Guid UserId { get; set; }
    }

    public class SanPhamParamView : PaginationView
    {
        public List<Guid?> LoaiSanPhamId { get; set; }
        public Guid? ShopId { get; set; }
        public bool? ConHang { get; set; }
    }

    public class SanPhamListView : ResponseWithPaginationView
    {
        public List<SanPhamView> Data { get; set; }
        public SanPhamListView(List<SanPhamView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }

    public class SanPhamWithHinhAnhView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string TenSanPham { get; set; }
        public int DonGia { get; set; }
        public int SoLuong { get; set; }
        public string ThongTinKhac { get; set; }
        public int GiamGia { get; set; }
        public int SoLuongDaBan { get; set; }
        public int SoNguoiDanhGia { get; set; }
        public int MotSao { get; set; }
        public int HaiSao { get; set; }
        public int BaSao { get; set; }
        public int BonSao { get; set; }
        public int NamSao { get; set; }
        public float DanhGiaChung { get; set; }
        public float NongDo { get; set; }
        public string XuatXu { get; set; }
        public int Nam { get; set; }
        public string MauSac { get; set; }
        public float DungTich { get; set; }
        public Guid ShopId { get; set; }
        public Guid LoaiSanPhamId { get; set; }
        public Guid UserId { get; set; }
        public string TenShop { get; set; }
        public string TenLoaiSanPham { get; set; }
        public List<HinhAnhSanPhamView> HinhAnhSanPham { get; set; }
    }

    public class SanPhamWithHinhAnhListView : ResponseWithPaginationView
    {
        public List<SanPhamWithHinhAnhView> Data { get; set; }
        public SanPhamWithHinhAnhListView(List<SanPhamWithHinhAnhView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
