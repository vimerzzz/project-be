﻿using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.ViewModels
{
    public class UserView
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool HoatDong { get; set; }
        public bool EmailConfirmed { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public UserInfoView UserInfo { get; set; }
    }

    public class VerifyUserView
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string Password { get; set; }
    }

    public class UserParamView : PaginationView
    {
        public bool? HoatDong { get; set; }
        public Guid? RoleId { get; set; }
    }

    public class UserListView : ResponseWithPaginationView
    {
        public List<UserView> Data { get; set; }
        public UserListView(List<UserView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }

    public class UserLogoutView
    {
        public Guid Id { get; set; }
    }
}
