﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class NhanTinShopView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string NoiDung { get; set; }
        public Guid ShopId { get; set; }
        public Guid UserId { get; set; }
        public Guid NguoiGuiId { get; set; }
        public bool DaXem { get; set; }
        public bool IsFile { get; set; }
        public bool IsImage { get; set; }
        public string FileName { get; set; }
    }

    public class NhanTinShopParamView : PaginationView
    {
        public Guid? UserId { get; set; }
        public Guid? ShopId { get; set; }
        public bool? DaXem { get; set; }
    }

    public class NhanTinShopListView : ResponseWithPaginationView
    {
        public List<NhanTinShopView> Data { get; set; }
        public NhanTinShopListView(List<NhanTinShopView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
