﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class ShopView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string TenShop { get; set; }
        public string MoTaShop { get; set; }
        public int SoNguoiTheoDoi { get; set; }
        public string AnhDaiDien { get; set; }
        public bool CoAnhDaiDien { get; set; }
        public int TongSoSanPham { get; set; }
        public int SoNguoiDanhGia { get; set; }
        public int MotSao { get; set; }
        public int HaiSao { get; set; }
        public int BaSao { get; set; }
        public int BonSao { get; set; }
        public int NamSao { get; set; }
        public float DanhGiaChung { get; set; }
        public bool HoatDong { get; set; }
        public Guid UserId { get; set; }
        public bool UserHoatDong { get; set; }
        public string TaiKhoanChuShop { get; set; }
    }

    public class ShopParamView : PaginationView
    {
        public bool? HoatDong { get; set; }
    }

    public class ShopListView : ResponseWithPaginationView
    {
        public List<ShopView> Data { get; set; }
        public ShopListView(List<ShopView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
