﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class DonHangView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int MaDonHang { get; set; }
        public int ThanhTien { get; set; }
        public string TenDayDu { get; set; }
        public string TenDayDuNguoiDat { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public string SoDienThoaiNguoiDat { get; set; }
        public string GhiChu { get; set; }
        public string LyDoHuy { get; set; }
        public bool XuatHoaDon { get; set; }
        public bool DatHo { get; set; }
        public Guid UserId { get; set; }
        public Guid ShopId { get; set; }
        public int SoSanPham { get; set; }
        public string TinhTrang { get; set; }
        public string ShopName { get; set; }
        public Guid TinhTrangDonHangId { get; set; }
        public List<DonHangSanPhamView> DonHangSanPham { get; set; }
    }

    public class DonHangParamView : PaginationView
    {
        public Guid? UserId { get; set; }
        public Guid? ShopId { get; set; }
        public Guid? TinhTrangDonHangId { get; set; }
        public int? MaDonHang { get; set; }
    }

    public class DonHangListView : ResponseWithPaginationView
    {
        public List<DonHangView> Data { get; set; }
        public DonHangListView(List<DonHangView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
