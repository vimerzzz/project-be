﻿using System;

namespace Project.ViewModels
{
    public class HinhAnhSanPhamDauGiaView
    {
        public Guid Id { get; set; }
        public string HinhAnh { get; set; }
        public int ThuTu { get; set; }
        public Guid SanPhamDauGiaId { get; set; }
        public Guid UserId { get; set; }
    }
}
