﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.ViewModels
{
    public class SanPhamDauGiaView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TenSanPham { get; set; }
        public int GiaKhoiDiem { get; set; }
        public int KhoangGiaToiThieu { get; set; }
        public int GiaHienTai { get; set; }
        public string ThongTinKhac { get; set; }
        public float NongDo { get; set; }
        public string XuatXu { get; set; }
        public int Nam { get; set; }
        public string MauSac { get; set; }
        public float DungTich { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ThoiGianBatDau { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ThoiGianKetThuc { get; set; }
        public string TinhTrang { get; set; }
        public bool YeuCauKetThuc { get; set; }
        public Guid ShopId { get; set; }
        public Guid LoaiSanPhamId { get; set; }
        public Guid TinhTrangDauGiaId { get; set; }
        public Guid UserId { get; set; }
        public UserView NguoiRaGia { get; set; }
    }

    public class SanPhamDauGiaParamView : PaginationView
    {
        public List<Guid?> LoaiSanPhamId { get; set; }
        public Guid? ShopId { get; set; }
        public List<Guid?> TinhTrangDauGiaId { get; set; }
    }

    public class SanPhamDauGiaListView : ResponseWithPaginationView
    {
        public List<SanPhamDauGiaView> Data { get; set; }
        public SanPhamDauGiaListView(List<SanPhamDauGiaView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }

    public class SanPhamDauGiaWithHinhAnhView
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string TenSanPham { get; set; }
        public int GiaKhoiDiem { get; set; }
        public int KhoangGiaToiThieu { get; set; }
        public int GiaHienTai { get; set; }
        public string ThongTinKhac { get; set; }
        public float NongDo { get; set; }
        public string XuatXu { get; set; }
        public int Nam { get; set; }
        public string MauSac { get; set; }
        public float DungTich { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ThoiGianBatDau { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ThoiGianKetThuc { get; set; }
        public bool YeuCauKetThuc { get; set; }
        public Guid ShopId { get; set; }
        public Guid LoaiSanPhamId { get; set; }
        public string TinhTrang { get; set; }
        public Guid TinhTrangDauGiaId { get; set; }
        public Guid? UserId { get; set; }
        public string TenShop { get; set; }
        public string TenLoaiSanPham { get; set; }
        public UserView NguoiRaGia { get; set; }
        public List<HinhAnhSanPhamDauGiaView> HinhAnhSanPhamDauGia { get; set; }
    }

    public class SanPhamDauGiaWithHinhAnhListView : ResponseWithPaginationView
    {
        public List<SanPhamDauGiaWithHinhAnhView> Data { get; set; }
        public SanPhamDauGiaWithHinhAnhListView(List<SanPhamDauGiaWithHinhAnhView> Data, int TotalRecord, int StatusCode) : base(TotalRecord, StatusCode)
        {
            this.Data = Data;
        }
    }
}
