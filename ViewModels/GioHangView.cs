﻿using System;
using System.Collections.Generic;

namespace Project.ViewModels
{
    public class GioHangView
    {
        public Guid Id { get; set; }
        public int SoLuong { get; set; }
        public int DonGia { get; set; }
        public int GiamGia { get; set; }
        public Guid UserId { get; set; }
        public Guid ShopId { get; set; }
        public Guid? SanPhamId { get; set; }
        public Guid? SanPhamDauGiaId { get; set; }
        public string TenShop { get; set; }
        public string AnhDaiDienShop { get; set; }
        public SanPhamWithHinhAnhView SanPham { get; set; }
        public SanPhamDauGiaWithHinhAnhView SanPhamDauGia { get; set; }
    }

    public class ListSanPhamInGioHangView
    {
        public Guid Id { get; set; }
        public int SoLuong { get; set; }
        public int DonGia { get; set; }
        public int GiamGia { get; set; }
        public Guid UserId { get; set; }
        public Guid? SanPhamId { get; set; }
        public Guid? SanPhamDauGiaId { get; set; }
        public SanPhamWithHinhAnhView SanPham { get; set; }
        public SanPhamDauGiaWithHinhAnhView SanPhamDauGia { get; set; }
    }

    public class GioHangWithListSanPhamView
    {
        public string TenShop { get; set; }
        public string AnhDaiDienShop { get; set; }
        public Guid ShopId { get; set; }
        public List<ListSanPhamInGioHangView> ListSanPhamInGioHang { get; set; }
    }
}
