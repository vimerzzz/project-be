﻿using System;

namespace Project.ViewModels
{
    public class HinhAnhSanPhamView
    {
        public Guid Id { get; set; }
        public string HinhAnh { get; set; }
        public int ThuTu { get; set; }
        public Guid SanPhamId { get; set; }
        public Guid UserId { get; set; }
    }
}
