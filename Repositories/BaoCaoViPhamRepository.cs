﻿using AutoMapper;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Repositories
{
    public interface IBaoCaoViPhamRepository
    {
        public ResponseWithPaginationView GetAllBaoCao(BaoCaoViPhamParamView baoCaoParamView);
        public List<BaoCaoViPhamView> GetAllNguoiBaoCao();
        public List<BaoCaoViPhamView> GetAllNguoiBiBaoCao();
        public List<BaoCaoViPhamView> GetAllShopBiBaoCao();
        public ResponsePostView CreateBaoCao(ref BaoCaoViPhamView baoCaoView);
        public ResponsePostView UpdateBaoCao(BaoCaoViPhamView baoCaoView);
        public ResponsePostView DeleteBaoCao(BaoCaoViPhamView baoCaoView);
    }

    public class BaoCaoViPhamRepository : IBaoCaoViPhamRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public BaoCaoViPhamRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateBaoCao(ref BaoCaoViPhamView baoCaoView)
        {
            try
            {
                var tmpView = baoCaoView;
                var newBaoCao = new BaoCaoViPham();
                newBaoCao = _mapper.Map<BaoCaoViPham>(tmpView);
                newBaoCao.CreatedAt = DateTime.Now;
                newBaoCao.UpdatedAt = DateTime.Now;
                _context.BaoCaoViPhams.Add(newBaoCao);
                _context.SaveChanges();
                baoCaoView.Id = newBaoCao.Id;
                baoCaoView.CreatedAt = newBaoCao.CreatedAt;
                baoCaoView.Shop = (from s in _context.Shops
                                   where s.Id == tmpView.ShopId
                                   select new ShopView
                                   {
                                       UserId = s.UserId,
                                       TenShop = s.TenShop,
                                       AnhDaiDien = s.AnhDaiDien
                                   }).FirstOrDefault();
                baoCaoView.User = (from s in _context.Users
                                   where s.Id == tmpView.UserId
                                   select new UserView
                                   {
                                       UserName = s.UserName,
                                       UserInfo = (from ui in _context.UserInfos
                                                   where ui.UserId == s.Id
                                                   select new UserInfoView
                                                   {
                                                       Ho = ui.Ho,
                                                       TenDem = ui.TenDem,
                                                       Ten = ui.Ten,
                                                       AnhDaiDien = ui.AnhDaiDien
                                                   }).FirstOrDefault()
                                   }).FirstOrDefault();
                baoCaoView.NguoiBaoCao = (from s in _context.Users
                                          where s.Id == tmpView.NguoiBaoCaoId
                                          select new UserView
                                          {
                                              UserName = s.UserName,
                                              UserInfo = (from ui in _context.UserInfos
                                                          where ui.UserId == s.Id
                                                          select new UserInfoView
                                                          {
                                                              Ho = ui.Ho,
                                                              TenDem = ui.TenDem,
                                                              Ten = ui.Ten,
                                                              AnhDaiDien = ui.AnhDaiDien
                                                          }).FirstOrDefault()
                                          }).FirstOrDefault();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteBaoCao(BaoCaoViPhamView baoCaoView)
        {
            try
            {
                var baoCao = _context.BaoCaoViPhams.Where(s => s.Id == baoCaoView.Id).FirstOrDefault();
                if (baoCao == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _context.BaoCaoViPhams.Remove(baoCao);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllBaoCao(BaoCaoViPhamParamView baoCaoParamView)
        {
            try
            {
                var baoCaos =
                    from s in _context.BaoCaoViPhams
                    select new BaoCaoViPhamView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        ShopId = s.ShopId,
                        CreatedAt = s.CreatedAt,
                        DaXem = s.DaXem,
                        LyDoBaoCao = s.LyDoBaoCao,
                        NguoiBaoCaoId = s.NguoiBaoCaoId,
                        NoiGuiBaoCao = s.NoiGuiBaoCao,
                        Shop = (from sh in _context.Shops
                                where sh.Id == s.ShopId
                                select new ShopView
                                {
                                    UserId = sh.UserId,
                                    TenShop = sh.TenShop,
                                    AnhDaiDien = sh.AnhDaiDien
                                }).FirstOrDefault(),
                        User = (from u in _context.Users
                                where u.Id == s.UserId
                                select new UserView
                                {
                                    UserName = u.UserName,
                                    UserInfo = (from ui in _context.UserInfos
                                                where ui.UserId == u.Id
                                                select new UserInfoView
                                                {
                                                    Ho = ui.Ho,
                                                    TenDem = ui.TenDem,
                                                    Ten = ui.Ten,
                                                    AnhDaiDien = ui.AnhDaiDien
                                                }).FirstOrDefault()
                                }).FirstOrDefault(),
                        NguoiBaoCao = (from u in _context.Users
                                       where u.Id == s.NguoiBaoCaoId
                                       select new UserView
                                       {
                                           UserName = u.UserName,
                                           UserInfo = (from ui in _context.UserInfos
                                                       where ui.UserId == u.Id
                                                       select new UserInfoView
                                                       {
                                                           Ho = ui.Ho,
                                                           TenDem = ui.TenDem,
                                                           Ten = ui.Ten,
                                                           AnhDaiDien = ui.AnhDaiDien
                                                       }).FirstOrDefault()
                                       }).FirstOrDefault()
                    };
                if (!baoCaos.Any())
                {
                    return new BaoCaoViPhamListView(null, 0, 404);
                }
                if (baoCaoParamView.SearchString != null)
                {
                    baoCaos = baoCaos.Where(s => s.LyDoBaoCao.Contains(baoCaoParamView.SearchString));
                }
                if (baoCaoParamView.DaXem != null)
                {
                    baoCaos = baoCaos.Where(s => s.DaXem == baoCaoParamView.DaXem);
                }
                if (baoCaoParamView.UserId != null)
                {
                    baoCaos = baoCaos.Where(s => s.UserId == baoCaoParamView.UserId);
                }
                if (baoCaoParamView.ShopId != null)
                {
                    baoCaos = baoCaos.Where(s => s.ShopId == baoCaoParamView.ShopId);
                }
                if (baoCaoParamView.NguoiBaoCaoId != null)
                {
                    baoCaos = baoCaos.Where(s => s.NguoiBaoCaoId == baoCaoParamView.NguoiBaoCaoId);
                }
                if (baoCaoParamView.Order == "asc")
                {
                    if (baoCaoParamView.OrderBy == "createdAt")
                    {
                        baoCaos = baoCaos.OrderBy(s => s.CreatedAt);
                    }
                }
                else if (baoCaoParamView.Order == "desc")
                {
                    if (baoCaoParamView.OrderBy == "createdAt")
                    {
                        baoCaos = baoCaos.OrderByDescending(s => s.CreatedAt);
                    }
                }
                var totalRecord = baoCaos.ToList().Count();
                if (baoCaoParamView.PageIndex > 0)
                {
                    baoCaos = baoCaos.Skip(baoCaoParamView.PageSize * (baoCaoParamView.PageIndex - 1)).Take(baoCaoParamView.PageSize);
                }
                var listBaoCao = baoCaos.ToList();
                return new BaoCaoViPhamListView(listBaoCao, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new BaoCaoViPhamListView(null, 0, 400);
            }
        }

        public List<BaoCaoViPhamView> GetAllNguoiBaoCao()
        {
            try
            {
                var baoCaos =
                    from s in _context.BaoCaoViPhams
                    select new BaoCaoViPhamView
                    {
                        NguoiBaoCaoId = s.NguoiBaoCaoId,
                        NguoiBaoCao = (from u in _context.Users
                                       where u.Id == s.NguoiBaoCaoId
                                       select new UserView
                                       {
                                           UserName = u.UserName,
                                           UserInfo = (from ui in _context.UserInfos
                                                       where ui.UserId == u.Id
                                                       select new UserInfoView
                                                       {
                                                           Ho = ui.Ho,
                                                           TenDem = ui.TenDem,
                                                           Ten = ui.Ten,
                                                           AnhDaiDien = ui.AnhDaiDien
                                                       }).FirstOrDefault()
                                       }).FirstOrDefault()
                    };
                if (!baoCaos.Any())
                {
                    return null;
                }
                var listBaoCao = baoCaos.Distinct().ToList();
                return listBaoCao;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public List<BaoCaoViPhamView> GetAllNguoiBiBaoCao()
        {
            try
            {
                var baoCaos =
                    from s in _context.BaoCaoViPhams
                    where s.UserId != null
                    select new BaoCaoViPhamView
                    {
                        UserId = s.UserId,
                        User = (from u in _context.Users
                                where u.Id == s.UserId
                                select new UserView
                                {
                                    UserName = u.UserName,
                                    UserInfo = (from ui in _context.UserInfos
                                                where ui.UserId == u.Id
                                                select new UserInfoView
                                                {
                                                    Ho = ui.Ho,
                                                    TenDem = ui.TenDem,
                                                    Ten = ui.Ten,
                                                    AnhDaiDien = ui.AnhDaiDien
                                                }).FirstOrDefault()
                                }).FirstOrDefault()
                    };
                if (!baoCaos.Any())
                {
                    return null;
                }
                var listBaoCao = baoCaos.Distinct().ToList();
                return listBaoCao;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public List<BaoCaoViPhamView> GetAllShopBiBaoCao()
        {
            try
            {
                var baoCaos =
                    from s in _context.BaoCaoViPhams
                    where s.ShopId != null
                    select new BaoCaoViPhamView
                    {
                        ShopId = s.ShopId,
                        Shop = (from sh in _context.Shops
                                where sh.Id == s.ShopId
                                select new ShopView
                                {
                                    UserId = sh.UserId,
                                    TenShop = sh.TenShop,
                                    AnhDaiDien = sh.AnhDaiDien
                                }).FirstOrDefault()
                    };
                if (!baoCaos.Any())
                {
                    return null;
                }
                var listBaoCao = baoCaos.Distinct().ToList();
                return listBaoCao;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateBaoCao(BaoCaoViPhamView baoCaoView)
        {
            try
            {
                var baoCao = _context.BaoCaoViPhams.Where(s => s.Id == baoCaoView.Id).FirstOrDefault();
                if (baoCao == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                baoCao.DaXem = baoCaoView.DaXem;
                baoCao.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
