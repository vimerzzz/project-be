﻿using AutoMapper;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface IHinhAnhSanPhamDauGiaRepository
    {
        public List<HinhAnhSanPhamDauGiaView> GetAllHinhAnhSanPhamDauGiaBySanPhamId(Guid sanPhamDauGiaId);
        public ResponsePostView CreateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews);
        public ResponsePostView UpdateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews);
        public ResponsePostView DeleteHinhAnhSanPhamDauGia(List<Guid> hinhAnhSanPhamDauGiaId, Guid userId);
    }

    public class HinhAnhSanPhamDauGiaRepository : IHinhAnhSanPhamDauGiaRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public HinhAnhSanPhamDauGiaRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews)
        {
            try
            {
                var hinhAnhSanPhamDauGias = _context.HinhAnhSanPhamDauGias.Where(s => s.SanPhamDauGiaId == hinhAnhSanPhamDauGiaViews[0].SanPhamDauGiaId).ToList();
                var listThuTu = new List<int>();
                foreach (var item in hinhAnhSanPhamDauGias)
                {
                    listThuTu.Add(item.ThuTu);
                }
                var index = 1;
                foreach (var item in hinhAnhSanPhamDauGiaViews)
                {
                    var newHinhAnhSanPhamDauGia = new HinhAnhSanPhamDauGia();
                    newHinhAnhSanPhamDauGia = _mapper.Map<HinhAnhSanPhamDauGia>(item);
                    newHinhAnhSanPhamDauGia.CreatedAt = DateTime.Now;
                    newHinhAnhSanPhamDauGia.UpdatedAt = DateTime.Now;
                    while (listThuTu.Exists(s => s == index))
                    {
                        index++;
                    }
                    newHinhAnhSanPhamDauGia.ThuTu = index;
                    index++;
                    _context.HinhAnhSanPhamDauGias.Add(newHinhAnhSanPhamDauGia);
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteHinhAnhSanPhamDauGia(List<Guid> hinhAnhSanPhamDauGiaId, Guid userId)
        {
            try
            {
                var files = new List<string>();
                foreach (var item in hinhAnhSanPhamDauGiaId)
                {
                    var hinhAnhSanPhamDauGia = _context.HinhAnhSanPhamDauGias.Where(s => s.Id == item).FirstOrDefault();
                    if (hinhAnhSanPhamDauGia == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    var sanPham = _context.SanPhamDauGias.Where(s => s.Id == hinhAnhSanPhamDauGia.SanPhamDauGiaId).FirstOrDefault();
                    if (sanPham == null)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    var shop = _context.Shops.Where(s => s.Id == sanPham.ShopId).FirstOrDefault();
                    if (shop == null)
                    {
                        return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                    }
                    if (shop.UserId != userId)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    var folderName = Path.Combine("Statics", "Images", "Auctions", shop.Id.ToString(), sanPham.Id.ToString());
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    var removePath = Path.Combine(pathToSave, hinhAnhSanPhamDauGia.HinhAnh.Split("\\").Last());
                    files.Add(removePath);
                    _context.HinhAnhSanPhamDauGias.Remove(hinhAnhSanPhamDauGia);
                }
                _context.SaveChanges();
                foreach (var file in files)
                {
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                }
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public List<HinhAnhSanPhamDauGiaView> GetAllHinhAnhSanPhamDauGiaBySanPhamId(Guid sanPhamDauGiaId)
        {
            try
            {
                var hinhAnhSanPhamDauGias =
                    from s in _context.HinhAnhSanPhamDauGias
                    join sp in _context.SanPhamDauGias on s.SanPhamDauGiaId equals sp.Id
                    join sh in _context.Shops on sp.ShopId equals sh.Id
                    where s.SanPhamDauGiaId == sanPhamDauGiaId
                    orderby s.ThuTu
                    select new HinhAnhSanPhamDauGiaView
                    {
                        Id = s.Id,
                        SanPhamDauGiaId = s.SanPhamDauGiaId,
                        HinhAnh = s.HinhAnh,
                        UserId = sh.UserId,
                        ThuTu = s.ThuTu
                    };
                if (!hinhAnhSanPhamDauGias.Any())
                {
                    return null;
                }
                var listHinhAnhSanPhamDauGia = hinhAnhSanPhamDauGias.ToList();
                return listHinhAnhSanPhamDauGia;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews)
        {
            try
            {
                foreach (var item in hinhAnhSanPhamDauGiaViews)
                {
                    var hinhAnhSanPhamDauGia = _context.HinhAnhSanPhamDauGias.Where(s => s.Id == item.Id).FirstOrDefault();
                    if (hinhAnhSanPhamDauGia == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    var sanPham = _context.SanPhamDauGias.Where(s => s.Id == hinhAnhSanPhamDauGia.SanPhamDauGiaId).FirstOrDefault();
                    if (sanPham == null)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    var shop = _context.Shops.Where(s => s.Id == sanPham.ShopId).FirstOrDefault();
                    if (shop == null)
                    {
                        return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                    }
                    if (shop.UserId != item.UserId)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    _mapper.Map(item, hinhAnhSanPhamDauGia);
                    hinhAnhSanPhamDauGia.UpdatedAt = DateTime.Now;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }
    }
}
