﻿using AutoMapper;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface IHinhAnhSanPhamRepository
    {
        public List<HinhAnhSanPhamView> GetAllHinhAnhSanPhamBySanPhamId(Guid sanPhamId);
        public ResponsePostView CreateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews);
        public ResponsePostView UpdateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews);
        public ResponsePostView DeleteHinhAnhSanPham(List<Guid> hinhAnhSanPhamId, Guid userId);
    }

    public class HinhAnhSanPhamRepository : IHinhAnhSanPhamRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public HinhAnhSanPhamRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews)
        {
            try
            {
                var hinhAnhSanPhams = _context.HinhAnhSanPhams.Where(s => s.SanPhamId == hinhAnhSanPhamViews[0].SanPhamId).ToList();
                var listThuTu = new List<int>();
                foreach(var item in hinhAnhSanPhams)
                {
                    listThuTu.Add(item.ThuTu);
                }
                var index = 1;
                foreach (var item in hinhAnhSanPhamViews)
                {
                    var newHinhAnhSanPham = new HinhAnhSanPham();
                    newHinhAnhSanPham = _mapper.Map<HinhAnhSanPham>(item);
                    newHinhAnhSanPham.CreatedAt = DateTime.Now;
                    newHinhAnhSanPham.UpdatedAt = DateTime.Now;
                    while(listThuTu.Exists(s => s == index))
                    {
                        index++;
                    }
                    newHinhAnhSanPham.ThuTu = index;
                    index++;
                    _context.HinhAnhSanPhams.Add(newHinhAnhSanPham);
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteHinhAnhSanPham(List<Guid> hinhAnhSanPhamId, Guid userId)
        {
            try
            {
                var files = new List<string>();
                foreach(var item in hinhAnhSanPhamId)
                {
                    var hinhAnhSanPham = _context.HinhAnhSanPhams.Where(s => s.Id == item).FirstOrDefault();
                    if (hinhAnhSanPham == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    var sanPham = _context.SanPhams.Where(s => s.Id == hinhAnhSanPham.SanPhamId).FirstOrDefault();
                    if (sanPham == null)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    var shop = _context.Shops.Where(s => s.Id == sanPham.ShopId).FirstOrDefault();
                    if (shop == null)
                    {
                        return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                    }
                    if (shop.UserId != userId)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    var folderName = Path.Combine("Statics", "Images", "Items", shop.Id.ToString(), sanPham.Id.ToString());
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    var removePath = Path.Combine(pathToSave, hinhAnhSanPham.HinhAnh.Split("\\").Last());
                    files.Add(removePath);
                    _context.HinhAnhSanPhams.Remove(hinhAnhSanPham);
                }
                _context.SaveChanges();
                foreach(var file in files)
                {
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                }
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public List<HinhAnhSanPhamView> GetAllHinhAnhSanPhamBySanPhamId(Guid sanPhamid)
        {
            try
            {
                var hinhAnhSanPhams = 
                    from s in _context.HinhAnhSanPhams
                    join sp in _context.SanPhams on s.SanPhamId equals sp.Id
                    join sh in _context.Shops on sp.ShopId equals sh.Id
                    where s.SanPhamId == sanPhamid
                    orderby s.ThuTu
                    select new HinhAnhSanPhamView
                    {
                        Id = s.Id,
                        SanPhamId = s.SanPhamId,
                        HinhAnh = s.HinhAnh,
                        UserId = sh.UserId,
                        ThuTu = s.ThuTu
                    };
                if (!hinhAnhSanPhams.Any())
                {
                    return null;
                }
                var listHinhAnhSanPham = hinhAnhSanPhams.ToList();
                return listHinhAnhSanPham;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews)
        {
            try
            {
                foreach (var item in hinhAnhSanPhamViews)
                {
                    var hinhAnhSanPham = _context.HinhAnhSanPhams.Where(s => s.Id == item.Id).FirstOrDefault();
                    if (hinhAnhSanPham == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    var sanPham = _context.SanPhams.Where(s => s.Id == hinhAnhSanPham.SanPhamId).FirstOrDefault();
                    if (sanPham == null)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    var shop = _context.Shops.Where(s => s.Id == sanPham.ShopId).FirstOrDefault();
                    if (shop == null)
                    {
                        return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                    }
                    if (shop.UserId != item.UserId)
                    {
                        return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                    }
                    _mapper.Map(item, hinhAnhSanPham);
                    hinhAnhSanPham.UpdatedAt = DateTime.Now;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }
    }
}
