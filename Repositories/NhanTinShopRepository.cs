﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface INhanTinShopRepository
    {
        public ResponseWithPaginationView GetAllNhanTinShop(NhanTinShopParamView nhanTinParamView);
        public ResponseIdPostView CreateNhanTinShop(NhanTinShopView nhanTinShopView);
        public ResponsePostView UpdateNhanTinShop(List<NhanTinShopView> nhanTinShopView);
        public ResponseWithPaginationView GetAllShopId(NhanTinShopParamView nhanTinParamView);
        public ResponseWithPaginationView GetAllUserId(NhanTinShopParamView nhanTinParamView);
        public ResponseNoiDungPostView UploadFile(NhanTinShopView nhanTinShopView, IFormFileCollection files);
    }

    public class NhanTinShopRepository : INhanTinShopRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public NhanTinShopRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponseIdPostView CreateNhanTinShop(NhanTinShopView nhanTinShopView)
        {
            try
            {
                var nhanTin = _context.NhanTinShops.Where(s => s.UserId == nhanTinShopView.UserId && s.ShopId == nhanTinShopView.ShopId).FirstOrDefault();
                var chatBoxId = Guid.NewGuid();
                if(nhanTin != null)
                {
                    chatBoxId = nhanTin.ChatBoxId;
                }
                var newNhanTin = new NhanTinShop();
                newNhanTin = _mapper.Map<NhanTinShop>(nhanTinShopView);
                newNhanTin.CreatedAt = DateTime.Now;
                newNhanTin.UpdatedAt = DateTime.Now;
                newNhanTin.ChatBoxId = chatBoxId;
                _context.NhanTinShops.Add(newNhanTin);
                _context.SaveChanges();
                return new ResponseIdPostView(MessageConstants.CREATE_SUCCESS, 200, newNhanTin.Id);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponseIdPostView(MessageConstants.CREATE_FAILED, 400, null);
            }
        }

        public ResponseWithPaginationView GetAllNhanTinShop(NhanTinShopParamView nhanTinParamView)
        {
            try
            {
                var nhanTins =
                    from s in _context.NhanTinShops
                    where s.UserId == nhanTinParamView.UserId &&
                        s.ShopId == nhanTinParamView.ShopId
                    orderby s.CreatedAt descending
                    select new NhanTinShopView
                    {
                        CreatedAt = s.CreatedAt,
                        ShopId = s.ShopId,
                        UserId = s.UserId,
                        Id = s.Id,
                        NoiDung = s.NoiDung,
                        NguoiGuiId = s.NguoiGuiId,
                        DaXem = s.DaXem,
                        FileName = s.FileName,
                        IsFile = s.IsFile,
                        IsImage = s.IsImage
                    };
                if(!nhanTins.Any())
                {
                    return new NhanTinShopListView(null, 0, 404);
                }
                if (nhanTinParamView.DaXem != null)
                {
                    nhanTins = nhanTins.Where(s => s.DaXem == nhanTinParamView.DaXem);
                }
                var totalRecord = nhanTins.ToList().Count();
                if (nhanTinParamView.PageIndex > 0)
                {
                    nhanTins = nhanTins.Skip(nhanTinParamView.PageSize * (nhanTinParamView.PageIndex - 1)).Take(nhanTinParamView.PageSize);
                }
                var listNhanTin = nhanTins.ToList();
                return new NhanTinShopListView(listNhanTin, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new NhanTinShopListView(null, 0, 400);
            }
        }

        public ResponseWithPaginationView GetAllShopId(NhanTinShopParamView nhanTinParamView)
        {
            try
            {
                var nhanTins =
                    from s in _context.NhanTinShops
                    where s.UserId == nhanTinParamView.UserId
                    orderby s.CreatedAt descending
                    select new NhanTinShopView
                    {
                        ShopId = s.ShopId,
                        UserId = s.UserId
                    };
                if (!nhanTins.Any())
                {
                    return new NhanTinShopListView(null, 0, 404);
                }
                var groupNhanTins = nhanTins.ToList().GroupBy(s => new
                {
                    s.UserId,
                    s.ShopId
                }).Select(g => new NhanTinShopView
                {
                    UserId = g.Key.UserId,
                    ShopId = g.Key.ShopId
                });
                var totalRecord = groupNhanTins.ToList().Count();
                if (nhanTinParamView.PageIndex > 0)
                {
                    groupNhanTins = groupNhanTins.Skip(nhanTinParamView.PageSize * (nhanTinParamView.PageIndex - 1)).Take(nhanTinParamView.PageSize);
                }
                var listNhanTin = groupNhanTins.ToList();
                return new NhanTinShopListView(listNhanTin, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new NhanTinShopListView(null, 0, 400);
            }
        }

        public ResponseWithPaginationView GetAllUserId(NhanTinShopParamView nhanTinParamView)
        {
            try
            {
                var nhanTins =
                    from s in _context.NhanTinShops
                    where s.ShopId == nhanTinParamView.ShopId
                    orderby s.CreatedAt descending
                    select new NhanTinShopView
                    {
                        ShopId = s.ShopId,
                        UserId = s.UserId
                    };
                if (!nhanTins.Any())
                {
                    return new NhanTinShopListView(null, 0, 404);
                }
                var groupNhanTins = nhanTins.ToList().GroupBy(s => new
                {
                    s.UserId,
                    s.ShopId
                }).Select(g => new NhanTinShopView
                {
                    UserId = g.Key.UserId,
                    ShopId = g.Key.ShopId
                });
                var totalRecord = groupNhanTins.ToList().Count();
                if (nhanTinParamView.PageIndex > 0)
                {
                    groupNhanTins = groupNhanTins.Skip(nhanTinParamView.PageSize * (nhanTinParamView.PageIndex - 1)).Take(nhanTinParamView.PageSize);
                }
                var listNhanTin = groupNhanTins.ToList();
                return new NhanTinShopListView(listNhanTin, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new NhanTinShopListView(null, 0, 400);
            }
        }

        public ResponsePostView UpdateNhanTinShop(List<NhanTinShopView> nhanTinShopView)
        {
            try
            {
                foreach(var item in nhanTinShopView)
                {
                    var nhanTin = _context.NhanTinShops.Where(s => s.Id == item.Id).FirstOrDefault();
                    if (nhanTin == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    nhanTin.DaXem = item.DaXem;
                    nhanTin.UpdatedAt = DateTime.Now;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponseNoiDungPostView UploadFile(NhanTinShopView nhanTinShopView, IFormFileCollection files)
        {
            try
            {
                if (files.Count > 0)
                {
                    var file = files[0];
                    if (file.Length > 0)
                    {
                        var nhanTin = _context.NhanTinShops.Where(s => s.UserId == nhanTinShopView.UserId && s.ShopId == nhanTinShopView.ShopId).FirstOrDefault();
                        var chatBoxId = Guid.NewGuid();
                        if (nhanTin != null)
                        {
                            chatBoxId = nhanTin.ChatBoxId;
                        }
                        var folderName = Path.Combine("Statics", "Files", "Chats", chatBoxId.ToString());
                        var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                        Directory.CreateDirectory(pathToSave);
                        var fileName = file.FileName;
                        var name = "";
                        for (int i = 0; i < fileName.Split('.').Count(); i++)
                        {
                            if (i < fileName.Split('.').Count() - 1)
                            {
                                name += fileName.Split('.')[i];
                            }
                        }
                        var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                        var fullPath = Path.Combine(pathToSave, fileRename);
                        var dbPath = Path.Combine(folderName, fileRename);
                        var newNhanTin = new NhanTinShop();
                        newNhanTin = _mapper.Map<NhanTinShop>(nhanTinShopView);
                        newNhanTin.CreatedAt = DateTime.Now;
                        newNhanTin.UpdatedAt = DateTime.Now;
                        newNhanTin.ChatBoxId = chatBoxId;
                        newNhanTin.NoiDung = dbPath;
                        newNhanTin.FileName = fileName;
                        _context.NhanTinShops.Add(newNhanTin);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        _context.SaveChanges();
                        return new ResponseNoiDungPostView(MessageConstants.CREATE_SUCCESS, 200, dbPath, newNhanTin.Id);
                    }
                    else
                    {
                        return new ResponseNoiDungPostView(MessageConstants.CREATE_FAILED, 400, null, null);
                    }
                }
                else
                {
                    return new ResponseNoiDungPostView(MessageConstants.CREATE_FAILED, 400, null, null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponseNoiDungPostView(MessageConstants.CREATE_FAILED, 400, null, null);
            }
        }
    }
}
