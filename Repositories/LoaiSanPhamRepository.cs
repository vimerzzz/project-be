﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Linq;

namespace Project.Repositories
{
    public interface ILoaiSanPhamRepository
    {
        public ResponseWithPaginationView GetAllLoaiSanPham(LoaiSanPhamParamView loaiSanPhamParamView);
        public ResponsePostView CreateLoaiSanPham(LoaiSanPhamView loaiSanPhamView);
        public ResponsePostView UpdateLoaiSanPham(LoaiSanPhamView loaiSanPhamView);
        public ResponsePostView DeleteLoaiSanPham(Guid id);
    }

    public class LoaiSanPhamRepository : ILoaiSanPhamRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public LoaiSanPhamRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateLoaiSanPham(LoaiSanPhamView loaiSanPhamView)
        {
            try
            {
                var newLoaiSanPham = new LoaiSanPham();
                newLoaiSanPham = _mapper.Map<LoaiSanPham>(loaiSanPhamView);
                newLoaiSanPham.CreatedAt = DateTime.Now;
                newLoaiSanPham.UpdatedAt = DateTime.Now;
                _context.LoaiSanPhams.Add(newLoaiSanPham);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteLoaiSanPham(Guid id)
        {
            try
            {
                var loaiSanPham = _context.LoaiSanPhams.Where(s => s.Id == id).FirstOrDefault();
                if (loaiSanPham == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _context.LoaiSanPhams.Remove(loaiSanPham);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllLoaiSanPham(LoaiSanPhamParamView loaiSanPhamParamView)
        {
            try
            {
                var loaiSanPhams =
                    from s in _context.LoaiSanPhams
                    select new LoaiSanPhamView
                    {
                        Id = s.Id,
                        MoTaLoaiSanPham = s.MoTaLoaiSanPham,
                        TenLoaiSanPham = s.TenLoaiSanPham
                    };
                if (!loaiSanPhams.Any())
                {
                    return new LoaiSanPhamListView(null, 0, 404);
                }
                if (loaiSanPhamParamView.SearchString != null)
                {
                    loaiSanPhams = loaiSanPhams.Where(s => (s.MoTaLoaiSanPham.Contains(loaiSanPhamParamView.SearchString) || s.TenLoaiSanPham.Contains(loaiSanPhamParamView.SearchString)));
                }
                if (loaiSanPhamParamView.Order == "asc")
                {
                    if (loaiSanPhamParamView.OrderBy == "tenLoaiSanPham")
                    {
                        loaiSanPhams = loaiSanPhams.OrderBy(s => s.TenLoaiSanPham);
                    }
                    else if (loaiSanPhamParamView.OrderBy == "moTaLoaiSanPham")
                    {
                        loaiSanPhams = loaiSanPhams.OrderBy(s => s.MoTaLoaiSanPham);
                    }
                }
                else if (loaiSanPhamParamView.Order == "desc")
                {
                    if (loaiSanPhamParamView.OrderBy == "tenLoaiSanPham")
                    {
                        loaiSanPhams = loaiSanPhams.OrderByDescending(s => s.TenLoaiSanPham);
                    }
                    else if (loaiSanPhamParamView.OrderBy == "moTaLoaiSanPham")
                    {
                        loaiSanPhams = loaiSanPhams.OrderByDescending(s => s.MoTaLoaiSanPham);
                    }
                }
                var totalRecord = loaiSanPhams.ToList().Count();
                if (loaiSanPhamParamView.PageIndex > 0)
                {
                    loaiSanPhams = loaiSanPhams.Skip(loaiSanPhamParamView.PageSize * (loaiSanPhamParamView.PageIndex - 1)).Take(loaiSanPhamParamView.PageSize);
                }
                var listLoaiSanPham = loaiSanPhams.ToList();
                return new LoaiSanPhamListView(listLoaiSanPham, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new LoaiSanPhamListView(null, 0, 400);
            }
        }

        public ResponsePostView UpdateLoaiSanPham(LoaiSanPhamView loaiSanPhamView)
        {
            try
            {
                var loaiSanPham = _context.LoaiSanPhams.Where(s => s.Id == loaiSanPhamView.Id).FirstOrDefault();
                if (loaiSanPham == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _mapper.Map(loaiSanPhamView, loaiSanPham);
                loaiSanPham.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
