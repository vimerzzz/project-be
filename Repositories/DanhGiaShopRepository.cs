﻿using AutoMapper;
using LinqKit;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Repositories
{
    public interface IDanhGiaShopRepository
    {
        public ResponseWithPaginationView GetAllDanhGia(DanhGiaShopParamView danhGiaShopParamView);
        public ResponsePostView CreateDanhGia(DanhGiaShopView danhGiaShopView);
        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView);
        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView);
        public List<DanhGiaShopHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId);
    }

    public class DanhGiaShopRepository : IDanhGiaShopRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public DanhGiaShopRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateDanhGia(DanhGiaShopView danhGiaShopView)
        {
            try
            {
                var newDanhGia = new DanhGiaShop();
                newDanhGia = _mapper.Map<DanhGiaShop>(danhGiaShopView);
                newDanhGia.CreatedAt = DateTime.Now;
                newDanhGia.UpdatedAt = DateTime.Now;
                _context.DanhGiaShops.Add(newDanhGia);
                var shop = _context.Shops.Where(s => s.Id == danhGiaShopView.ShopId).FirstOrDefault();
                if(shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                switch (danhGiaShopView.DanhGia)
                {
                    case 1:
                        {
                            shop.MotSao++;
                            shop.SoNguoiDanhGia++;
                            break;
                        }
                    case 2:
                        {
                            shop.HaiSao++;
                            shop.SoNguoiDanhGia++;
                            break;
                        }
                    case 3:
                        {
                            shop.BaSao++;
                            shop.SoNguoiDanhGia++;
                            break;
                        }
                    case 4:
                        {
                            shop.BonSao++;
                            shop.SoNguoiDanhGia++;
                            break;
                        }
                    case 5:
                        {
                            shop.NamSao++;
                            shop.SoNguoiDanhGia++;
                            break;
                        }
                    default:
                        {
                            return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
                        }
                }
                var danhGiaTmp = (shop.DanhGiaChung * (shop.SoNguoiDanhGia - 1) + danhGiaShopView.DanhGia) / shop.SoNguoiDanhGia;
                shop.DanhGiaChung = float.Parse(danhGiaTmp.ToString("0.0"));
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView)
        {
            try
            {
                var newDanhGiaHuuIch = new DanhGiaShopHuuIch();
                newDanhGiaHuuIch = _mapper.Map<DanhGiaShopHuuIch>(danhGiaShopHuuIchView);
                newDanhGiaHuuIch.CreatedAt = DateTime.Now;
                newDanhGiaHuuIch.UpdatedAt = DateTime.Now;
                _context.DanhGiaShopHuuIchs.Add(newDanhGiaHuuIch);
                var danhGia = _context.DanhGiaShops.Where(s => s.Id == danhGiaShopHuuIchView.DanhGiaShopId).FirstOrDefault();
                if (danhGia == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                danhGia.HuuIch++;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView)
        {
            try
            {
                var danhGiaHuuIch = _context.DanhGiaShopHuuIchs.Where(s => s.Id == danhGiaShopHuuIchView.Id).FirstOrDefault();
                if (danhGiaHuuIch == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if (danhGiaHuuIch.UserId != danhGiaShopHuuIchView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _context.DanhGiaShopHuuIchs.Remove(danhGiaHuuIch);
                var danhGia = _context.DanhGiaShops.Where(s => s.Id == danhGiaHuuIch.DanhGiaShopId).FirstOrDefault();
                if (danhGia == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                danhGia.HuuIch = danhGia.HuuIch == 0 ? 0 : danhGia.HuuIch - 1;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllDanhGia(DanhGiaShopParamView danhGiaShopParamView)
        {
            try
            {
                var danhGias =
                    from s in _context.DanhGiaShops
                    where s.ShopId == danhGiaShopParamView.ShopId
                    select new DanhGiaShopView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        ShopId = s.ShopId,
                        CreatedAt = s.CreatedAt,
                        DanhGia = s.DanhGia,
                        NhanXet = s.NhanXet,
                        HuuIch = s.HuuIch,
                        User = (from u in _context.Users
                                where u.Id == s.UserId
                                select new UserView
                                {
                                    UserName = u.UserName,
                                    UserInfo = (from ui in _context.UserInfos
                                                where ui.UserId == u.Id
                                                select new UserInfoView
                                                {
                                                    Ho = ui.Ho,
                                                    TenDem = ui.TenDem,
                                                    Ten = ui.Ten,
                                                    AnhDaiDien = ui.AnhDaiDien
                                                }).FirstOrDefault()
                                }).FirstOrDefault()
                    };
                if (!danhGias.Any())
                {
                    return new DanhGiaShopListView(null, 0, 404);
                }
                if(danhGiaShopParamView.DanhGia !=  null)
                {
                    var predicate = PredicateBuilder.New<DanhGiaShopView>(false);
                    foreach (var subItem in danhGiaShopParamView.DanhGia)
                    {
                        predicate = predicate.Or(s => s.DanhGia == subItem);
                    }
                    danhGias = danhGias.Where(predicate);
                }
                if (danhGiaShopParamView.Order == "asc")
                {
                    if (danhGiaShopParamView.OrderBy == "createdAt")
                    {
                        danhGias = danhGias.OrderBy(s => s.CreatedAt);
                    }
                    if (danhGiaShopParamView.OrderBy == "huuIch")
                    {
                        danhGias = danhGias.OrderBy(s => s.HuuIch)
                                            .ThenByDescending(s => s.CreatedAt);
                    }
                }
                else if (danhGiaShopParamView.Order == "desc")
                {
                    if (danhGiaShopParamView.OrderBy == "createdAt")
                    {
                        danhGias = danhGias.OrderByDescending(s => s.CreatedAt);
                    }
                    if (danhGiaShopParamView.OrderBy == "huuIch")
                    {
                        danhGias = danhGias.OrderByDescending(s => s.HuuIch)
                                            .ThenByDescending(s => s.CreatedAt);
                    }
                }
                var totalRecord = danhGias.ToList().Count();
                if (danhGiaShopParamView.PageIndex > 0)
                {
                    danhGias = danhGias.Skip(danhGiaShopParamView.PageSize * (danhGiaShopParamView.PageIndex - 1)).Take(danhGiaShopParamView.PageSize);
                }
                var listDanhGia = danhGias.ToList();
                return new DanhGiaShopListView(listDanhGia, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new DanhGiaShopListView(null, 0, 400);
            }
        }

        public List<DanhGiaShopHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId)
        {
            var danhGiaHuuIchs =
                from s in _context.DanhGiaShopHuuIchs
                where s.UserId == userId
                select new DanhGiaShopHuuIchView
                {
                    UserId = s.UserId,
                    DanhGiaShopId = s.DanhGiaShopId,
                    Id = s.Id
                };
            if (!danhGiaHuuIchs.Any())
            {
                return null;
            }
            return danhGiaHuuIchs.ToList();
        }
    }
}
