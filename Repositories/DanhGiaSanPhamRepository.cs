﻿using AutoMapper;
using LinqKit;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Repositories
{
    public interface IDanhGiaSanPhamRepository
    {
        public ResponseWithPaginationView GetAllDanhGia(DanhGiaSanPhamParamView danhGiaSanPhamParamView);
        public ResponsePostView CreateDanhGia(DanhGiaSanPhamView danhGiaSanPhamView);
        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView);
        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView);
        public List<DanhGiaSanPhamHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId);
    }

    public class DanhGiaSanPhamRepository : IDanhGiaSanPhamRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public DanhGiaSanPhamRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateDanhGia(DanhGiaSanPhamView danhGiaSanPhamView)
        {
            try
            {
                var newDanhGia = new DanhGiaSanPham();
                newDanhGia = _mapper.Map<DanhGiaSanPham>(danhGiaSanPhamView);
                newDanhGia.CreatedAt = DateTime.Now;
                newDanhGia.UpdatedAt = DateTime.Now;
                _context.DanhGiaSanPhams.Add(newDanhGia);
                var sanPham = _context.SanPhams.Where(s => s.Id == danhGiaSanPhamView.SanPhamId).FirstOrDefault();
                if (sanPham == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                switch (danhGiaSanPhamView.DanhGia)
                {
                    case 1:
                        {
                            sanPham.MotSao++;
                            sanPham.SoNguoiDanhGia++;
                            break;
                        }
                    case 2:
                        {
                            sanPham.HaiSao++;
                            sanPham.SoNguoiDanhGia++;
                            break;
                        }
                    case 3:
                        {
                            sanPham.BaSao++;
                            sanPham.SoNguoiDanhGia++;
                            break;
                        }
                    case 4:
                        {
                            sanPham.BonSao++;
                            sanPham.SoNguoiDanhGia++;
                            break;
                        }
                    case 5:
                        {
                            sanPham.NamSao++;
                            sanPham.SoNguoiDanhGia++;
                            break;
                        }
                    default:
                        {
                            return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
                        }
                }
                var danhGiaTmp = (sanPham.DanhGiaChung * (sanPham.SoNguoiDanhGia - 1) + danhGiaSanPhamView.DanhGia) / sanPham.SoNguoiDanhGia;
                sanPham.DanhGiaChung = float.Parse(danhGiaTmp.ToString("0.0"));
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView)
        {
            try
            {
                var newDanhGiaHuuIch = new DanhGiaSanPhamHuuIch();
                newDanhGiaHuuIch = _mapper.Map<DanhGiaSanPhamHuuIch>(danhGiaSanPhamHuuIchView);
                newDanhGiaHuuIch.CreatedAt = DateTime.Now;
                newDanhGiaHuuIch.UpdatedAt = DateTime.Now;
                _context.DanhGiaSanPhamHuuIchs.Add(newDanhGiaHuuIch);
                var danhGia = _context.DanhGiaSanPhams.Where(s => s.Id == danhGiaSanPhamHuuIchView.DanhGiaSanPhamId).FirstOrDefault();
                if(danhGia == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                danhGia.HuuIch++;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView)
        {
            try
            {
                var danhGiaHuuIch = _context.DanhGiaSanPhamHuuIchs.Where(s => s.Id == danhGiaSanPhamHuuIchView.Id).FirstOrDefault();
                if(danhGiaHuuIch == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if (danhGiaHuuIch.UserId != danhGiaSanPhamHuuIchView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _context.DanhGiaSanPhamHuuIchs.Remove(danhGiaHuuIch);
                var danhGia = _context.DanhGiaSanPhams.Where(s => s.Id == danhGiaHuuIch.DanhGiaSanPhamId).FirstOrDefault();
                if (danhGia == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                danhGia.HuuIch = danhGia.HuuIch == 0 ? 0 : danhGia.HuuIch - 1;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllDanhGia(DanhGiaSanPhamParamView danhGiaSanPhamParamView)
        {
            try
            {
                var danhGias =
                    from s in _context.DanhGiaSanPhams
                    where s.SanPhamId == danhGiaSanPhamParamView.SanPhamId
                    select new DanhGiaSanPhamView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        SanPhamId = s.SanPhamId,
                        CreatedAt = s.CreatedAt,
                        DanhGia = s.DanhGia,
                        NhanXet = s.NhanXet,
                        HuuIch = s.HuuIch,
                        User = (from u in _context.Users
                                where u.Id == s.UserId
                                select new UserView
                                {
                                    UserName = u.UserName,
                                    UserInfo = (from ui in _context.UserInfos
                                                where ui.UserId == u.Id
                                                select new UserInfoView
                                                {
                                                    Ho = ui.Ho,
                                                    TenDem = ui.TenDem,
                                                    Ten = ui.Ten,
                                                    AnhDaiDien = ui.AnhDaiDien
                                                }).FirstOrDefault()
                                }).FirstOrDefault()
                    };
                if (!danhGias.Any())
                {
                    return new DanhGiaSanPhamListView(null, 0, 404);
                }
                if (danhGiaSanPhamParamView.DanhGia != null)
                {
                    var predicate = PredicateBuilder.New<DanhGiaSanPhamView>(false);
                    foreach (var subItem in danhGiaSanPhamParamView.DanhGia)
                    {
                        predicate = predicate.Or(s => s.DanhGia == subItem);
                    }
                    danhGias = danhGias.Where(predicate);
                }
                if (danhGiaSanPhamParamView.Order == "asc")
                {
                    if (danhGiaSanPhamParamView.OrderBy == "createdAt")
                    {
                        danhGias = danhGias.OrderBy(s => s.CreatedAt);
                    }
                    if (danhGiaSanPhamParamView.OrderBy == "huuIch")
                    {
                        danhGias = danhGias.OrderBy(s => s.HuuIch)
                                            .ThenByDescending(s => s.CreatedAt);
                    }
                }
                else if (danhGiaSanPhamParamView.Order == "desc")
                {
                    if (danhGiaSanPhamParamView.OrderBy == "createdAt")
                    {
                        danhGias = danhGias.OrderByDescending(s => s.CreatedAt);
                    }
                    if (danhGiaSanPhamParamView.OrderBy == "huuIch")
                    {
                        danhGias = danhGias.OrderByDescending(s => s.HuuIch)
                                            .ThenByDescending(s => s.CreatedAt);
                    }
                }
                var totalRecord = danhGias.ToList().Count();
                if (danhGiaSanPhamParamView.PageIndex > 0)
                {
                    danhGias = danhGias.Skip(danhGiaSanPhamParamView.PageSize * (danhGiaSanPhamParamView.PageIndex - 1)).Take(danhGiaSanPhamParamView.PageSize);
                }
                var listDanhGia = danhGias.ToList();
                return new DanhGiaSanPhamListView(listDanhGia, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new DanhGiaSanPhamListView(null, 0, 400);
            }
        }

        public List<DanhGiaSanPhamHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId)
        {
            var danhGiaHuuIchs =
                from s in _context.DanhGiaSanPhamHuuIchs
                where s.UserId == userId
                select new DanhGiaSanPhamHuuIchView
                {
                    UserId = s.UserId,
                    DanhGiaSanPhamId = s.DanhGiaSanPhamId,
                    Id = s.Id
                };
            if (!danhGiaHuuIchs.Any())
            {
                return null;
            }
            return danhGiaHuuIchs.ToList();
        }
    }
}
