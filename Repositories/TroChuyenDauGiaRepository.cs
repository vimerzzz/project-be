﻿using AutoMapper;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Linq;

namespace Project.Repositories
{
    public interface ITroChuyenDauGiaRepository
    {
        public ResponseWithPaginationView GetAllTroChuyenDauGia(TroChuyenDauGiaParamView troChuyenDauGiaParamView);
        public ResponsePostView CreateTroChuyenDauGia(ref TroChuyenDauGiaView troChuyenDauGiaView);
    }

    public class TroChuyenDauGiaRepository : ITroChuyenDauGiaRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public TroChuyenDauGiaRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateTroChuyenDauGia(ref TroChuyenDauGiaView troChuyenDauGiaView)
        {
            try
            {
                var tmpView = troChuyenDauGiaView;
                var newTroChuyenDauGia = new TroChuyenDauGia();
                newTroChuyenDauGia = _mapper.Map<TroChuyenDauGia>(tmpView);
                newTroChuyenDauGia.CreatedAt = DateTime.Now;
                newTroChuyenDauGia.UpdatedAt = DateTime.Now;
                _context.TroChuyenDauGias.Add(newTroChuyenDauGia);
                troChuyenDauGiaView.CreatedAt = DateTime.Now;
                if (!tmpView.IsSystem && !tmpView.IsShopOwner)
                {
                    troChuyenDauGiaView.User = (from u in _context.Users
                                                where u.Id == tmpView.UserId
                                                select new UserView
                                                {
                                                    UserName = u.UserName,
                                                    UserInfo = (from ui in _context.UserInfos
                                                                where ui.UserId == u.Id
                                                                select new UserInfoView
                                                                {
                                                                    Ho = ui.Ho,
                                                                    TenDem = ui.TenDem,
                                                                    Ten = ui.Ten,
                                                                    AnhDaiDien = ui.AnhDaiDien
                                                                }).FirstOrDefault()
                                                }).FirstOrDefault();
                    if (troChuyenDauGiaView.User == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                }
                _context.SaveChanges();
                troChuyenDauGiaView.Id = newTroChuyenDauGia.Id;
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllTroChuyenDauGia(TroChuyenDauGiaParamView troChuyenDauGiaParamView)
        {
            try
            {
                var troChuyenDauGias =
                    from s in _context.TroChuyenDauGias
                    where s.SanPhamDauGiaId == troChuyenDauGiaParamView.SanPhamDauGiaId
                    orderby s.CreatedAt descending
                    select new TroChuyenDauGiaView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        CreatedAt = s.CreatedAt,
                        NoiDung = s.NoiDung,
                        SanPhamDauGiaId = s.SanPhamDauGiaId,
                        IsShopOwner = s.IsShopOwner,
                        IsSystem = s.IsSystem,
                        User = (from u in _context.Users
                                where u.Id == s.UserId
                                select new UserView
                                {
                                    UserName = u.UserName,
                                    UserInfo = (from ui in _context.UserInfos
                                                where ui.UserId == u.Id
                                                select new UserInfoView
                                                {
                                                    Ho = ui.Ho,
                                                    TenDem = ui.TenDem,
                                                    Ten = ui.Ten,
                                                    AnhDaiDien = ui.AnhDaiDien
                                                }).FirstOrDefault()
                                }).FirstOrDefault()
                    };
                if (!troChuyenDauGias.Any())
                {
                    return new TroChuyenDauGiaListView(null, 0, 404);
                }
                var totalRecord = troChuyenDauGias.ToList().Count();
                if (troChuyenDauGiaParamView.PageIndex > 0)
                {
                    troChuyenDauGias = troChuyenDauGias.Skip(troChuyenDauGiaParamView.PageSize * (troChuyenDauGiaParamView.PageIndex - 1)).Take(troChuyenDauGiaParamView.PageSize);
                }
                var listDanhGia = troChuyenDauGias.ToList();
                return new TroChuyenDauGiaListView(listDanhGia, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new TroChuyenDauGiaListView(null, 0, 400);
            }
        }
    }
}
