﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.Utilities.Converters;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Project.Repositories
{
    public interface IUserRepository
    {
        public ResponseWithPaginationView GetAllUser(UserParamView userParamView);
        public UserView GetUserById(Guid id);
        public KeyValuePair<ResponsePostView, Guid> CreateUser(UserView userView);
        public ResponsePostView UpdateUser(UserView userView);
        public ResponsePostView CheckExistedUserName(UserView userView);
        public ResponsePostView CheckExistedEmail(UserView userView);
        public ResponsePostView CheckPassword(UserView userView);
        public Task<ResponseLoginView> Login(UserView userView);
        public ResponsePostView SetStatusAndRoleForUser(UserView userView);
    }

    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IntegerToGuid converter = new IntegerToGuid();

        public UserRepository(ApplicationDBContext context, IMapper mapper, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public ResponseWithPaginationView GetAllUser(UserParamView userParamView)
        {
            try
            {
                var users =
                    from s in _context.Users
                    join ur in _context.UserRoles on s.Id equals ur.UserId
                    join r in _context.Roles on ur.RoleId equals r.Id
                    select new UserView
                    {
                        Id = s.Id,
                        Email = s.Email,
                        UserName = s.UserName,
                        HoatDong = s.HoatDong,
                        CreatedAt = s.CreatedAt,
                        RoleId = ur.RoleId,
                        RoleName = r.Name,
                        EmailConfirmed = s.EmailConfirmed,
                        UserInfo = (from ui in _context.UserInfos
                                    where ui.UserId == s.Id
                                    select new UserInfoView
                                    {
                                        Id = ui.Id,
                                        AnhDaiDien = ui.AnhDaiDien,
                                        GioiTinh = ui.GioiTinh,
                                        Ho = ui.Ho,
                                        SDT = ui.SDT,
                                        Ten = ui.Ten,
                                        TenDem = ui.TenDem,
                                        NgaySinh = ui.NgaySinh
                                    }).FirstOrDefault()
                    };
                if (!users.Any())
                {
                    return new UserListView(null, 0, 404);
                }
                if(userParamView.RoleId != null)
                {
                    users = users.Where(s => s.RoleId == userParamView.RoleId);
                }
                if(userParamView.HoatDong != null)
                {
                    users = users.Where(s => s.HoatDong == userParamView.HoatDong);
                }
                if(userParamView.SearchString != null)
                {
                    users = users.Where(s => (s.UserName.Contains(userParamView.SearchString) || s.Email.Contains(userParamView.SearchString)));
                }
                if(userParamView.Order == "asc")
                {
                    if(userParamView.OrderBy == "userName")
                    {
                        users = users.OrderBy(s => s.UserName);
                    }
                    else if(userParamView.OrderBy == "email")
                    {
                        users = users.OrderBy(s => s.Email);
                    }
                    else if(userParamView.OrderBy == "hoatDong")
                    {
                        users = users.OrderBy(s => s.HoatDong);
                    }
                    else if(userParamView.OrderBy == "createdAt")
                    {
                        users = users.OrderBy(s => s.CreatedAt);
                    }
                    else if (userParamView.OrderBy == "roleName")
                    {
                        users = users.OrderBy(s => s.RoleName);
                    }
                }
                else if(userParamView.Order == "desc")
                {
                    if(userParamView.OrderBy == "userName")
                    {
                        users = users.OrderByDescending(s => s.UserName);
                    }
                    else if (userParamView.OrderBy == "email")
                    {
                        users = users.OrderByDescending(s => s.Email);
                    }
                    else if (userParamView.OrderBy == "hoatDong")
                    {
                        users = users.OrderByDescending(s => s.HoatDong);
                    }
                    else if (userParamView.OrderBy == "createdAt")
                    {
                        users = users.OrderByDescending(s => s.CreatedAt);
                    }
                    else if (userParamView.OrderBy == "roleName")
                    {
                        users = users.OrderByDescending(s => s.RoleName);
                    }
                }
                var totalRecord = users.ToList().Count();
                if(userParamView.PageIndex > 0)
                {
                    users = users.Skip(userParamView.PageSize * (userParamView.PageIndex - 1)).Take(userParamView.PageSize);
                }
                var listUser = users.ToList();
                return new UserListView(listUser, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new UserListView(null, 0, 400);
            }
        }

        public UserView GetUserById(Guid id)
        {
            try
            {
                var users =
                    from s in _context.Users
                    where s.Id == id
                    select new UserView
                    {
                        Id = s.Id,
                        Email = s.Email,
                        UserName = s.UserName,
                        HoatDong = s.HoatDong,
                        EmailConfirmed = s.EmailConfirmed
                    };
                if (!users.Any())
                {
                    return null;
                }
                var user = users.FirstOrDefault();
                return user;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public KeyValuePair<ResponsePostView, Guid> CreateUser(UserView userView)
        {
            try
            {
                var newUser = new User();
                newUser = _mapper.Map<User>(userView);
                newUser.CreatedAt = DateTime.Now;
                newUser.UpdatedAt = DateTime.Now;
                newUser.HoatDong = true;
                newUser.SecurityStamp = Guid.NewGuid().ToString();
                newUser.PasswordHash = _userManager.PasswordHasher.HashPassword(newUser, userView.Password);
                _context.Users.Add(newUser);
                _context.SaveChanges();
                var newUserRole = new IdentityUserRole<Guid>
                {
                    UserId = newUser.Id,
                    RoleId = converter.IntToGuid(2)
                };
                _context.UserRoles.Add(newUserRole);
                _context.SaveChanges();
                var response = new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
                var pair = new KeyValuePair<ResponsePostView, Guid>(response, newUser.Id);
                return pair;
            }
            catch(Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                var response = new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
                var pair = new KeyValuePair<ResponsePostView, Guid>(response, converter.IntToGuid(0));
                return pair;
            }
        }

        public ResponsePostView UpdateUser(UserView userView)
        {
            try
            {
                var user = _context.Users.Where(s => s.Id == userView.Id).FirstOrDefault();
                if (user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _mapper.Map(userView, user);
                user.UpdatedAt = DateTime.Now;
                user.SecurityStamp = Guid.NewGuid().ToString();
                if(userView.Password != null)
                {
                    user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, userView.Password);
                }
                var userInfo = _context.UserInfos.Where(ui => ui.UserId == userView.Id).FirstOrDefault();
                if(userInfo == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                userInfo.Email = userView.Email;
                userInfo.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView CheckExistedUserName(UserView userView)
        {
            try
            {
                var user = _context.Users.Where(s => s.UserName == userView.UserName && s.Id != userView.Id).FirstOrDefault();
                if(user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if(user.HoatDong == false)
                {
                    return new ResponsePostView(MessageConstants.ACCOUNT_WAS_BANNED, 406);
                }
                return new ResponsePostView(MessageConstants.FOUND, 200);
            }
            catch(Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
            }
        }

        public ResponsePostView CheckExistedEmail(UserView userView)
        {
            try
            {
                var user = _context.Users.Where(s => s.Email == userView.Email && s.Id != userView.Id).FirstOrDefault();
                if (user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                return new ResponsePostView(MessageConstants.FOUND, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
            }
        }

        public ResponsePostView CheckPassword(UserView userView)
        {
            try
            {
                var user = _context.Users.Where(s => s.UserName == userView.UserName).FirstOrDefault();
                if (user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var checkHash = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, userView.Password);
                if (checkHash == PasswordVerificationResult.Failed)
                {
                    return new ResponsePostView(MessageConstants.WRONG_PASSWORD, 401);
                }
                else
                {
                    return new ResponsePostView(MessageConstants.FOUND, 200);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.SYSTEM_ERROR, 400);
            }
        }

        public async Task<ResponseLoginView> Login(UserView userview)
        {
            try
            {
                var user = _context.Users.Where(s => s.UserName == userview.UserName).FirstOrDefault();
                if(user == null)
                {
                    return new ResponseLoginView(converter.IntToGuid(0), converter.IntToGuid(0), null, MessageConstants.NOT_FOUND, 404);
                }
                var userRole = _context.UserRoles.Where(s => s.UserId == user.Id).FirstOrDefault();
                if(userRole == null)
                {
                    return new ResponseLoginView(converter.IntToGuid(0), converter.IntToGuid(0), null, MessageConstants.NOT_FOUND, 404);
                }
                var claims = new List<Claim> { };
                if (userRole.RoleId == converter.IntToGuid(1))
                {
                    claims.Add(new Claim("type", "Admin"));
                    claims.Add(new Claim("type", "Customer"));
                }
                else if (userRole.RoleId == converter.IntToGuid(2))
                {
                    claims.Add(new Claim("type", "Customer"));
                }
                else if (userRole.RoleId == converter.IntToGuid(3))
                {
                    claims.Add(new Claim("type", "Deliver"));
                    claims.Add(new Claim("type", "Customer"));
                }
                else if (userRole.RoleId == converter.IntToGuid(4))
                {
                    claims.Add(new Claim("type", "Expert"));
                    claims.Add(new Claim("type", "Customer"));
                }
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SXkSqsKyNUyvGbnHs7ke2NCq8zQzNLW7mPmHbnZZ"));
                var jwtToken = new JwtSecurityToken(
                    TokenProviderConstants.IssuerName,
                    user.Id.ToString(),
                    claims,
                    expires: DateTime.Now.AddDays(15),
                    signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256));
                var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
                await _userManager.SetAuthenticationTokenAsync(user, TokenProviderConstants.LoginProvider, TokenProviderConstants.LoginTokenName, token);
                return new ResponseLoginView(user.Id, userRole.RoleId, token, MessageConstants.LOGIN_SUCCESS, 200);
            }
            catch(Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponseLoginView(converter.IntToGuid(0), converter.IntToGuid(0), null, MessageConstants.LOGIN_FAILED, 400);
            }
        }

        public ResponsePostView SetStatusAndRoleForUser(UserView userView)
        {
            try
            {
                var user = _context.Users.Where(s => s.Id == userView.Id).FirstOrDefault();
                if (user == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                user.HoatDong = userView.HoatDong;
                user.UpdatedAt = DateTime.Now;
                user.SecurityStamp = Guid.NewGuid().ToString();
                var userRole = _context.UserRoles.Where(s => s.UserId == userView.Id).FirstOrDefault();
                if (userRole == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if(userRole.RoleId != userView.RoleId)
                {
                    _context.Remove(userRole);
                    var newUserRole = new IdentityUserRole<Guid>
                    {
                        UserId = userView.Id,
                        RoleId = userView.RoleId
                    };
                    _context.Add(newUserRole);
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
