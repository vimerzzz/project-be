﻿using AutoMapper;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Repositories
{
    public interface IGioHangRepository
    {
        public List<GioHangWithListSanPhamView> GetAllGioHangDauGia(GioHangView gioHangView);
        public List<GioHangWithListSanPhamView> GetAllGioHang(GioHangView gioHangView);
        public ResponsePostView CreateGioHang(GioHangView gioHangView);
        public ResponsePostView UpdateGioHang(GioHangView gioHangView);
        public ResponsePostView DeleteGioHang(GioHangView gioHangView);
    }

    public class GioHangRepository : IGioHangRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public GioHangRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateGioHang(GioHangView gioHangView)
        {
            try
            {
                var existedGioHang = _context.GioHangs.Where(s => (((s.SanPhamId != null && s.SanPhamId == gioHangView.SanPhamId) ||
                                                                    (s.SanPhamDauGiaId != null && s.SanPhamDauGiaId == gioHangView.SanPhamDauGiaId)) &&
                                                                    s.UserId == gioHangView.UserId &&
                                                                    s.DonGia == gioHangView.DonGia &&
                                                                    s.GiamGia == gioHangView.GiamGia)).FirstOrDefault();
                if (existedGioHang == null)
                {
                    var newGioHang = new GioHang();
                    newGioHang = _mapper.Map<GioHang>(gioHangView);
                    newGioHang.CreatedAt = DateTime.Now;
                    newGioHang.UpdatedAt = DateTime.Now;
                    _context.GioHangs.Add(newGioHang);
                }
                else
                {
                    existedGioHang.SoLuong += gioHangView.SoLuong;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteGioHang(GioHangView gioHangView)
        {
            try
            {
                var gioHang = _context.GioHangs.Where(s => s.Id == gioHangView.Id).FirstOrDefault();
                if (gioHang == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _context.GioHangs.Remove(gioHang);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public List<GioHangWithListSanPhamView> GetAllGioHangDauGia(GioHangView gioHangView)
        {
            try
            {
                var gioHangs =
                    from s in _context.GioHangs
                    join sp in _context.SanPhamDauGias on s.SanPhamDauGiaId equals sp.Id
                    join sh in _context.Shops on sp.ShopId equals sh.Id
                    where s.UserId == gioHangView.UserId
                    select new GioHangView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        SanPhamDauGiaId = s.SanPhamDauGiaId,
                        SoLuong = s.SoLuong,
                        DonGia = s.DonGia,
                        GiamGia = s.GiamGia,
                        AnhDaiDienShop = sh.AnhDaiDien,
                        TenShop = sh.TenShop,
                        ShopId = sh.Id,
                        SanPhamDauGia = (from ss in _context.SanPhamDauGias
                                         where ss.Id == s.SanPhamDauGiaId
                                         select new SanPhamDauGiaWithHinhAnhView
                                         {
                                             TenSanPham = ss.TenSanPham,
                                             CreatedAt = ss.CreatedAt,
                                             UpdatedAt = ss.UpdatedAt,
                                             Id = ss.Id,
                                             LoaiSanPhamId = ss.LoaiSanPhamId,
                                             ThongTinKhac = ss.ThongTinKhac,
                                             ShopId = ss.ShopId,
                                             XuatXu = ss.XuatXu,
                                             Nam = ss.Nam,
                                             DungTich = ss.DungTich,
                                             MauSac = ss.MauSac,
                                             NongDo = ss.NongDo,
                                             HinhAnhSanPhamDauGia = (from h in _context.HinhAnhSanPhamDauGias
                                                                     where h.SanPhamDauGiaId == ss.Id
                                                                     orderby h.ThuTu
                                                                     select new HinhAnhSanPhamDauGiaView
                                                                     {
                                                                         Id = h.Id,
                                                                         SanPhamDauGiaId = h.SanPhamDauGiaId,
                                                                         HinhAnh = h.HinhAnh,
                                                                         ThuTu = h.ThuTu
                                                                     }).ToList()
                                         }).FirstOrDefault()
                    };
                if (!gioHangs.Any())
                {
                    return null;
                }
                var groupGioHangs = gioHangs.ToList().GroupBy(s => new
                {
                    s.AnhDaiDienShop,
                    s.TenShop,
                    s.ShopId
                }).Select(g => new GioHangWithListSanPhamView
                {
                    AnhDaiDienShop = g.Key.AnhDaiDienShop,
                    TenShop = g.Key.TenShop,
                    ShopId = g.Key.ShopId,
                    ListSanPhamInGioHang = g.Select(x => new ListSanPhamInGioHangView
                    {
                        DonGia = x.DonGia,
                        GiamGia = x.GiamGia,
                        Id = x.Id,
                        SanPhamDauGia = x.SanPhamDauGia,
                        SanPhamDauGiaId = x.SanPhamDauGiaId,
                        SoLuong = x.SoLuong,
                        UserId = x.UserId
                    }).ToList()
                });
                return groupGioHangs.ToList();

            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public List<GioHangWithListSanPhamView> GetAllGioHang(GioHangView gioHangView)
        {
            try
            {
                var gioHangs =
                    from s in _context.GioHangs
                    join sp in _context.SanPhams on s.SanPhamId equals sp.Id
                    join sh in _context.Shops on sp.ShopId equals sh.Id
                    where s.UserId == gioHangView.UserId
                    select new GioHangView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        SanPhamId = s.SanPhamId,
                        SoLuong = s.SoLuong,
                        DonGia = s.DonGia,
                        GiamGia = s.GiamGia,
                        AnhDaiDienShop = sh.AnhDaiDien,
                        TenShop = sh.TenShop,
                        ShopId = sh.Id,
                        SanPham = (from ss in _context.SanPhams
                                   where ss.Id == s.SanPhamId
                                   select new SanPhamWithHinhAnhView
                                   {
                                       DonGia = ss.DonGia,
                                       TenSanPham = ss.TenSanPham,
                                       GiamGia = ss.GiamGia,
                                       CreatedAt = ss.CreatedAt,
                                       DanhGiaChung = ss.DanhGiaChung,
                                       Id = ss.Id,
                                       SoLuong = ss.SoLuong,
                                       LoaiSanPhamId = ss.LoaiSanPhamId,
                                       ThongTinKhac = ss.ThongTinKhac,
                                       ShopId = ss.ShopId,
                                       SoLuongDaBan = ss.SoLuongDaBan,
                                       SoNguoiDanhGia = ss.SoNguoiDanhGia,
                                       MotSao = ss.MotSao,
                                       HaiSao = ss.HaiSao,
                                       BaSao = ss.BaSao,
                                       BonSao = ss.BonSao,
                                       NamSao = ss.NamSao,
                                       XuatXu = ss.XuatXu,
                                       Nam = ss.Nam,
                                       DungTich = ss.DungTich,
                                       MauSac = ss.MauSac,
                                       NongDo = ss.NongDo,
                                       HinhAnhSanPham = (from h in _context.HinhAnhSanPhams
                                                         where h.SanPhamId == ss.Id
                                                         orderby h.ThuTu
                                                         select new HinhAnhSanPhamView
                                                         {
                                                             Id = h.Id,
                                                             SanPhamId = h.SanPhamId,
                                                             HinhAnh = h.HinhAnh,
                                                             ThuTu = h.ThuTu
                                                         }).ToList()
                                   }).FirstOrDefault()
                    };
                if (!gioHangs.Any())
                {
                    return null;
                }
                var groupGioHangs = gioHangs.ToList().GroupBy(s => new
                {
                    s.AnhDaiDienShop,
                    s.TenShop,
                    s.ShopId
                }).Select(g => new GioHangWithListSanPhamView
                {
                    AnhDaiDienShop = g.Key.AnhDaiDienShop,
                    TenShop = g.Key.TenShop,
                    ShopId = g.Key.ShopId,
                    ListSanPhamInGioHang = g.Select(x => new ListSanPhamInGioHangView
                    {
                        DonGia = x.DonGia,
                        GiamGia = x.GiamGia,
                        Id = x.Id,
                        SanPham = x.SanPham,
                        SanPhamId = x.SanPhamId,
                        SoLuong = x.SoLuong,
                        UserId = x.UserId
                    }).ToList()
                });
                return groupGioHangs.ToList();
                
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateGioHang(GioHangView gioHangView)
        {
            try
            {
                var gioHang = _context.GioHangs.Where(s => s.Id == gioHangView.Id).FirstOrDefault();
                if (gioHang == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _mapper.Map(gioHangView, gioHang);
                gioHang.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
