﻿using AutoMapper;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.Utilities.Converters;
using Project.ViewModels;
using Serilog;
using System;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface IDonHangRepository
    {
        public ResponseWithPaginationView GetAllDonHang(DonHangParamView donHangParamView);
        public DonHangView GetDonHangByMaDonHang(DonHangView donHangView);
        public ResponsePostView CreateDonHang(ref DonHangView donHangView);
        public ResponsePostView UpdateDonHang(DonHangView donHangView);
        public ResponsePostView UpdateTinhTrangDonHang(DonHangView donHangView);
    }

    public class DonHangRepository : IDonHangRepository
    {
        private readonly IntegerToGuid converter = new IntegerToGuid();
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public DonHangRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateDonHang(ref DonHangView donHangView)
        {
            try
            {
                foreach (var item in donHangView.DonHangSanPham)
                {
                    if (!item.SanPhamDauGia)
                    {
                        var sanPham = _context.SanPhams.Where(s => s.Id == item.SanPhamGocId).FirstOrDefault();
                        if (sanPham == null)
                        {
                            return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                        }
                        if (sanPham.SoLuong < item.SoLuong)
                        {
                            return new ResponsePostView(MessageConstants.INSUFFICIENT, 406);
                        }
                        sanPham.SoLuong -= item.SoLuong;
                    }
                    else
                    {
                        var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == item.SanPhamGocId).FirstOrDefault();
                        if (sanPhamDauGia == null)
                        {
                            return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                        }
                        sanPhamDauGia.TinhTrangDauGiaId = converter.IntToGuid(9);
                        sanPhamDauGia.UpdatedAt = DateTime.Now;
                    }
                }
                var newDonHang = new DonHang();
                newDonHang = _mapper.Map<DonHang>(donHangView);
                newDonHang.CreatedAt = DateTime.Now;
                newDonHang.UpdatedAt = DateTime.Now;
                newDonHang.TinhTrangDonHangId = converter.IntToGuid(1);
                _context.DonHangs.Add(newDonHang);
                _context.SaveChanges();
                donHangView.MaDonHang = newDonHang.MaDonHang;
                foreach (var item in donHangView.DonHangSanPham)
                {
                    var folderName = Path.Combine("Statics", "Images", "Orders", donHangView.UserId.ToString(), newDonHang.Id.ToString());
                    // var itemFolderName = Path.Combine("Statics", "Images", "Items");
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    Directory.CreateDirectory(pathToSave);
                    // var pathToCopy = Path.Combine(Directory.GetCurrentDirectory(), itemFolderName);
                    var fileName = item.HinhAnhSanPham.Split("\\").Last();
                    // var itemPath = Path.Combine(pathToCopy, fileName);
                    var itemPath = Path.Combine(Directory.GetCurrentDirectory(), item.HinhAnhSanPham);
                    var name = "";
                    for (int i = 0; i < fileName.Split('.').Count(); i++)
                    {
                        if (i < fileName.Split('.').Count() - 1)
                        {
                            name += fileName.Split('.')[i];
                        }
                    }
                    var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                    var fullPath = Path.Combine(pathToSave, fileRename);
                    var dbPath = Path.Combine(folderName, fileRename);
                    if (System.IO.File.Exists(itemPath))
                    {
                        System.IO.File.Copy(itemPath, fullPath, true);
                    }
                    item.HinhAnhSanPham = dbPath;
                }
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllDonHang(DonHangParamView donHangParamView)
        {
            try
            {
                var donHangs =
                    from s in _context.DonHangs
                    join t in _context.TinhTrangDonHangs on s.TinhTrangDonHangId equals t.Id
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    orderby s.MaDonHang descending
                    select new DonHangView
                    {
                        MaDonHang = s.MaDonHang,
                        Id = s.Id,
                        CreatedAt = s.CreatedAt,
                        DatHo = s.DatHo,
                        DiaChi = s.DiaChi,
                        GhiChu = s.GhiChu,
                        SoDienThoai = s.SoDienThoai,
                        TenDayDu = s.TenDayDu,
                        ThanhTien = s.ThanhTien,
                        TinhTrangDonHangId = s.TinhTrangDonHangId,
                        TinhTrang = t.TinhTrang,
                        UserId = s.UserId,
                        XuatHoaDon = s.XuatHoaDon,
                        SoSanPham = s.SoSanPham,
                        ShopId = s.ShopId,
                        ShopName = sh.TenShop,
                        SoDienThoaiNguoiDat = s.SoDienThoaiNguoiDat,
                        TenDayDuNguoiDat = s.TenDayDuNguoiDat,
                        LyDoHuy = s.LyDoHuy,
                        DonHangSanPham = (from d in _context.DonHangSanPhams
                                          where d.DonHangId == s.Id
                                          select new DonHangSanPhamView
                                          {
                                              Id = d.Id,
                                              DonHangId = d.DonHangId,
                                              DonGia = d.DonGia,
                                              GiamGia = d.GiamGia,
                                              HinhAnhSanPham = d.HinhAnhSanPham,
                                              SanPhamGocId = d.SanPhamGocId,
                                              SoLuong = d.SoLuong,
                                              TenSanPham = d.TenSanPham,
                                              SanPhamDauGia = d.SanPhamDauGia,
                                          }).ToList()
                    };
                if (!donHangs.Any())
                {
                    return new DonHangListView(null, 0, 404);
                }
                donHangs = donHangs.Where(s => s.DonHangSanPham.Count() > 0);
                if (donHangParamView.UserId != null)
                {
                    donHangs = donHangs.Where(s => s.UserId == donHangParamView.UserId);
                }
                if (donHangParamView.ShopId != null)
                {
                    donHangs = donHangs.Where(s => s.ShopId == donHangParamView.ShopId);
                }
                if (donHangParamView.TinhTrangDonHangId != null)
                {
                    if(donHangParamView.TinhTrangDonHangId == converter.IntToGuid(5) || donHangParamView.TinhTrangDonHangId == converter.IntToGuid(6))
                    {
                        donHangs = donHangs.Where(s => (s.TinhTrangDonHangId == converter.IntToGuid(5) || s.TinhTrangDonHangId == converter.IntToGuid(6)));
                    }
                    else
                    {
                        donHangs = donHangs.Where(s => s.TinhTrangDonHangId == donHangParamView.TinhTrangDonHangId);
                    }
                }
                if (donHangParamView.MaDonHang != null)
                {
                    donHangs = donHangs.Where(s => s.MaDonHang == donHangParamView.MaDonHang);
                }
                if (donHangParamView.Order == "asc")
                {
                    if (donHangParamView.OrderBy == "createdAt")
                    {
                        donHangs = donHangs.OrderBy(s => s.CreatedAt);
                    }
                    if (donHangParamView.OrderBy == "maDonHang")
                    {
                        donHangs = donHangs.OrderBy(s => s.MaDonHang);
                    }
                }
                else if (donHangParamView.Order == "desc")
                {
                    if (donHangParamView.OrderBy == "createdAt")
                    {
                        donHangs = donHangs.OrderByDescending(s => s.CreatedAt);
                    }
                    if (donHangParamView.OrderBy == "maDonHang")
                    {
                        donHangs = donHangs.OrderByDescending(s => s.MaDonHang);
                    }
                }
                var totalRecord = donHangs.ToList().Count();
                if (donHangParamView.PageIndex > 0)
                {
                    donHangs = donHangs.Skip(donHangParamView.PageSize * (donHangParamView.PageIndex - 1)).Take(donHangParamView.PageSize);
                }
                var listDonHang = donHangs.ToList();
                return new DonHangListView(listDonHang, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new DonHangListView(null, 0, 400);
            }
        }

        public DonHangView GetDonHangByMaDonHang(DonHangView donHangView)
        {
            try
            {
                var donHangs =
                    from s in _context.DonHangs
                    where s.MaDonHang == donHangView.MaDonHang &&
                        s.UserId == donHangView.UserId
                    join t in _context.TinhTrangDonHangs on s.TinhTrangDonHangId equals t.Id
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    orderby s.MaDonHang descending
                    select new DonHangView
                    {
                        MaDonHang = s.MaDonHang,
                        Id = s.Id,
                        CreatedAt = s.CreatedAt,
                        DatHo = s.DatHo,
                        DiaChi = s.DiaChi,
                        GhiChu = s.GhiChu,
                        SoDienThoai = s.SoDienThoai,
                        TenDayDu = s.TenDayDu,
                        ThanhTien = s.ThanhTien,
                        TinhTrangDonHangId = s.TinhTrangDonHangId,
                        UserId = s.UserId,
                        XuatHoaDon = s.XuatHoaDon,
                        SoSanPham = s.SoSanPham,
                        ShopId = s.ShopId,
                        ShopName = sh.TenShop,
                        TinhTrang = t.TinhTrang,
                        SoDienThoaiNguoiDat = s.SoDienThoaiNguoiDat,
                        TenDayDuNguoiDat = s.TenDayDuNguoiDat,
                        LyDoHuy = s.LyDoHuy,
                        DonHangSanPham = (from d in _context.DonHangSanPhams
                                          where d.DonHangId == s.Id
                                          select new DonHangSanPhamView
                                          {
                                              Id = d.Id,
                                              DonHangId = d.DonHangId,
                                              DonGia = d.DonGia,
                                              GiamGia = d.GiamGia,
                                              HinhAnhSanPham = d.HinhAnhSanPham,
                                              SanPhamGocId = d.SanPhamGocId,
                                              SoLuong = d.SoLuong,
                                              TenSanPham = d.TenSanPham,
                                              SanPhamDauGia = d.SanPhamDauGia,
                                          }).ToList()
                    };
                return donHangs.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateDonHang(DonHangView donHangView)
        {
            try
            {
                var donHang = _context.DonHangs.Where(s => s.Id == donHangView.Id).FirstOrDefault();
                if (donHang == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if(donHang.UserId != donHangView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                if(donHang.TinhTrangDonHangId == converter.IntToGuid(1))
                {
                    donHang.UpdatedAt = DateTime.Now;
                    donHang.DatHo = donHangView.DatHo;
                    donHang.TenDayDu = donHangView.TenDayDu;
                    donHang.TenDayDuNguoiDat = donHangView.TenDayDuNguoiDat;
                    donHang.XuatHoaDon = donHangView.XuatHoaDon;
                    donHang.DiaChi = donHangView.DiaChi;
                    donHang.GhiChu = donHangView.GhiChu;
                    donHang.SoDienThoai = donHangView.SoDienThoai;
                    donHang.SoDienThoaiNguoiDat = donHangView.SoDienThoaiNguoiDat;
                    _context.SaveChanges();
                    return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
                }
                else
                {
                    return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView UpdateTinhTrangDonHang(DonHangView donHangView)
        {
            try
            {
                var donHang = _context.DonHangs.Where(s => s.Id == donHangView.Id).FirstOrDefault();
                if (donHang == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                donHang.TinhTrangDonHangId = donHangView.TinhTrangDonHangId;
                donHang.LyDoHuy = donHangView.LyDoHuy;
                donHang.UpdatedAt = DateTime.Now;
                if (converter.IntToGuid(4) == donHangView.TinhTrangDonHangId)
                {
                    foreach (var item in donHangView.DonHangSanPham)
                    {
                        if (!item.SanPhamDauGia)
                        {
                            var sanPham = _context.SanPhams.Where(s => s.Id == item.SanPhamGocId).FirstOrDefault();
                            if (sanPham == null)
                            {
                                return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                            }
                            sanPham.SoLuongDaBan += item.SoLuong;
                        }
                    }
                }
                else if (converter.IntToGuid(5) == donHangView.TinhTrangDonHangId || converter.IntToGuid(6) == donHangView.TinhTrangDonHangId)
                {
                    foreach (var item in donHangView.DonHangSanPham)
                    {
                        if (!item.SanPhamDauGia)
                        {
                            var sanPham = _context.SanPhams.Where(s => s.Id == item.SanPhamGocId).FirstOrDefault();
                            if (sanPham == null)
                            {
                                return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                            }
                            sanPham.SoLuong += item.SoLuong;
                        }
                        else
                        {
                            var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == item.SanPhamGocId).FirstOrDefault();
                            if (sanPhamDauGia == null)
                            {
                                return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                            }
                            sanPhamDauGia.TinhTrangDauGiaId = converter.IntToGuid(7);
                            sanPhamDauGia.UpdatedAt = DateTime.Now;
                        }
                    }
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
