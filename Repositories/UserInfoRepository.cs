﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Repositories
{
    public interface IUserInfoRepository
    {
        public UserInfoView GetUserInfoByUserId(Guid userId);
        public ResponsePostView CreateUserInfoByUserView(UserView userView);
        public ResponsePostView UpdateUserInfo(UserInfoView userInfoView);
    }

    public class UserInfoRepository : IUserInfoRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public UserInfoRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public UserInfoView GetUserInfoByUserId(Guid userId)
        {
            try
            {
                var userInfo =
                    from s in _context.UserInfos
                    join u in _context.Users on s.UserId equals u.Id
                    join ur in _context.UserRoles on u.Id equals ur.UserId
                    join r in _context.Roles on ur.RoleId equals r.Id
                    where s.UserId == userId
                    select new UserInfoView
                    {
                        Id = s.Id,
                        Email = s.Email,
                        GioiTinh = s.GioiTinh,
                        Ho = s.Ho,
                        NgaySinh = s.NgaySinh,
                        SDT = s.SDT,
                        Ten = s.Ten,
                        TenDem = s.TenDem,
                        AnhDaiDien = s.AnhDaiDien,
                        CoAnhDaiDien = s.CoAnhDaiDien,
                        CoShop = s.CoShop,
                        EmailConfirmed = u.EmailConfirmed,
                        UserId = s.UserId,
                        RoleName = r.Name
                    };
                if(!userInfo.Any())
                {
                    return null;
                }
                var user = userInfo.FirstOrDefault();
                return user;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }
        public ResponsePostView CreateUserInfoByUserView(UserView userView)
        {
            try
            {
                var newUserInfo = new UserInfo();
                newUserInfo = _mapper.Map<UserInfo>(userView);
                newUserInfo.CreatedAt = DateTime.Now;
                newUserInfo.UpdatedAt = DateTime.Now;
                newUserInfo.NgaySinh = DateTime.Now;
                newUserInfo.AnhDaiDien = "Statics\\Images\\Users\\blank_avatar.png";
                _context.UserInfos.Add(newUserInfo);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }
        public ResponsePostView UpdateUserInfo(UserInfoView userInfoView)
        {
            try
            {
                var userInfo = _context.UserInfos.Where(s => s.Id == userInfoView.Id).FirstOrDefault();
                if(userInfo == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if(userInfo.UserId != userInfoView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _mapper.Map(userInfoView, userInfo);
                userInfo.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
