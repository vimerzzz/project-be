﻿using AutoMapper;
using LinqKit;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.Utilities.Converters;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface ISanPhamDauGiaRepository
    {
        public ResponseWithPaginationView GetAllSanPhamDauGiaByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView);
        public SanPhamDauGiaView GetSanPhamDauGiaById(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView CreateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView UpdateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView UpdateTinhHinhDauGia(ref SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView UpdateTinhTrangDauGia(ref SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView YeuCauKetThucDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView CancelSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView DeleteSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponseWithPaginationView GetNewestAllSanPhamDauGiaWithHinhAnhByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView);
        public ResponseWithPaginationView GetAllSanPhamDauGia(SanPhamDauGiaParamView sanPhamDauGiaParamView);
        public SanPhamDauGiaWithHinhAnhView GetSanPhamDauGiaWithHinhAnhByName(string tenShop, string tenSanPham);
        public List<SanPhamDauGia> GetAllSanPhamDauGiaToCheckStatus(Guid tinhTrangDauGiaId);
        public List<TinhTrangDauGiaView> GetAllTinhTrangDauGia();
    }

    public class SanPhamDauGiaRepository : ISanPhamDauGiaRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        private readonly IntegerToGuid converter = new IntegerToGuid();

        public SanPhamDauGiaRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CancelSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == sanPhamDauGiaView.Id).FirstOrDefault();
                if (sanPhamDauGia == null && sanPhamDauGia.ShopId != sanPhamDauGiaView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var shop = _context.Shops.Where(s => s.Id == sanPhamDauGiaView.ShopId).FirstOrDefault();
                if(shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if(shop.UserId != sanPhamDauGiaView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                sanPhamDauGia.TinhTrangDauGiaId = converter.IntToGuid(8);
                sanPhamDauGia.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView CreateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var newSanPhamDauGia = new SanPhamDauGia();
                newSanPhamDauGia = _mapper.Map<SanPhamDauGia>(sanPhamDauGiaView);
                newSanPhamDauGia.CreatedAt = DateTime.Now;
                newSanPhamDauGia.UpdatedAt = DateTime.Now;
                newSanPhamDauGia.TinhTrangDauGiaId = converter.IntToGuid(1);
                _context.SanPhamDauGias.Add(newSanPhamDauGia);
                var shop = _context.Shops.Where(s => s.Id == sanPhamDauGiaView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if(shop.UserId != sanPhamDauGiaView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                shop.TongSoSanPham++;
                _context.SaveChanges();
                return new ResponseIdPostView(MessageConstants.CREATE_SUCCESS, 200, newSanPhamDauGia.Id);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == sanPhamDauGiaView.Id).FirstOrDefault();
                if (sanPhamDauGia == null && sanPhamDauGia.ShopId != sanPhamDauGiaView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _context.SanPhamDauGias.Remove(sanPhamDauGia);
                var shop = _context.Shops.Where(s => s.Id == sanPhamDauGiaView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if (shop.UserId != sanPhamDauGiaView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                var folderName = Path.Combine("Statics", "Images", "Auctions", shop.Id.ToString(), sanPhamDauGia.Id.ToString());
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                var hinhAnhSanPhamDauGias = _context.HinhAnhSanPhamDauGias.Where(s => s.SanPhamDauGiaId == sanPhamDauGiaView.Id).ToList();
                _context.HinhAnhSanPhamDauGias.RemoveRange(hinhAnhSanPhamDauGias);
                shop.TongSoSanPham = shop.TongSoSanPham == 0 ? 0 : shop.TongSoSanPham - 1;
                _context.SaveChanges();
                if (Directory.Exists(pathToSave))
                {
                    Directory.Delete(pathToSave, true);
                }
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllSanPhamDauGia(SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            try
            {
                var sanPhamDauGias =
                    from s in _context.SanPhamDauGias
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    join t in _context.TinhTrangDauGias on s.TinhTrangDauGiaId equals t.Id
                    join l in _context.LoaiSanPhams on s.LoaiSanPhamId equals l.Id
                    select new SanPhamDauGiaWithHinhAnhView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        TenLoaiSanPham = l.TenLoaiSanPham,
                        ThongTinKhac = s.ThongTinKhac,
                        TenSanPham = s.TenSanPham,
                        UserId = sh.UserId,
                        CreatedAt = s.CreatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        TinhTrangDauGiaId = s.TinhTrangDauGiaId,
                        GiaKhoiDiem = s.GiaKhoiDiem,
                        GiaHienTai = s.GiaHienTai,
                        ThoiGianKetThuc = s.ThoiGianKetThuc,
                        ThoiGianBatDau = s.ThoiGianBatDau,
                        KhoangGiaToiThieu = s.KhoangGiaToiThieu,
                        TenShop = sh.TenShop,
                        TinhTrang = t.TinhTrang,
                        HinhAnhSanPhamDauGia = (from h in _context.HinhAnhSanPhamDauGias
                                                where h.SanPhamDauGiaId == s.Id
                                                orderby h.ThuTu
                                                select new HinhAnhSanPhamDauGiaView
                                                {
                                                    Id = h.Id,
                                                    SanPhamDauGiaId = h.SanPhamDauGiaId,
                                                    HinhAnh = h.HinhAnh,
                                                    ThuTu = h.ThuTu
                                                }).ToList(),
                        NguoiRaGia = (from u in _context.Users
                                        where u.Id == s.UserId
                                        select new UserView
                                        {
                                            UserName = u.UserName,
                                            UserInfo = (from ui in _context.UserInfos
                                                        where ui.UserId == u.Id
                                                        select new UserInfoView
                                                        {
                                                            Ho = ui.Ho,
                                                            TenDem = ui.TenDem,
                                                            Ten = ui.Ten,
                                                            AnhDaiDien = ui.AnhDaiDien
                                                        }).FirstOrDefault()
                                        }).FirstOrDefault()
                    };
                if (!sanPhamDauGias.Any())
                {
                    return new SanPhamDauGiaWithHinhAnhListView(null, 0, 404);
                }
                if (sanPhamDauGiaParamView.SearchString != null)
                {
                    sanPhamDauGias = sanPhamDauGias.Where(s => (s.TenSanPham.Contains(sanPhamDauGiaParamView.SearchString) ||
                                                                s.TenLoaiSanPham.Contains(sanPhamDauGiaParamView.SearchString) ||
                                                                s.TenShop.Contains(sanPhamDauGiaParamView.SearchString) ||
                                                                s.ThongTinKhac.Contains(sanPhamDauGiaParamView.SearchString)));
                }
                if (sanPhamDauGiaParamView.LoaiSanPhamId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamDauGiaWithHinhAnhView>(false);
                    foreach (var subItem in sanPhamDauGiaParamView.LoaiSanPhamId)
                    {
                        predicate = predicate.Or(s => s.LoaiSanPhamId == subItem);
                    }
                    sanPhamDauGias = sanPhamDauGias.Where(predicate);
                }
                if (sanPhamDauGiaParamView.TinhTrangDauGiaId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamDauGiaWithHinhAnhView>(false);
                    foreach (var subItem in sanPhamDauGiaParamView.TinhTrangDauGiaId)
                    {
                        predicate = predicate.Or(s => s.TinhTrangDauGiaId == subItem);
                    }
                    sanPhamDauGias = sanPhamDauGias.Where(predicate);
                }
                if (sanPhamDauGiaParamView.Order == "asc")
                {
                    if (sanPhamDauGiaParamView.OrderBy == "createdAt")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.CreatedAt);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianBatDau")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.ThoiGianBatDau);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianKetThuc")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.ThoiGianKetThuc);
                    }
                }
                else if (sanPhamDauGiaParamView.Order == "desc")
                {
                    if (sanPhamDauGiaParamView.OrderBy == "createdAt")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.CreatedAt);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianBatDau")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.ThoiGianBatDau);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianKetThuc")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.ThoiGianKetThuc);
                    }
                }
                var totalRecord = sanPhamDauGias.ToList().Count();
                if (sanPhamDauGiaParamView.PageIndex > 0)
                {
                    sanPhamDauGias = sanPhamDauGias.Skip(sanPhamDauGiaParamView.PageSize * (sanPhamDauGiaParamView.PageIndex - 1)).Take(sanPhamDauGiaParamView.PageSize);
                }
                var listSanPhamDauGia = sanPhamDauGias.ToList();
                return new SanPhamDauGiaWithHinhAnhListView(listSanPhamDauGia, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SanPhamDauGiaWithHinhAnhListView(null, 0, 400);
            }
        }

        public ResponseWithPaginationView GetAllSanPhamDauGiaByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            try
            {
                var sanPhamDauGias =
                    from s in _context.SanPhamDauGias
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    join t in _context.TinhTrangDauGias on s.TinhTrangDauGiaId equals t.Id
                    where s.ShopId == sanPhamDauGiaParamView.ShopId
                    select new SanPhamDauGiaView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        ThongTinKhac = s.ThongTinKhac,
                        TenSanPham = s.TenSanPham,
                        UserId = sh.UserId,
                        CreatedAt = s.CreatedAt,
                        UpdatedAt = s.UpdatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        GiaHienTai = s.GiaHienTai,
                        KhoangGiaToiThieu = s.KhoangGiaToiThieu,
                        GiaKhoiDiem = s.GiaKhoiDiem,
                        ThoiGianBatDau = s.ThoiGianBatDau,
                        ThoiGianKetThuc = s.ThoiGianKetThuc,
                        TinhTrangDauGiaId = s.TinhTrangDauGiaId,
                        TinhTrang = t.TinhTrang
                    };
                if (!sanPhamDauGias.Any())
                {
                    return new SanPhamDauGiaListView(null, 0, 404);
                }
                if (sanPhamDauGiaParamView.SearchString != null)
                {
                    sanPhamDauGias = sanPhamDauGias.Where(s => (s.TenSanPham.Contains(sanPhamDauGiaParamView.SearchString) || s.ThongTinKhac.Contains(sanPhamDauGiaParamView.SearchString)));
                }
                if (sanPhamDauGiaParamView.LoaiSanPhamId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamDauGiaView>(false);
                    foreach (var subItem in sanPhamDauGiaParamView.LoaiSanPhamId)
                    {
                        predicate = predicate.Or(s => s.LoaiSanPhamId == subItem);
                    }
                    sanPhamDauGias = sanPhamDauGias.Where(predicate);
                }
                if (sanPhamDauGiaParamView.TinhTrangDauGiaId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamDauGiaView>(false);
                    foreach (var subItem in sanPhamDauGiaParamView.TinhTrangDauGiaId)
                    {
                        predicate = predicate.Or(s => s.TinhTrangDauGiaId == subItem);
                    }
                    sanPhamDauGias = sanPhamDauGias.Where(predicate);
                }
                if (sanPhamDauGiaParamView.Order == "asc")
                {
                    if (sanPhamDauGiaParamView.OrderBy == "tenSanPham")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.TenSanPham);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "createdAt")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.CreatedAt);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianBatDau")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.ThoiGianBatDau);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianKetThuc")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.ThoiGianKetThuc);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "giaHienTai")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.GiaHienTai);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "giaKhoiDiem")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderBy(s => s.GiaKhoiDiem);
                    }
                }
                else if (sanPhamDauGiaParamView.Order == "desc")
                {
                    if (sanPhamDauGiaParamView.OrderBy == "tenSanPham")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.TenSanPham);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "createdAt")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.CreatedAt);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianBatDau")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.ThoiGianBatDau);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "thoiGianKetThuc")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.ThoiGianKetThuc);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "giaHienTai")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.GiaHienTai);
                    }
                    else if (sanPhamDauGiaParamView.OrderBy == "giaKhoiDiem")
                    {
                        sanPhamDauGias = sanPhamDauGias.OrderByDescending(s => s.GiaKhoiDiem);
                    }
                }
                var totalRecord = sanPhamDauGias.ToList().Count();
                if (sanPhamDauGiaParamView.PageIndex > 0)
                {
                    sanPhamDauGias = sanPhamDauGias.Skip(sanPhamDauGiaParamView.PageSize * (sanPhamDauGiaParamView.PageIndex - 1)).Take(sanPhamDauGiaParamView.PageSize);
                }
                var listSanPhamDauGia = sanPhamDauGias.ToList();
                return new SanPhamDauGiaListView(listSanPhamDauGia, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SanPhamDauGiaListView(null, 0, 400);
            }
        }

        public List<SanPhamDauGia> GetAllSanPhamDauGiaToCheckStatus(Guid tinhTrangDauGiaId)
        {
            try
            {
                var sanPhamDauGias = _context.SanPhamDauGias.Where(s => s.TinhTrangDauGiaId == tinhTrangDauGiaId).ToList();
                return sanPhamDauGias;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public List<TinhTrangDauGiaView> GetAllTinhTrangDauGia()
        {
            try
            {
                var tinhTrangs = 
                    from s in _context.TinhTrangDauGias
                    select new TinhTrangDauGiaView
                    {
                        TinhTrang = s.TinhTrang,
                        Id = s.Id
                    };
                if (!tinhTrangs.Any())
                {
                    return null;
                }
                return tinhTrangs.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponseWithPaginationView GetNewestAllSanPhamDauGiaWithHinhAnhByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            try
            {
                var sanPhamDauGias =
                    from s in _context.SanPhamDauGias
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    join t in _context.TinhTrangDauGias on s.TinhTrangDauGiaId equals t.Id
                    where s.ShopId == sanPhamDauGiaParamView.ShopId
                    orderby s.CreatedAt descending
                    select new SanPhamDauGiaWithHinhAnhView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        ThongTinKhac = s.ThongTinKhac,
                        TenSanPham = s.TenSanPham,
                        UserId = sh.UserId,
                        CreatedAt = s.CreatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        TinhTrangDauGiaId = s.TinhTrangDauGiaId,
                        GiaKhoiDiem = s.GiaKhoiDiem,
                        GiaHienTai = s.GiaHienTai,
                        ThoiGianKetThuc = s.ThoiGianKetThuc,
                        ThoiGianBatDau = s.ThoiGianBatDau,
                        KhoangGiaToiThieu = s.KhoangGiaToiThieu,
                        TinhTrang = t.TinhTrang,
                        HinhAnhSanPhamDauGia = (from h in _context.HinhAnhSanPhamDauGias
                                                where h.SanPhamDauGiaId == s.Id
                                                orderby h.ThuTu
                                                select new HinhAnhSanPhamDauGiaView
                                                {
                                                    Id = h.Id,
                                                    SanPhamDauGiaId = h.SanPhamDauGiaId,
                                                    HinhAnh = h.HinhAnh,
                                                    ThuTu = h.ThuTu
                                                }).ToList()
                    };
                if (!sanPhamDauGias.Any())
                {
                    return new SanPhamDauGiaWithHinhAnhListView(null, 0, 404);
                }
                if (sanPhamDauGiaParamView.SearchString != null)
                {
                    sanPhamDauGias = sanPhamDauGias.Where(s => (s.TenSanPham.Contains(sanPhamDauGiaParamView.SearchString) || s.ThongTinKhac.Contains(sanPhamDauGiaParamView.SearchString)));
                }
                if (sanPhamDauGiaParamView.LoaiSanPhamId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamDauGiaWithHinhAnhView>(false);
                    foreach (var subItem in sanPhamDauGiaParamView.LoaiSanPhamId)
                    {
                        predicate = predicate.Or(s => s.LoaiSanPhamId == subItem);
                    }
                    sanPhamDauGias = sanPhamDauGias.Where(predicate);
                }
                if (sanPhamDauGiaParamView.TinhTrangDauGiaId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamDauGiaWithHinhAnhView>(false);
                    foreach (var subItem in sanPhamDauGiaParamView.TinhTrangDauGiaId)
                    {
                        predicate = predicate.Or(s => s.TinhTrangDauGiaId == subItem);
                    }
                    sanPhamDauGias = sanPhamDauGias.Where(predicate);
                }
                var totalRecord = sanPhamDauGias.ToList().Count();
                if (sanPhamDauGiaParamView.PageIndex > 0)
                {
                    sanPhamDauGias = sanPhamDauGias.Skip(sanPhamDauGiaParamView.PageSize * (sanPhamDauGiaParamView.PageIndex - 1)).Take(sanPhamDauGiaParamView.PageSize);
                }
                var listSanPhamDauGia = sanPhamDauGias.ToList();
                return new SanPhamDauGiaWithHinhAnhListView(listSanPhamDauGia, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SanPhamDauGiaWithHinhAnhListView(null, 0, 400);
            }
        }

        public SanPhamDauGiaView GetSanPhamDauGiaById(SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var sanPhamDauGia =
                    from s in _context.SanPhamDauGias
                    join t in _context.TinhTrangDauGias on s.TinhTrangDauGiaId equals t.Id
                    where s.Id == sanPhamDauGiaView.Id
                    select new SanPhamDauGiaView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        ThongTinKhac = s.ThongTinKhac,
                        TenSanPham = s.TenSanPham,
                        CreatedAt = s.CreatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        TinhTrangDauGiaId = s.TinhTrangDauGiaId,
                        GiaKhoiDiem = s.GiaKhoiDiem,
                        GiaHienTai = s.GiaHienTai,
                        ThoiGianKetThuc = s.ThoiGianKetThuc,
                        ThoiGianBatDau = s.ThoiGianBatDau,
                        KhoangGiaToiThieu = s.KhoangGiaToiThieu,
                        TinhTrang = t.TinhTrang
                    };
                return sanPhamDauGia.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public SanPhamDauGiaWithHinhAnhView GetSanPhamDauGiaWithHinhAnhByName(string tenShop, string tenSanPham)
        {
            try
            {
                var sanPhamDauGia =
                    from s in _context.SanPhamDauGias
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    join t in _context.TinhTrangDauGias on s.TinhTrangDauGiaId equals t.Id
                    join l in _context.LoaiSanPhams on s.LoaiSanPhamId equals l.Id
                    where s.TenSanPham == tenSanPham &&
                        sh.TenShop == tenShop
                    select new SanPhamDauGiaWithHinhAnhView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        ThongTinKhac = s.ThongTinKhac,
                        TenSanPham = s.TenSanPham,
                        CreatedAt = s.CreatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        TinhTrangDauGiaId = s.TinhTrangDauGiaId,
                        GiaKhoiDiem = s.GiaKhoiDiem,
                        GiaHienTai = s.GiaHienTai,
                        ThoiGianKetThuc = s.ThoiGianKetThuc,
                        ThoiGianBatDau = s.ThoiGianBatDau,
                        KhoangGiaToiThieu = s.KhoangGiaToiThieu,
                        TinhTrang = t.TinhTrang,
                        UserId = s.UserId,
                        TenLoaiSanPham = l.TenLoaiSanPham,
                        YeuCauKetThuc = s.YeuCauKetThuc,
                        HinhAnhSanPhamDauGia = (from h in _context.HinhAnhSanPhamDauGias
                                                where h.SanPhamDauGiaId == s.Id
                                                orderby h.ThuTu
                                                select new HinhAnhSanPhamDauGiaView
                                                {
                                                    Id = h.Id,
                                                    SanPhamDauGiaId = h.SanPhamDauGiaId,
                                                    HinhAnh = h.HinhAnh,
                                                    ThuTu = h.ThuTu
                                                }).ToList(),
                        NguoiRaGia = (from u in _context.Users
                                      where u.Id == s.UserId
                                      select new UserView
                                      {
                                          UserName = u.UserName,
                                          UserInfo = (from ui in _context.UserInfos
                                                      where ui.UserId == u.Id
                                                      select new UserInfoView
                                                      {
                                                          Ho = ui.Ho,
                                                          TenDem = ui.TenDem,
                                                          Ten = ui.Ten,
                                                          AnhDaiDien = ui.AnhDaiDien
                                                      }).FirstOrDefault()
                                      }).FirstOrDefault()
                    };
                if (!sanPhamDauGia.Any())
                {
                    return null;
                }
                return sanPhamDauGia.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == sanPhamDauGiaView.Id).FirstOrDefault();
                if (sanPhamDauGia == null && sanPhamDauGia.ShopId != sanPhamDauGiaView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var shop = _context.Shops.Where(s => s.Id == sanPhamDauGiaView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if (shop.UserId != sanPhamDauGiaView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _mapper.Map(sanPhamDauGiaView, sanPhamDauGia);
                sanPhamDauGia.UpdatedAt = DateTime.Now;
                if (sanPhamDauGia.TinhTrangDauGiaId == converter.IntToGuid(2) ||
                    sanPhamDauGia.TinhTrangDauGiaId == converter.IntToGuid(3) ||
                    sanPhamDauGia.TinhTrangDauGiaId == converter.IntToGuid(7))
                {
                    sanPhamDauGia.TinhTrangDauGiaId = converter.IntToGuid(1);
                    sanPhamDauGia.CreatedAt = DateTime.Now;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView UpdateTinhHinhDauGia(ref SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var tmpView = sanPhamDauGiaView;
                var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == tmpView.Id).FirstOrDefault();
                if (sanPhamDauGia == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var timeNow = DateTime.Now;
                var user = _context.Users.Where(s => s.Id == tmpView.UserId).FirstOrDefault();
                if(sanPhamDauGia.ThoiGianKetThuc >= timeNow && sanPhamDauGia.ThoiGianBatDau <= timeNow &&
                    sanPhamDauGia.GiaHienTai + sanPhamDauGia.KhoangGiaToiThieu <= tmpView.GiaHienTai && user != null)
                {
                    sanPhamDauGia.GiaHienTai = tmpView.GiaHienTai;
                    sanPhamDauGia.UpdatedAt = timeNow;
                    sanPhamDauGia.UserId = tmpView.UserId;
                    sanPhamDauGiaView.YeuCauKetThuc = sanPhamDauGia.YeuCauKetThuc;
                    sanPhamDauGiaView.NguoiRaGia = (from u in _context.Users
                                                    where u.Id == tmpView.UserId
                                                    select new UserView
                                                    {
                                                        UserName = u.UserName,
                                                        UserInfo = (from ui in _context.UserInfos
                                                                    where ui.UserId == u.Id
                                                                    select new UserInfoView
                                                                    {
                                                                        Ho = ui.Ho,
                                                                        TenDem = ui.TenDem,
                                                                        Ten = ui.Ten,
                                                                        AnhDaiDien = ui.AnhDaiDien
                                                                    }).FirstOrDefault()
                                                    }).FirstOrDefault();
                    if(sanPhamDauGiaView.NguoiRaGia == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    _context.SaveChanges();
                    return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
                }
                else
                {
                    return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView UpdateTinhTrangDauGia(ref SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var tmpView = sanPhamDauGiaView;
                var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == tmpView.Id).FirstOrDefault();
                if (sanPhamDauGia == null && sanPhamDauGia.ShopId != tmpView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var shop = _context.Shops.Where(s => s.Id == tmpView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                sanPhamDauGia.TinhTrangDauGiaId = tmpView.TinhTrangDauGiaId;
                sanPhamDauGia.UpdatedAt = DateTime.Now;
                if(tmpView.TinhTrangDauGiaId == converter.IntToGuid(6))
                {
                    var newGioHang = new GioHang
                    {
                        DonGia = sanPhamDauGia.GiaHienTai,
                        GiamGia = sanPhamDauGia.GiaHienTai,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        SanPhamDauGiaId = sanPhamDauGia.Id,
                        SoLuong = 1,
                        UserId = sanPhamDauGia.UserId != null ? sanPhamDauGia.UserId.Value : default(Guid)
                    };
                    _context.GioHangs.Add(newGioHang);
                }
                else if (tmpView.TinhTrangDauGiaId == converter.IntToGuid(7))
                {
                    var gioHang = _context.GioHangs.Where(s => s.SanPhamDauGiaId == tmpView.Id).FirstOrDefault();
                    if(gioHang != null)
                    {
                        _context.GioHangs.Remove(gioHang);
                    }
                }
                var tinhTrang = _context.TinhTrangDauGias.Where(s => s.Id == tmpView.TinhTrangDauGiaId).FirstOrDefault();
                if (tinhTrang == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                sanPhamDauGiaView.TinhTrang = tinhTrang.TinhTrang;
                sanPhamDauGiaView.GiaHienTai = sanPhamDauGia.GiaHienTai;
                sanPhamDauGiaView.YeuCauKetThuc = sanPhamDauGia.YeuCauKetThuc;
                sanPhamDauGiaView.UserId = sanPhamDauGia.UserId != null ? sanPhamDauGia.UserId.Value : default(Guid);
                sanPhamDauGiaView.NguoiRaGia = (from u in _context.Users
                                                where u.Id == sanPhamDauGia.UserId
                                                select new UserView
                                                {
                                                    UserName = u.UserName,
                                                    UserInfo = (from ui in _context.UserInfos
                                                                where ui.UserId == u.Id
                                                                select new UserInfoView
                                                                {
                                                                    Ho = ui.Ho,
                                                                    TenDem = ui.TenDem,
                                                                    Ten = ui.Ten,
                                                                    AnhDaiDien = ui.AnhDaiDien
                                                                }).FirstOrDefault()
                                                }).FirstOrDefault();
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView YeuCauKetThucDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            try
            {
                var sanPhamDauGia = _context.SanPhamDauGias.Where(s => s.Id == sanPhamDauGiaView.Id).FirstOrDefault();
                if (sanPhamDauGia == null && sanPhamDauGia.ShopId != sanPhamDauGiaView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var shop = _context.Shops.Where(s => s.Id == sanPhamDauGiaView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if (shop.UserId != sanPhamDauGiaView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                sanPhamDauGia.YeuCauKetThuc = true;
                sanPhamDauGia.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
