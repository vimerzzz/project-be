﻿using AutoMapper;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Linq;

namespace Project.Repositories
{
    public interface ISoTheoDoiRepository
    {
        public ResponseWithPaginationView GetAllSoTheoDoi(SoTheoDoiParamView soTheoDoiParamView);
        public ResponsePostView CreateSoTheoDoi(SoTheoDoiView soTheoDoiView);
        public ResponsePostView DeleteSoTheoDoi(SoTheoDoiView soTheoDoiView);
    }

    public class SoTheoDoiRepository : ISoTheoDoiRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public SoTheoDoiRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateSoTheoDoi(SoTheoDoiView soTheoDoiView)
        {
            try
            {
                var newSoTheoDoi = new SoTheoDoi();
                newSoTheoDoi = _mapper.Map<SoTheoDoi>(soTheoDoiView);
                newSoTheoDoi.CreatedAt = DateTime.Now;
                newSoTheoDoi.UpdatedAt = DateTime.Now;
                _context.SoTheoDois.Add(newSoTheoDoi);
                var shop = _context.Shops.Where(s => s.Id == soTheoDoiView.ShopId).FirstOrDefault();
                if(shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                shop.SoNguoiTheoDoi++;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteSoTheoDoi(SoTheoDoiView soTheoDoiView)
        {
            try
            {
                var soTheoDoi = _context.SoTheoDois.Where(s => s.Id == soTheoDoiView.Id).FirstOrDefault();
                if (soTheoDoi == null && soTheoDoi.ShopId != soTheoDoiView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if(soTheoDoi.UserId != soTheoDoiView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _context.SoTheoDois.Remove(soTheoDoi);
                var shop = _context.Shops.Where(s => s.Id == soTheoDoiView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                shop.SoNguoiTheoDoi--;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllSoTheoDoi(SoTheoDoiParamView soTheoDoiParamView)
        {
            try
            {
                var soTheoDois =
                    from s in _context.SoTheoDois
                    join h in _context.Shops on s.ShopId equals h.Id
                    join u in _context.Users on h.UserId equals u.Id
                    where s.UserId == soTheoDoiParamView.UserId &&
                        h.HoatDong ==  true &&
                        u.HoatDong == true
                    select new SoTheoDoiView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        ShopId = s.ShopId,
                        CreatedAt = s.CreatedAt,
                        Shop = (from sh in _context.Shops
                                where sh.Id == s.ShopId
                                select new ShopView
                                {
                                    Id = sh.Id,
                                    HoatDong = sh.HoatDong,
                                    AnhDaiDien = sh.AnhDaiDien,
                                    CoAnhDaiDien = sh.CoAnhDaiDien,
                                    CreatedAt = sh.CreatedAt,
                                    DanhGiaChung = sh.DanhGiaChung,
                                    MoTaShop = sh.MoTaShop,
                                    SoNguoiDanhGia = sh.SoNguoiDanhGia,
                                    MotSao = sh.MotSao,
                                    HaiSao = sh.HaiSao,
                                    BaSao = sh.BaSao,
                                    BonSao = sh.BonSao,
                                    NamSao = sh.NamSao,
                                    SoNguoiTheoDoi = sh.SoNguoiTheoDoi,
                                    TenShop = sh.TenShop,
                                    TongSoSanPham = sh.TongSoSanPham,
                                    UserId = sh.UserId,
                                }).FirstOrDefault()
                    };
                if (!soTheoDois.Any())
                {
                    return new SoTheoDoiListView(null, 0, 404);
                }
                if (soTheoDoiParamView.SearchString != null)
                {
                    soTheoDois = soTheoDois.Where(s => s.Shop.TenShop.Contains(soTheoDoiParamView.SearchString));
                }
                if (soTheoDoiParamView.ShopId != null)
                {
                    soTheoDois = soTheoDois.Where(s => s.ShopId == soTheoDoiParamView.ShopId);
                }
                var totalRecord = soTheoDois.ToList().Count();
                if (soTheoDoiParamView.PageIndex > 0)
                {
                    soTheoDois = soTheoDois.Skip(soTheoDoiParamView.PageSize * (soTheoDoiParamView.PageIndex - 1)).Take(soTheoDoiParamView.PageSize);
                }
                var listSoTheoDoi = soTheoDois.ToList();
                return new SoTheoDoiListView(listSoTheoDoi, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SoTheoDoiListView(null, 0, 400);
            }
        }
    }
}
