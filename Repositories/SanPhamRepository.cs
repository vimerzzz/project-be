﻿using AutoMapper;
using LinqKit;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface ISanPhamRepository
    {
        public ResponseWithPaginationView GetAllSanPhamByShopId(SanPhamParamView sanPhamParamView);
        public SanPhamView GetSanPhamById(SanPhamView sanPhamView);
        public ResponsePostView CreateSanPham(SanPhamView sanPhamView);
        public ResponsePostView UpdateSanPham(SanPhamView sanPhamView);
        public ResponsePostView DeleteSanPham(SanPhamView sanPhamView);
        public ResponseWithPaginationView GetNewestAllSanPhamWithHinhAnhByShopId(SanPhamParamView sanPhamParamView);
        public ResponseWithPaginationView GetAllSanPham(SanPhamParamView sanPhamParamView);
        public SanPhamWithHinhAnhView GetSanPhamWithHinhAnhByName(string tenShop, string tenSanPham);
    }

    public class SanPhamRepository : ISanPhamRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public SanPhamRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateSanPham(SanPhamView sanPhamView)
        {
            try
            {
                var newSanPham = new SanPham();
                newSanPham = _mapper.Map<SanPham>(sanPhamView);
                newSanPham.CreatedAt = DateTime.Now;
                newSanPham.UpdatedAt = DateTime.Now;
                _context.SanPhams.Add(newSanPham);
                var shop = _context.Shops.Where(s => s.Id == sanPhamView.ShopId).FirstOrDefault();
                if(shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if(shop.UserId != sanPhamView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                shop.TongSoSanPham++;
                _context.SaveChanges();
                return new ResponseIdPostView(MessageConstants.CREATE_SUCCESS, 200, newSanPham.Id);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteSanPham(SanPhamView sanPhamView)
        {
            try
            {
                var sanPham = _context.SanPhams.Where(s => s.Id == sanPhamView.Id).FirstOrDefault();
                if (sanPham == null && sanPham.ShopId != sanPhamView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _context.SanPhams.Remove(sanPham);
                var shop = _context.Shops.Where(s => s.Id == sanPhamView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if (shop.UserId != sanPhamView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                var folderName = Path.Combine("Statics", "Images", "Items", shop.Id.ToString(), sanPham.Id.ToString());
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                var hinhAnhSanPhams = _context.HinhAnhSanPhams.Where(s => s.SanPhamId == sanPhamView.Id).ToList();
                _context.HinhAnhSanPhams.RemoveRange(hinhAnhSanPhams);
                shop.TongSoSanPham = shop.TongSoSanPham == 0 ? 0 : shop.TongSoSanPham - 1;
                _context.SaveChanges();
                if (Directory.Exists(pathToSave))
                {
                    Directory.Delete(pathToSave, true);
                }
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllSanPhamByShopId(SanPhamParamView sanPhamParamView)
        {
            try
            {
                var sanPhams =
                    from s in _context.SanPhams
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    where s.ShopId == sanPhamParamView.ShopId
                    select new SanPhamView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        DanhGiaChung = s.DanhGiaChung,
                        DonGia = s.DonGia,
                        GiamGia = s.GiamGia,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        ThongTinKhac = s.ThongTinKhac,
                        SoLuong = s.SoLuong,
                        SoLuongDaBan = s.SoLuongDaBan,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        TenSanPham = s.TenSanPham,
                        UserId = sh.UserId,
                        CreatedAt = s.CreatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo
                    };
                if (!sanPhams.Any())
                {
                    return new SanPhamListView(null, 0, 404);
                }
                if (sanPhamParamView.SearchString != null)
                {
                    sanPhams = sanPhams.Where(s => (s.TenSanPham.Contains(sanPhamParamView.SearchString) || s.ThongTinKhac.Contains(sanPhamParamView.SearchString)));
                }
                if (sanPhamParamView.LoaiSanPhamId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamView>(false);
                    foreach (var subItem in sanPhamParamView.LoaiSanPhamId)
                    {
                        predicate = predicate.Or(s => s.LoaiSanPhamId == subItem);
                    }
                    sanPhams = sanPhams.Where(predicate);
                }
                if (sanPhamParamView.Order == "asc")
                {
                    if (sanPhamParamView.OrderBy == "tenSanPham")
                    {
                        sanPhams = sanPhams.OrderBy(s => s.TenSanPham);
                    }
                    else if (sanPhamParamView.OrderBy == "createdAt")
                    {
                        sanPhams = sanPhams.OrderBy(s => s.CreatedAt);
                    }
                    else if (sanPhamParamView.OrderBy == "danhGiaChung")
                    {
                        sanPhams = sanPhams.OrderBy(s => s.DanhGiaChung);
                    }
                    else if (sanPhamParamView.OrderBy == "soLuongDaBan")
                    {
                        sanPhams = sanPhams.OrderBy(s => s.SoLuongDaBan);
                    }
                }
                else if (sanPhamParamView.Order == "desc")
                {
                    if (sanPhamParamView.OrderBy == "tenSanPham")
                    {
                        sanPhams = sanPhams.OrderByDescending(s => s.TenSanPham);
                    }
                    else if (sanPhamParamView.OrderBy == "createdAt")
                    {
                        sanPhams = sanPhams.OrderByDescending(s => s.CreatedAt);
                    }
                    else if (sanPhamParamView.OrderBy == "danhGiaChung")
                    {
                        sanPhams = sanPhams.OrderByDescending(s => s.DanhGiaChung);
                    }
                    else if (sanPhamParamView.OrderBy == "soLuongDaBan")
                    {
                        sanPhams = sanPhams.OrderByDescending(s => s.SoLuongDaBan);
                    }
                }
                var totalRecord = sanPhams.ToList().Count();
                if (sanPhamParamView.PageIndex > 0)
                {
                    sanPhams = sanPhams.Skip(sanPhamParamView.PageSize * (sanPhamParamView.PageIndex - 1)).Take(sanPhamParamView.PageSize);
                }
                var listSanPham = sanPhams.ToList();
                return new SanPhamListView(listSanPham, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SanPhamListView(null, 0, 400);
            }
        }

        public ResponseWithPaginationView GetNewestAllSanPhamWithHinhAnhByShopId(SanPhamParamView sanPhamParamView)
        {
            try
            {
                var sanPhams =
                    from s in _context.SanPhams
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    where s.ShopId == sanPhamParamView.ShopId
                    orderby s.CreatedAt descending
                    select new SanPhamWithHinhAnhView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        DanhGiaChung = s.DanhGiaChung,
                        DonGia = s.DonGia,
                        GiamGia = s.GiamGia,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        ThongTinKhac = s.ThongTinKhac,
                        SoLuong = s.SoLuong,
                        SoLuongDaBan = s.SoLuongDaBan,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        TenSanPham = s.TenSanPham,
                        UserId = sh.UserId,
                        CreatedAt = s.CreatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        HinhAnhSanPham = (from h in _context.HinhAnhSanPhams
                                          where h.SanPhamId == s.Id
                                          orderby h.ThuTu
                                          select new HinhAnhSanPhamView
                                          {
                                              Id = h.Id,
                                              SanPhamId = h.SanPhamId,
                                              HinhAnh = h.HinhAnh,
                                              ThuTu = h.ThuTu
                                          }).ToList()
                    };
                if (!sanPhams.Any())
                {
                    return new SanPhamWithHinhAnhListView(null, 0, 404);
                }
                if (sanPhamParamView.SearchString != null)
                {
                    sanPhams = sanPhams.Where(s => (s.TenSanPham.Contains(sanPhamParamView.SearchString) || s.ThongTinKhac.Contains(sanPhamParamView.SearchString)));
                }
                if (sanPhamParamView.LoaiSanPhamId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamWithHinhAnhView>(false);
                    foreach (var subItem in sanPhamParamView.LoaiSanPhamId)
                    {
                        predicate = predicate.Or(s => s.LoaiSanPhamId == subItem);
                    }
                    sanPhams = sanPhams.Where(predicate);
                }
                var totalRecord = sanPhams.ToList().Count();
                if (sanPhamParamView.PageIndex > 0)
                {
                    sanPhams = sanPhams.Skip(sanPhamParamView.PageSize * (sanPhamParamView.PageIndex - 1)).Take(sanPhamParamView.PageSize);
                }
                var listSanPham = sanPhams.ToList();
                return new SanPhamWithHinhAnhListView(listSanPham, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SanPhamWithHinhAnhListView(null, 0, 400);
            }
        }

        public ResponseWithPaginationView GetAllSanPham(SanPhamParamView sanPhamParamView)
        {
            try
            {
                var sanPhams =
                    from s in _context.SanPhams
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    join l in _context.LoaiSanPhams on s.LoaiSanPhamId equals l.Id
                    select new SanPhamWithHinhAnhView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        DanhGiaChung = s.DanhGiaChung,
                        DonGia = s.DonGia,
                        GiamGia = s.GiamGia,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        TenLoaiSanPham = l.TenLoaiSanPham,
                        ThongTinKhac = s.ThongTinKhac,
                        SoLuong = s.SoLuong,
                        SoLuongDaBan = s.SoLuongDaBan,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        TenSanPham = s.TenSanPham,
                        UserId = sh.UserId,
                        CreatedAt = s.CreatedAt,
                        TenShop = sh.TenShop,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        HinhAnhSanPham = (from h in _context.HinhAnhSanPhams
                                          where h.SanPhamId == s.Id
                                          orderby h.ThuTu
                                          select new HinhAnhSanPhamView
                                          {
                                              Id = h.Id,
                                              SanPhamId = h.SanPhamId,
                                              HinhAnh = h.HinhAnh,
                                              ThuTu = h.ThuTu
                                          }).ToList()
                    };
                if (!sanPhams.Any())
                {
                    return new SanPhamWithHinhAnhListView(null, 0, 404);
                }
                if (sanPhamParamView.SearchString != null)
                {
                    sanPhams = sanPhams.Where(s => (s.TenSanPham.Contains(sanPhamParamView.SearchString) ||
                                                    s.TenLoaiSanPham.Contains(sanPhamParamView.SearchString) ||
                                                    s.TenShop.Contains(sanPhamParamView.SearchString) ||
                                                    s.ThongTinKhac.Contains(sanPhamParamView.SearchString)));
                }
                if (sanPhamParamView.LoaiSanPhamId != null)
                {
                    var predicate = PredicateBuilder.New<SanPhamWithHinhAnhView>(false);
                    foreach (var subItem in sanPhamParamView.LoaiSanPhamId)
                    {
                        predicate = predicate.Or(s => s.LoaiSanPhamId == subItem);
                    }
                    sanPhams = sanPhams.Where(predicate);
                }
                if (sanPhamParamView.ConHang != null)
                {
                    if(sanPhamParamView.ConHang == true)
                    {
                        sanPhams = sanPhams.Where(s => s.SoLuong > 0);
                    }
                }
                if (sanPhamParamView.Order == "asc")
                {
                    if (sanPhamParamView.OrderBy == "createdAt")
                    {
                        sanPhams = sanPhams.OrderBy(s => s.CreatedAt);
                    }
                }
                else if (sanPhamParamView.Order == "desc")
                {
                    if (sanPhamParamView.OrderBy == "createdAt")
                    {
                        sanPhams = sanPhams.OrderByDescending(s => s.CreatedAt);
                    }
                }
                var totalRecord = sanPhams.ToList().Count();
                if (sanPhamParamView.PageIndex > 0)
                {
                    sanPhams = sanPhams.Skip(sanPhamParamView.PageSize * (sanPhamParamView.PageIndex - 1)).Take(sanPhamParamView.PageSize);
                }
                var listSanPham = sanPhams.ToList();
                return new SanPhamWithHinhAnhListView(listSanPham, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SanPhamWithHinhAnhListView(null, 0, 400);
            }
        }

        public SanPhamView GetSanPhamById(SanPhamView sanPhamView)
        {
            try
            {
                var sanPham =
                    from s in _context.SanPhams
                    where s.Id == sanPhamView.Id
                    select new SanPhamView
                    {
                        Id = s.Id,
                        CreatedAt = s.CreatedAt,
                        DanhGiaChung = s.DanhGiaChung,
                        DonGia = s.DonGia,
                        GiamGia = s.GiamGia,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        ThongTinKhac = s.ThongTinKhac,
                        ShopId = s.ShopId,
                        SoLuong = s.SoLuong,
                        SoLuongDaBan = s.SoLuongDaBan,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        TenSanPham = s.TenSanPham,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo
                    };
                return sanPham.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public SanPhamWithHinhAnhView GetSanPhamWithHinhAnhByName(string tenShop, string tenSanPham)
        {
            try
            {
                var sanPham =
                    from s in _context.SanPhams
                    join sh in _context.Shops on s.ShopId equals sh.Id
                    join l in _context.LoaiSanPhams on s.LoaiSanPhamId equals l.Id
                    where s.TenSanPham == tenSanPham &&
                        sh.TenShop == tenShop
                    select new SanPhamWithHinhAnhView
                    {
                        Id = s.Id,
                        ShopId = s.ShopId,
                        DanhGiaChung = s.DanhGiaChung,
                        DonGia = s.DonGia,
                        GiamGia = s.GiamGia,
                        LoaiSanPhamId = s.LoaiSanPhamId,
                        TenLoaiSanPham = l.TenLoaiSanPham,
                        ThongTinKhac = s.ThongTinKhac,
                        SoLuong = s.SoLuong,
                        SoLuongDaBan = s.SoLuongDaBan,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        TenSanPham = s.TenSanPham,
                        UserId = sh.UserId,
                        CreatedAt = s.CreatedAt,
                        XuatXu = s.XuatXu,
                        Nam = s.Nam,
                        DungTich = s.DungTich,
                        MauSac = s.MauSac,
                        NongDo = s.NongDo,
                        HinhAnhSanPham = (from h in _context.HinhAnhSanPhams
                                          where h.SanPhamId == s.Id
                                          orderby h.ThuTu
                                          select new HinhAnhSanPhamView
                                          {
                                              Id = h.Id,
                                              SanPhamId = h.SanPhamId,
                                              HinhAnh = h.HinhAnh,
                                              ThuTu = h.ThuTu
                                          }).ToList()
                    };
                if (!sanPham.Any())
                {
                    return null;
                }
                return sanPham.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateSanPham(SanPhamView sanPhamView)
        {
            try
            {
                var sanPham = _context.SanPhams.Where(s => s.Id == sanPhamView.Id).FirstOrDefault();
                if (sanPham == null && sanPham.ShopId != sanPhamView.ShopId)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                var shop = _context.Shops.Where(s => s.Id == sanPhamView.ShopId).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.SHOP_NOT_FOUND, 404);
                }
                if (shop.UserId != sanPhamView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _mapper.Map(sanPhamView, sanPham);
                sanPham.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
