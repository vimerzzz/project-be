﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Repositories
{
    public interface IRoleRepository
    {
        public List<RoleView> GetAllRole();
        public ResponsePostView CreateRole(RoleView roleView);
        public ResponsePostView UpdateRole(RoleView roleView);
        public ResponsePostView DeleteRole(Guid id);
    }

    public class RoleRepository : IRoleRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public RoleRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<RoleView> GetAllRole()
        {
            try
            {
                var roles =
                    from s in _context.Roles
                    orderby s.Id
                    select new RoleView
                    {
                        Id = s.Id,
                        Name = s.Name
                    };
                if (!roles.Any())
                {
                    return null;
                }
                var listRole = roles.ToList();
                return listRole;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }
        public ResponsePostView CreateRole(RoleView roleView)
        {
            try
            {
                var newRole = new Role();
                newRole = _mapper.Map<Role>(roleView);
                newRole.CreatedAt = DateTime.Now;
                newRole.UpdatedAt = DateTime.Now;
                _context.Roles.Add(newRole);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }
        public ResponsePostView UpdateRole(RoleView roleView)
        {
            try
            {
                var role = _context.Roles.Where(s => s.Id == roleView.Id).FirstOrDefault();
                if(role == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _mapper.Map(roleView, role);
                role.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
        public ResponsePostView DeleteRole(Guid id)
        {
            try
            {
                var role = _context.Roles.Where(s => s.Id == id).FirstOrDefault();
                if (role == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                _context.Roles.Remove(role);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }
    }
}
