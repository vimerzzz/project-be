﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Repositories
{
    public interface IThongBaoRepository
    {
        public ResponseWithPaginationView GetAllThongBao(ThongBaoParamView thongBaoParamView);
        public ResponseWithPaginationView GetAllThongBaoCoLichHen(ThongBaoParamView thongBaoParamView);
        public ResponsePostView CreateThongBao(ref List<ThongBaoView> thongBaoView);
        public ResponsePostView UpdateThongBao(List<ThongBaoView> thongBaoView);
        public ResponsePostView DeleteThongBao(List<Guid> id);
        public List<ThongBaoView> GetAllThongBaoToCheckStatus();
    }

    public class ThongBaoRepository : IThongBaoRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public ThongBaoRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponsePostView CreateThongBao(ref List<ThongBaoView> thongBaoView)
        {
            try
            {
                foreach(var item in thongBaoView)
                {
                    var newThongBao = new ThongBao();
                    newThongBao = _mapper.Map<ThongBao>(item);
                    newThongBao.CreatedAt = DateTime.Now;
                    newThongBao.UpdatedAt = DateTime.Now;
                    _context.ThongBaos.Add(newThongBao);
                    item.Id = newThongBao.Id;
                    item.CreatedAt = newThongBao.CreatedAt;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteThongBao(List<Guid> id)
        {
            try
            {
                foreach(Guid item in id)
                {
                    var thongBao = _context.ThongBaos.Where(s => s.Id == item).FirstOrDefault();
                    if (thongBao == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    _context.ThongBaos.Remove(thongBao);
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllThongBao(ThongBaoParamView thongBaoParamView)
        {
            try
            {
                var thongBaos =
                    from s in _context.ThongBaos
                    where s.UserId == thongBaoParamView.UserId
                        && s.CoLichHen == false
                    orderby s.CreatedAt descending
                    select new ThongBaoView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        CreatedAt = s.CreatedAt,
                        NoiDungThongBao = s.NoiDungThongBao,
                        TieuDeThongBao = s.TieuDeThongBao,
                        DaDoc = s.DaDoc,
                        DaXem = s.DaXem,
                        CoLichHen = s.CoLichHen,
                        ThoiGianHen = s.ThoiGianHen
                    };
                if (!thongBaos.Any())
                {
                    return new ThongBaoListView(null, 0, 404);
                }
                if(thongBaoParamView.DaXem != null)
                {
                    thongBaos = thongBaos.Where(s => s.DaXem == thongBaoParamView.DaXem);
                }
                if (thongBaoParamView.DaDoc != null)
                {
                    thongBaos = thongBaos.Where(s => s.DaDoc == thongBaoParamView.DaDoc);
                }
                var totalRecord = thongBaos.ToList().Count();
                if (thongBaoParamView.PageIndex > 0)
                {
                    thongBaos = thongBaos.Skip(thongBaoParamView.PageSize * (thongBaoParamView.PageIndex - 1)).Take(thongBaoParamView.PageSize);
                }
                var listThongBao = thongBaos.ToList();
                return new ThongBaoListView(listThongBao, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ThongBaoListView(null, 0, 400);
            }
        }

        public ResponseWithPaginationView GetAllThongBaoCoLichHen(ThongBaoParamView thongBaoParamView)
        {
            try
            {
                var thongBaos =
                    from s in _context.ThongBaos
                    join u in _context.Users on s.UserId equals u.Id
                    join ur in _context.UserRoles on u.Id equals ur.UserId
                    where s.CoLichHen == true
                        && u.HoatDong == true
                    select new ThongBaoView
                    {
                        NoiDungThongBao = s.NoiDungThongBao,
                        TieuDeThongBao = s.TieuDeThongBao,
                        ThoiGianHen = s.ThoiGianHen,
                        UserId = s.UserId,
                        DaXem = s.DaXem,
                        DaDoc = s.DaDoc,
                        CoLichHen = s.CoLichHen,
                        CreatedAt = s.CreatedAt,
                        Id = s.Id,
                        RoleId = ur.RoleId
                    };
                if (!thongBaos.Any())
                {
                    return new ThongBaoCoLichHenListView(null, 0, 404);
                }
                var groupThongBaos = thongBaos.ToList().GroupBy(s => new
                {
                    s.NoiDungThongBao,
                    s.TieuDeThongBao,
                    s.ThoiGianHen,
                    s.CoLichHen
                }).Select(g => new ThongBaoCoLichHenView
                {
                    NoiDungThongBao = g.Key.NoiDungThongBao,
                    TieuDeThongBao = g.Key.TieuDeThongBao,
                    ThoiGianHen = g.Key.ThoiGianHen,
                    CoLichHen = g.Key.CoLichHen,
                    IdThongBao = g.Select(x => new IdThongBaoCoLichHenView
                    {
                        DaDoc = x.DaDoc,
                        DaXem = x.DaXem,
                        Id = x.Id,
                        UserId = x.UserId,
                        RoleId = x.RoleId
                    }).ToList()
                });
                if(thongBaoParamView.ThoiGianHenBatDau != null)
                {
                    groupThongBaos = groupThongBaos.Where(s => s.ThoiGianHen >= thongBaoParamView.ThoiGianHenBatDau);
                }
                if (thongBaoParamView.ThoiGianHenKetThuc != null)
                {
                    groupThongBaos = groupThongBaos.Where(s => s.ThoiGianHen <= thongBaoParamView.ThoiGianHenKetThuc);
                }
                if (thongBaoParamView.SearchString != null)
                {
                    groupThongBaos = groupThongBaos.Where(s => (s.TieuDeThongBao.Contains(thongBaoParamView.SearchString) || s.NoiDungThongBao.Contains(thongBaoParamView.SearchString)));
                }
                if (thongBaoParamView.Order == "asc")
                {
                    if (thongBaoParamView.OrderBy == "thoiGianHen")
                    {
                        groupThongBaos = groupThongBaos.OrderBy(s => s.ThoiGianHen);
                    }
                }
                else if (thongBaoParamView.Order == "desc")
                {
                    if (thongBaoParamView.OrderBy == "thoiGianHen")
                    {
                        groupThongBaos = groupThongBaos.OrderByDescending(s => s.ThoiGianHen);
                    }
                }
                var totalRecord = groupThongBaos.ToList().Count();
                if (thongBaoParamView.PageIndex > 0)
                {
                    groupThongBaos = groupThongBaos.Skip(thongBaoParamView.PageSize * (thongBaoParamView.PageIndex - 1)).Take(thongBaoParamView.PageSize);
                }
                var listThongBao = groupThongBaos.ToList();
                return new ThongBaoCoLichHenListView(listThongBao, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ThongBaoCoLichHenListView(null, 0, 400);
            }
        }

        public List<ThongBaoView> GetAllThongBaoToCheckStatus()
        {
            try
            {
                var thongBaos =
                    from s in _context.ThongBaos
                    where s.CoLichHen == true
                    select new ThongBaoView
                    {
                        Id = s.Id,
                        CoLichHen = s.CoLichHen,
                        CreatedAt = s.CreatedAt,
                        DaDoc = s.DaDoc,
                        DaXem = s.DaXem,
                        NoiDungThongBao = s.NoiDungThongBao,
                        ThoiGianHen = s.ThoiGianHen,
                        TieuDeThongBao = s.TieuDeThongBao,
                        UserId = s.UserId
                    };
                if (!thongBaos.Any())
                {
                    return null;
                }
                return thongBaos.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateThongBao(List<ThongBaoView> thongBaoView)
        {
            try
            {
                foreach(var item in thongBaoView)
                {
                    var thongBao = _context.ThongBaos.Where(s => s.Id == item.Id).FirstOrDefault();
                    if (thongBao == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    if (thongBao.CoLichHen == true && item.CoLichHen == false)
                    {
                        thongBao.CreatedAt = DateTime.Now;
                    }
                    _mapper.Map(item, thongBao);
                    thongBao.UpdatedAt = DateTime.Now;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
