﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface IShopRepository
    {
        public ResponseWithPaginationView GetAllShop(ShopParamView shopParamView);
        public ShopView GetShopByUserId(Guid userId);
        public ShopView GetShopByShopName(string shopName);
        public ShopView GetShopByShopId(Guid shopId);
        public ResponsePostView CreateShop(ShopView shopView);
        public ResponsePostView UpdateShop(ShopView shopView);
        public ResponsePostView UpdateStatusShop(ShopView shopView);
    }

    public class ShopRepository : IShopRepository
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDBContext _context;

        public ShopRepository(IMapper mapper, ApplicationDBContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public ResponsePostView CreateShop(ShopView shopView)
        {
            try
            {
                var newShop = new Shop();
                newShop = _mapper.Map<Shop>(shopView);
                newShop.CreatedAt = DateTime.Now;
                newShop.UpdatedAt = DateTime.Now;
                newShop.HoatDong = true;
                newShop.AnhDaiDien = "Statics\\Images\\Shops\\blank_avatar.png";
                _context.Shops.Add(newShop);
                var user = _context.UserInfos.Where(s => s.UserId == shopView.UserId).FirstOrDefault();
                if(user == null)
                {
                    return new ResponsePostView(MessageConstants.USER_NOT_FOUND, 404);
                }
                user.CoShop = true;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponseWithPaginationView GetAllShop(ShopParamView shopParamView)
        {
            try
            {
                var shops =
                    from s in _context.Shops
                    join u in _context.Users on s.UserId equals u.Id
                    select new ShopView
                    {
                        Id = s.Id,
                        HoatDong = s.HoatDong,
                        AnhDaiDien = s.AnhDaiDien,
                        CoAnhDaiDien = s.CoAnhDaiDien,
                        CreatedAt = s.CreatedAt,
                        DanhGiaChung = s.DanhGiaChung,
                        MoTaShop = s.MoTaShop,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        SoNguoiTheoDoi = s.SoNguoiTheoDoi,
                        TenShop = s.TenShop,
                        TongSoSanPham = s.TongSoSanPham,
                        UserId = s.UserId,
                        UserHoatDong = u.HoatDong,
                        TaiKhoanChuShop = u.UserName
                    };
                if (!shops.Any())
                {
                    return new ShopListView(null, 0, 404);
                }
                if (shopParamView.HoatDong != null)
                {
                    shops = shops.Where(s => s.HoatDong == shopParamView.HoatDong);
                }
                if (shopParamView.SearchString != null)
                {
                    shops = shops.Where(s => (s.TaiKhoanChuShop.Contains(shopParamView.SearchString) || s.TenShop.Contains(shopParamView.SearchString) || s.MoTaShop.Contains(shopParamView.SearchString)));
                }
                if (shopParamView.Order == "asc")
                {
                    if (shopParamView.OrderBy == "tenShop")
                    {
                        shops = shops.OrderBy(s => s.TenShop);
                    }
                    else if (shopParamView.OrderBy == "hoatDong")
                    {
                        shops = shops.OrderBy(s => s.HoatDong);
                    }
                    else if (shopParamView.OrderBy == "createdAt")
                    {
                        shops = shops.OrderBy(s => s.CreatedAt);
                    }
                    else if (shopParamView.OrderBy == "taiKhoanChuShop")
                    {
                        shops = shops.OrderBy(s => s.TaiKhoanChuShop);
                    }
                }
                else if (shopParamView.Order == "desc")
                {
                    if (shopParamView.OrderBy == "tenShop")
                    {
                        shops = shops.OrderByDescending(s => s.TenShop);
                    }
                    else if (shopParamView.OrderBy == "hoatDong")
                    {
                        shops = shops.OrderByDescending(s => s.HoatDong);
                    }
                    else if (shopParamView.OrderBy == "createdAt")
                    {
                        shops = shops.OrderByDescending(s => s.CreatedAt);
                    }
                    else if (shopParamView.OrderBy == "taiKhoanChuShop")
                    {
                        shops = shops.OrderByDescending(s => s.TaiKhoanChuShop);
                    }
                }
                var totalRecord = shops.ToList().Count();
                if (shopParamView.PageIndex > 0)
                {
                    shops = shops.Skip(shopParamView.PageSize * (shopParamView.PageIndex - 1)).Take(shopParamView.PageSize);
                }
                var listShop = shops.ToList();
                return new ShopListView(listShop, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ShopListView(null, 0, 400);
            }
        }

        public ShopView GetShopByShopName(string shopName)
        {
            try
            {
                var shops =
                    from s in _context.Shops
                    join u in _context.Users on s.UserId equals u.Id
                    where s.TenShop == shopName
                    select new ShopView
                    {
                        Id = s.Id,
                        HoatDong = s.HoatDong,
                        AnhDaiDien = s.AnhDaiDien,
                        CoAnhDaiDien = s.CoAnhDaiDien,
                        CreatedAt = s.CreatedAt,
                        DanhGiaChung = s.DanhGiaChung,
                        MoTaShop = s.MoTaShop,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        SoNguoiTheoDoi = s.SoNguoiTheoDoi,
                        TenShop = s.TenShop,
                        TongSoSanPham = s.TongSoSanPham,
                        UserId = s.UserId,
                        UserHoatDong = u.HoatDong
                    };
                if (!shops.Any())
                {
                    return null;
                }
                var listShop = shops.FirstOrDefault();
                return listShop;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ShopView GetShopByShopId(Guid shopId)
        {
            try
            {
                var shops =
                    from s in _context.Shops
                    join u in _context.Users on s.UserId equals u.Id
                    where s.Id == shopId
                    select new ShopView
                    {
                        Id = s.Id,
                        HoatDong = s.HoatDong,
                        AnhDaiDien = s.AnhDaiDien,
                        CoAnhDaiDien = s.CoAnhDaiDien,
                        CreatedAt = s.CreatedAt,
                        DanhGiaChung = s.DanhGiaChung,
                        MoTaShop = s.MoTaShop,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        SoNguoiTheoDoi = s.SoNguoiTheoDoi,
                        TenShop = s.TenShop,
                        TongSoSanPham = s.TongSoSanPham,
                        UserId = s.UserId,
                        UserHoatDong = u.HoatDong
                    };
                if (!shops.Any())
                {
                    return null;
                }
                var listShop = shops.FirstOrDefault();
                return listShop;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ShopView GetShopByUserId(Guid userId)
        {
            try
            {
                var shops =
                    from s in _context.Shops
                    join u in _context.Users on s.UserId equals u.Id
                    where s.UserId == userId
                    select new ShopView
                    {
                        Id = s.Id,
                        HoatDong = s.HoatDong,
                        AnhDaiDien = s.AnhDaiDien,
                        CoAnhDaiDien = s.CoAnhDaiDien,
                        CreatedAt = s.CreatedAt,
                        DanhGiaChung = s.DanhGiaChung,
                        MoTaShop = s.MoTaShop,
                        SoNguoiDanhGia = s.SoNguoiDanhGia,
                        MotSao = s.MotSao,
                        HaiSao = s.HaiSao,
                        BaSao = s.BaSao,
                        BonSao = s.BonSao,
                        NamSao = s.NamSao,
                        SoNguoiTheoDoi = s.SoNguoiTheoDoi,
                        TenShop = s.TenShop,
                        TongSoSanPham = s.TongSoSanPham,
                        UserId = s.UserId,
                        UserHoatDong = u.HoatDong
                    };
                if (!shops.Any())
                {
                    return null;
                }
                var listShop = shops.FirstOrDefault();
                return listShop;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return null;
            }
        }

        public ResponsePostView UpdateShop(ShopView shopView)
        {
            try
            {
                var shop = _context.Shops.Where(s => s.Id == shopView.Id).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if(shop.UserId != shopView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _mapper.Map(shopView, shop);
                shop.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView UpdateStatusShop(ShopView shopView)
        {
            try
            {
                var shop = _context.Shops.Where(s => s.Id == shopView.Id).FirstOrDefault();
                if (shop == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                shop.HoatDong = shopView.HoatDong;
                shop.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }
    }
}
