﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Linq;

namespace Project.Repositories
{
    public interface ISoDiaChiRepository
    {
        public ResponseWithPaginationView GetAllDiaChi(SoDiaChiParamView soDiaChiParamView);
        public ResponsePostView CreateDiaChi(SoDiaChiView soDiaChiView);
        public ResponsePostView UpdateDiaChi(SoDiaChiView soDiaChiView);
        public ResponsePostView DeleteDiaChi(SoDiaChiView soDiaChiView);
    }

    public class SoDiaChiRepository : ISoDiaChiRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public SoDiaChiRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponseWithPaginationView GetAllDiaChi(SoDiaChiParamView soDiaChiParamView)
        {
            try
            {
                var soDiaChis =
                    from s in _context.SoDiaChis
                    where s.UserId == soDiaChiParamView.UserId
                    select new SoDiaChiView
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        DiaChi = s.DiaChi
                    };
                if (!soDiaChis.Any())
                {
                    return new SoDiaChiListView(null, 0, 404);
                }
                if (soDiaChiParamView.SearchString != null)
                {
                    soDiaChis = soDiaChis.Where(s => s.DiaChi.Contains(soDiaChiParamView.SearchString));
                }
                if (soDiaChiParamView.Order == "asc")
                {
                    if (soDiaChiParamView.OrderBy == "diaChi")
                    {
                        soDiaChis = soDiaChis.OrderBy(s => s.DiaChi);
                    }
                }
                else if (soDiaChiParamView.Order == "desc")
                {
                    if (soDiaChiParamView.OrderBy == "diaChi")
                    {
                        soDiaChis = soDiaChis.OrderByDescending(s => s.DiaChi);
                    }
                }
                var totalRecord = soDiaChis.ToList().Count();
                if (soDiaChiParamView.PageIndex > 0)
                {
                    soDiaChis = soDiaChis.Skip(soDiaChiParamView.PageSize * (soDiaChiParamView.PageIndex - 1)).Take(soDiaChiParamView.PageSize);
                }
                var listSoDiaChi = soDiaChis.ToList();
                return new SoDiaChiListView(listSoDiaChi, totalRecord, 200);
            }
            catch(Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new SoDiaChiListView(null, 0, 400);
            }
        }

        public ResponsePostView CreateDiaChi(SoDiaChiView soDiaChiView)
        {
            try
            {
                var newDiaChi = new SoDiaChi();
                newDiaChi = _mapper.Map<SoDiaChi>(soDiaChiView);
                newDiaChi.CreatedAt = DateTime.Now;
                newDiaChi.UpdatedAt = DateTime.Now;
                _context.SoDiaChis.Add(newDiaChi);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.CREATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.CREATE_FAILED, 400);
            }
        }

        public ResponsePostView UpdateDiaChi(SoDiaChiView soDiaChiView)
        {
            try
            {
                var diaChi = _context.SoDiaChis.Where(s => s.Id == soDiaChiView.Id).FirstOrDefault();
                if (diaChi == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if(diaChi.UserId != soDiaChiView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _mapper.Map(soDiaChiView, diaChi);
                diaChi.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponsePostView DeleteDiaChi(SoDiaChiView soDiaChiView)
        {
            try
            {
                var diaChi = _context.SoDiaChis.Where(s => s.Id == soDiaChiView.Id).FirstOrDefault();
                if (diaChi == null)
                {
                    return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                }
                if (diaChi.UserId != soDiaChiView.UserId)
                {
                    return new ResponsePostView(MessageConstants.VALIDATE_FAILED, 400);
                }
                _context.SoDiaChis.Remove(diaChi);
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.DELETE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.DELETE_FAILED, 400);
            }
        }
    }
}
