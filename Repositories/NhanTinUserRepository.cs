﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Project.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project.Repositories
{
    public interface INhanTinUserRepository
    {
        public ResponseWithPaginationView GetAllNhanTinUser(NhanTinUserParamView nhanTinParamView);
        public ResponseIdPostView CreateNhanTinUser(NhanTinUserView nhanTinUserView);
        public ResponsePostView UpdateNhanTinUser(List<NhanTinUserView> nhanTinUserView);
        public ResponseWithPaginationView GetAllUserNhanId(NhanTinUserParamView nhanTinParamView);
        public ResponseNoiDungPostView UploadFile(NhanTinUserView nhanTinUserView, IFormFileCollection files);
    }

    public class NhanTinUserRepository : INhanTinUserRepository
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public NhanTinUserRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ResponseIdPostView CreateNhanTinUser(NhanTinUserView nhanTinUserView)
        {
            try
            {
                var nhanTin = _context.NhanTinUsers.Where(s => (s.UserGuiId == nhanTinUserView.UserGuiId && s.UserNhanId == nhanTinUserView.UserNhanId) ||
                                                               (s.UserGuiId == nhanTinUserView.UserNhanId && s.UserNhanId == nhanTinUserView.UserGuiId)).FirstOrDefault();
                var chatBoxId = Guid.NewGuid();
                if (nhanTin != null)
                {
                    chatBoxId = nhanTin.ChatBoxId;
                }
                var newNhanTin = new NhanTinUser();
                newNhanTin = _mapper.Map<NhanTinUser>(nhanTinUserView);
                newNhanTin.CreatedAt = DateTime.Now;
                newNhanTin.UpdatedAt = DateTime.Now;
                newNhanTin.ChatBoxId = chatBoxId;
                _context.NhanTinUsers.Add(newNhanTin);
                _context.SaveChanges();
                return new ResponseIdPostView(MessageConstants.CREATE_SUCCESS, 200, newNhanTin.Id);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponseIdPostView(MessageConstants.CREATE_FAILED, 400, null);
            }
        }

        public ResponseWithPaginationView GetAllNhanTinUser(NhanTinUserParamView nhanTinParamView)
        {
            try
            {
                var nhanTins =
                    from s in _context.NhanTinUsers
                    where (s.UserGuiId == nhanTinParamView.UserGuiId &&
                        s.UserNhanId == nhanTinParamView.UserNhanId) ||
                        (s.UserNhanId == nhanTinParamView.UserGuiId &&
                        s.UserGuiId == nhanTinParamView.UserNhanId)
                    orderby s.CreatedAt descending
                    select new NhanTinUserView
                    {
                        CreatedAt = s.CreatedAt,
                        UserGuiId = s.UserGuiId,
                        UserNhanId = s.UserNhanId,
                        Id = s.Id,
                        NoiDung = s.NoiDung,
                        DaXem = s.DaXem,
                        FileName = s.FileName,
                        IsFile = s.IsFile,
                        IsImage = s.IsImage
                    };
                if (!nhanTins.Any())
                {
                    return new NhanTinUserListView(null, 0, 404);
                }
                if (nhanTinParamView.DaXem != null)
                {
                    nhanTins = nhanTins.Where(s => s.DaXem == nhanTinParamView.DaXem);
                }
                var totalRecord = nhanTins.ToList().Count();
                if (nhanTinParamView.PageIndex > 0)
                {
                    nhanTins = nhanTins.Skip(nhanTinParamView.PageSize * (nhanTinParamView.PageIndex - 1)).Take(nhanTinParamView.PageSize);
                }
                var listNhanTin = nhanTins.ToList();
                return new NhanTinUserListView(listNhanTin, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new NhanTinUserListView(null, 0, 400);
            }
        }

        public ResponseWithPaginationView GetAllUserNhanId(NhanTinUserParamView nhanTinParamView)
        {
            try
            {
                var nhanTins =
                    from s in _context.NhanTinUsers
                    where s.UserGuiId == nhanTinParamView.UserGuiId ||
                        s.UserNhanId == nhanTinParamView.UserGuiId
                    orderby s.CreatedAt descending
                    select new NhanTinUserView
                    {
                        UserGuiId = s.UserGuiId,
                        UserNhanId = s.UserNhanId
                    };
                if (!nhanTins.Any())
                {
                    return new NhanTinUserListView(null, 0, 404);
                }
                var groupNhanTins = nhanTins.ToList().GroupBy(s => new
                {
                    s.UserGuiId,
                    s.UserNhanId
                }).Select(g => new NhanTinUserView
                {
                    UserGuiId = g.Key.UserGuiId,
                    UserNhanId = g.Key.UserNhanId
                }).ToList();
                var tmp = nhanTinParamView.UserGuiId;
                if(tmp != null)
                {
                    foreach (var obj in groupNhanTins)
                    {
                        if (obj.UserNhanId == nhanTinParamView.UserGuiId)
                        {
                            obj.UserNhanId = obj.UserGuiId;
                            obj.UserGuiId = tmp.Value;
                        }
                    }
                    groupNhanTins = groupNhanTins.GroupBy(s => new
                    {
                        s.UserGuiId,
                        s.UserNhanId
                    }).Select(g => new NhanTinUserView
                    {
                        UserGuiId = g.Key.UserGuiId,
                        UserNhanId = g.Key.UserNhanId
                    }).ToList();
                }
                var totalRecord = groupNhanTins.Count();
                if (nhanTinParamView.PageIndex > 0)
                {
                    groupNhanTins = groupNhanTins.Skip(nhanTinParamView.PageSize * (nhanTinParamView.PageIndex - 1)).Take(nhanTinParamView.PageSize).ToList();
                }
                var listNhanTin = groupNhanTins;
                return new NhanTinUserListView(listNhanTin, totalRecord, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new NhanTinUserListView(null, 0, 400);
            }
        }

        public ResponsePostView UpdateNhanTinUser(List<NhanTinUserView> nhanTinUserView)
        {
            try
            {
                foreach (var item in nhanTinUserView)
                {
                    var nhanTin = _context.NhanTinUsers.Where(s => s.Id == item.Id).FirstOrDefault();
                    if (nhanTin == null)
                    {
                        return new ResponsePostView(MessageConstants.NOT_FOUND, 404);
                    }
                    nhanTin.DaXem = item.DaXem;
                    nhanTin.UpdatedAt = DateTime.Now;
                }
                _context.SaveChanges();
                return new ResponsePostView(MessageConstants.UPDATE_SUCCESS, 200);
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponsePostView(MessageConstants.UPDATE_FAILED, 400);
            }
        }

        public ResponseNoiDungPostView UploadFile(NhanTinUserView nhanTinUserView, IFormFileCollection files)
        {
            try
            {
                if (files.Count > 0)
                {
                    var file = files[0];
                    if (file.Length > 0)
                    {
                        var nhanTin = _context.NhanTinUsers.Where(s => (s.UserGuiId == nhanTinUserView.UserGuiId && s.UserNhanId == nhanTinUserView.UserNhanId) ||
                                                                       (s.UserGuiId == nhanTinUserView.UserNhanId && s.UserNhanId == nhanTinUserView.UserGuiId)).FirstOrDefault();
                        var chatBoxId = Guid.NewGuid();
                        if (nhanTin != null)
                        {
                            chatBoxId = nhanTin.ChatBoxId;
                        }
                        var folderName = Path.Combine("Statics", "Files", "Chats", chatBoxId.ToString());
                        var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                        Directory.CreateDirectory(pathToSave);
                        var fileName = file.FileName;
                        var name = "";
                        for (int i = 0; i < fileName.Split('.').Count(); i++)
                        {
                            if (i < fileName.Split('.').Count() - 1)
                            {
                                name += fileName.Split('.')[i];
                            }
                        }
                        var fileRename = name + Guid.NewGuid().ToString() + "." + fileName.Split(".").Last();
                        var fullPath = Path.Combine(pathToSave, fileRename);
                        var dbPath = Path.Combine(folderName, fileRename);
                        var newNhanTin = new NhanTinUser();
                        newNhanTin = _mapper.Map<NhanTinUser>(nhanTinUserView);
                        newNhanTin.CreatedAt = DateTime.Now;
                        newNhanTin.UpdatedAt = DateTime.Now;
                        newNhanTin.ChatBoxId = chatBoxId;
                        newNhanTin.NoiDung = dbPath;
                        newNhanTin.FileName = fileName;
                        _context.NhanTinUsers.Add(newNhanTin);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        _context.SaveChanges();
                        return new ResponseNoiDungPostView(MessageConstants.CREATE_SUCCESS, 200, dbPath, newNhanTin.Id);
                    }
                    else
                    {
                        return new ResponseNoiDungPostView(MessageConstants.CREATE_FAILED, 400, null, null);
                    }
                }
                else
                {
                    return new ResponseNoiDungPostView(MessageConstants.CREATE_FAILED, 400, null, null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return new ResponseNoiDungPostView(MessageConstants.CREATE_FAILED, 400, null, null);
            }
        }
    }
}
