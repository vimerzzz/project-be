﻿using System;

namespace Project.Utilities.Converters
{
    public class IntegerToGuid
    {
        public Guid IntToGuid(int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }

        public IntegerToGuid()
        {

        }
    }
}
