﻿using System;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Project.Utilities.Verifications
{
    public class SendVerificationEmail
    {
        public void SendMail(string fromAdress, string toAddress, string token, string smtpPassword, string urlClient)
        {
            var uriBuilder = new UriBuilder(urlClient);
            var values = HttpUtility.ParseQueryString(string.Empty);
            values["token"] = token;
            values["email"] = toAddress;
            uriBuilder.Query = values.ToString();

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(fromAdress);
            mail.To.Add(new MailAddress(toAddress));
            mail.Body = "<a href=\"" + uriBuilder + "\">click here to verify</a>";
            mail.IsBodyHtml = true;
            mail.Subject = "Verification";

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(fromAdress, smtpPassword);
            smtp.Send(mail);
        }

        public SendVerificationEmail()
        {

        }
    }
}
