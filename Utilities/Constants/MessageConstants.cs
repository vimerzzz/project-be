﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Utilities.Constants
{
    public class MessageConstants
    {
        public static string SYSTEM_ERROR = "Lỗi hệ thống!";
        public static string DONE = "OK!";
        public static string CREATE_SUCCESS = "Thêm mới thành công!";
        public static string CREATE_FAILED = "Thêm mới không thành công!";
        public static string UPDATE_SUCCESS = "Cập nhật thành công!";
        public static string UPDATE_FAILED = "Cập nhật không thành công!";
        public static string DELETE_SUCCESS = "Xóa thành công!";
        public static string DELETE_FAILED = "Xóa không thành công!";
        public static string NOT_FOUND = "Không tìm thấy";
        public static string USER_NOT_FOUND = "Không tìm thấy người dùng";
        public static string FILE_NOT_FOUND = "Không tìm thấy file";
        public static string SHOP_NOT_FOUND = "Không tìm thấy shop";
        public static string FOUND = "Tìm thấy";
        public static string LOGIN_FAILED = "Đăng nhập thất bại";
        public static string LOGIN_SUCCESS = "Đăng nhập thành công";
        public static string LOGOUT_FAILED = "Đăng xuất thất bại";
        public static string LOGOUT_SUCCESS = "Đăng xuất thành công";
        public static string WRONG_PASSWORD = "Mật khẩu không khớp";
        public static string ACCOUNT_WAS_BANNED = "Tài khoản đã bị ban";
        public static string VALIDATE_FAILED = "Xác thực thất bại";
        public static string INSUFFICIENT = "Không đủ số lượng";
    }
}
