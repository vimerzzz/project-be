﻿namespace Project.Utilities.Constants
{
    public class TokenProviderConstants
    {
        public static string IssuerName = "https://localhost:44303";
        public static string LoginProvider = "Project";
        public static string LoginTokenName = "LoginToken";
    }
}
