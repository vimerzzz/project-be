﻿using AutoMapper;
using Project.Models;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project
{
    public class ProjectMapperProfile : Profile
    {
        public ProjectMapperProfile()
        {
            CreateMap<UserView, User>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.HoatDong, option => option.Ignore())
                .ForMember(destination => destination.UserInfo, option => option.Ignore());
            CreateMap<RoleView, Role>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<UserInfoView, UserInfo>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<UserView, UserInfo>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.Id, option => option.Ignore())
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.Id));
            CreateMap<SoDiaChiView, SoDiaChi>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<LoaiSanPhamView, LoaiSanPham>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<ThongBaoView, ThongBao>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<ShopView, Shop>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.HoatDong, option => option.Ignore());
            CreateMap<SanPhamView, SanPham>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<SanPhamDauGiaView, SanPhamDauGia>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.UserId, option => option.Ignore())
                .ForMember(destination => destination.TinhTrangDauGiaId, option => option.Ignore())
                .ForMember(destination => destination.User, option => option.Ignore());
            CreateMap<HinhAnhSanPhamView, HinhAnhSanPham>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<HinhAnhSanPhamDauGiaView, HinhAnhSanPhamDauGia>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<SoTheoDoiView, SoTheoDoi>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.Shop, option => option.Ignore());
            CreateMap<DanhGiaShopView, DanhGiaShop>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.User, option => option.Ignore());
            CreateMap<GioHangView, GioHang>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.SanPham, option => option.Ignore())
                .ForMember(destination => destination.SanPhamDauGia, option => option.Ignore());
            CreateMap<DanhGiaSanPhamView, DanhGiaSanPham>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.User, option => option.Ignore());
            CreateMap<NhanTinShopView, NhanTinShop>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<NhanTinUserView, NhanTinUser>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<DonHangView, DonHang>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<DonHangSanPhamView, DonHangSanPham>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<DanhGiaSanPhamHuuIchView, DanhGiaSanPhamHuuIch>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<DanhGiaShopHuuIchView, DanhGiaShopHuuIch>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore());
            CreateMap<TroChuyenDauGiaView, TroChuyenDauGia>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.User, option => option.Ignore());
            CreateMap<BaoCaoViPhamView, BaoCaoViPham>()
                .ForMember(destination => destination.CreatedAt, option => option.Ignore())
                .ForMember(destination => destination.UpdatedAt, option => option.Ignore())
                .ForMember(destination => destination.User, option => option.Ignore())
                .ForMember(destination => destination.Shop, option => option.Ignore());
        }
    }
}
