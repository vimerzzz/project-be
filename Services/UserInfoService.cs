﻿using Project.Repositories;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface IUserInfoService
    {
        public UserInfoView GetUserInfoByUserId(Guid userId);
        public ResponsePostView UpdateUserInfo(UserInfoView userInfoView);
    }

    public class UserInfoService : IUserInfoService
    {
        private readonly IUserInfoRepository _repo;

        public UserInfoService(IUserInfoRepository repo)
        {
            _repo = repo;
        }

        public UserInfoView GetUserInfoByUserId(Guid userId)
        {
            var res = _repo.GetUserInfoByUserId(userId);
            return res;
        }
        public ResponsePostView UpdateUserInfo(UserInfoView userInfoView)
        {
            var res = _repo.UpdateUserInfo(userInfoView);
            return res;
        }
    }
}
