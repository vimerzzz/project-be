﻿using Project.Repositories;
using Project.ViewModels;
using System;
using System.Collections.Generic;

namespace Project.Services
{
    public interface IHinhAnhSanPhamDauGiaService
    {
        public List<HinhAnhSanPhamDauGiaView> GetAllHinhAnhSanPhamDauGiaBySanPhamId(Guid sanPhamDauGiaId);
        public ResponsePostView CreateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews);
        public ResponsePostView UpdateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews);
        public ResponsePostView DeleteHinhAnhSanPhamDauGia(List<Guid> hinhAnhSanPhamDauGiaId, Guid userId);
    }

    public class HinhAnhSanPhamDauGiaService : IHinhAnhSanPhamDauGiaService
    {
        private readonly IHinhAnhSanPhamDauGiaRepository _repo;

        public HinhAnhSanPhamDauGiaService(IHinhAnhSanPhamDauGiaRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews)
        {
            var res = _repo.CreateHinhAnhSanPhamDauGia(hinhAnhSanPhamDauGiaViews);
            return res;
        }

        public ResponsePostView DeleteHinhAnhSanPhamDauGia(List<Guid> hinhAnhSanPhamDauGiaId, Guid userId)
        {
            var res = _repo.DeleteHinhAnhSanPhamDauGia(hinhAnhSanPhamDauGiaId, userId);
            return res;
        }

        public List<HinhAnhSanPhamDauGiaView> GetAllHinhAnhSanPhamDauGiaBySanPhamId(Guid sanPhamDauGiaId)
        {
            var res = _repo.GetAllHinhAnhSanPhamDauGiaBySanPhamId(sanPhamDauGiaId);
            return res;
        }

        public ResponsePostView UpdateHinhAnhSanPhamDauGia(List<HinhAnhSanPhamDauGiaView> hinhAnhSanPhamDauGiaViews)
        {
            var res = _repo.UpdateHinhAnhSanPhamDauGia(hinhAnhSanPhamDauGiaViews);
            return res;
        }
    }
}
