﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface INhanTinUserService
    {
        public ResponseWithPaginationView GetAllNhanTinUser(NhanTinUserParamView nhanTinParamView);
        public Task<ResponsePostView> CreateNhanTinUser(NhanTinUserView nhanTinUserView);
        public ResponseWithPaginationView GetAllUserNhanId(NhanTinUserParamView nhanTinParamView);
        public Task<ResponsePostView> UpdateNhanTinUser(List<NhanTinUserView> nhanTinUserView);
        public Task<ResponseNoiDungPostView> UploadFile(NhanTinUserView nhanTinUserView, IFormFileCollection files);
    }

    public class NhanTinUserService : INhanTinUserService
    {
        private readonly INhanTinUserRepository _repo;
        private readonly IHubContext<ChatHub> _hub;

        public NhanTinUserService(INhanTinUserRepository repo, IHubContext<ChatHub> hub)
        {
            _repo = repo;
            _hub = hub;
        }

        public async Task<ResponsePostView> CreateNhanTinUser(NhanTinUserView nhanTinUserView)
        {
            var res = _repo.CreateNhanTinUser(nhanTinUserView);
            if (res.StatusCode == 200)
            {
                nhanTinUserView.Id = res.Id != null ? res.Id.Value : default(Guid);
                await _hub.Clients.All.SendAsync("chatUser", nhanTinUserView);
            }
            return res;
        }

        public ResponseWithPaginationView GetAllNhanTinUser(NhanTinUserParamView nhanTinParamView)
        {
            var res = _repo.GetAllNhanTinUser(nhanTinParamView);
            return res;
        }

        public ResponseWithPaginationView GetAllUserNhanId(NhanTinUserParamView nhanTinParamView)
        {
            var res = _repo.GetAllUserNhanId(nhanTinParamView);
            return res;
        }

        public async Task<ResponsePostView> UpdateNhanTinUser(List<NhanTinUserView> nhanTinUserView)
        {
            var res = _repo.UpdateNhanTinUser(nhanTinUserView);
            if (res.StatusCode == 200)
            {
                await _hub.Clients.All.SendAsync("chatUser", nhanTinUserView[0]);
            }
            return res;
        }

        public async Task<ResponseNoiDungPostView> UploadFile(NhanTinUserView nhanTinUserView, IFormFileCollection files)
        {
            var res = _repo.UploadFile(nhanTinUserView, files);
            if (res.StatusCode == 200)
            {
                nhanTinUserView.Id = res.Id != null ? res.Id.Value : default(Guid);
                nhanTinUserView.NoiDung = res.NoiDung != null ? res.NoiDung : "";
                await _hub.Clients.All.SendAsync("chatUser", nhanTinUserView);
            }
            return res;
        }
    }
}
