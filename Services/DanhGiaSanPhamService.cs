﻿using Project.Repositories;
using Project.ViewModels;
using System;
using System.Collections.Generic;

namespace Project.Services
{
    public interface IDanhGiaSanPhamService
    {
        public ResponseWithPaginationView GetAllDanhGia(DanhGiaSanPhamParamView danhGiaSanPhamParamView);
        public ResponsePostView CreateDanhGia(DanhGiaSanPhamView danhGiaSanPhamView);
        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView);
        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView);
        public List<DanhGiaSanPhamHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId);
    }

    public class DanhGiaSanPhamService : IDanhGiaSanPhamService
    {
        private readonly IDanhGiaSanPhamRepository _repo;

        public DanhGiaSanPhamService(IDanhGiaSanPhamRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateDanhGia(DanhGiaSanPhamView danhGiaSanPhamView)
        {
            var res = _repo.CreateDanhGia(danhGiaSanPhamView);
            return res;
        }

        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView)
        {
            var res = _repo.CreateDanhGiaHuuIch(danhGiaSanPhamHuuIchView);
            return res;
        }

        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaSanPhamHuuIchView danhGiaSanPhamHuuIchView)
        {
            var res = _repo.DeleteDanhGiaHuuIch(danhGiaSanPhamHuuIchView);
            return res;
        }

        public ResponseWithPaginationView GetAllDanhGia(DanhGiaSanPhamParamView danhGiaSanPhamParamView)
        {
            var res = _repo.GetAllDanhGia(danhGiaSanPhamParamView);
            return res;
        }

        public List<DanhGiaSanPhamHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId)
        {
            var res = _repo.GetDanhGiaHuuIchByUserId(userId);
            return res;
        }
    }
}
