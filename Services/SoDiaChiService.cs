﻿using Project.Repositories;
using Project.ViewModels;
using System;

namespace Project.Services
{
    public interface ISoDiaChiService
    {
        public ResponseWithPaginationView GetAllDiaChi(SoDiaChiParamView soDiaChiParamView);
        public ResponsePostView CreateDiaChi(SoDiaChiView soDiaChiView);
        public ResponsePostView UpdateDiaChi(SoDiaChiView soDiaChiView);
        public ResponsePostView DeleteDiaChi(SoDiaChiView soDiaChiView);
    }

    public class SoDiaChiService : ISoDiaChiService
    {
        private readonly ISoDiaChiRepository _repo;

        public SoDiaChiService(ISoDiaChiRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateDiaChi(SoDiaChiView soDiaChiView)
        {
            var res = _repo.CreateDiaChi(soDiaChiView);
            return res;
        }

        public ResponsePostView DeleteDiaChi(SoDiaChiView soDiaChiView)
        {
            var res = _repo.DeleteDiaChi(soDiaChiView);
            return res;
        }

        public ResponseWithPaginationView GetAllDiaChi(SoDiaChiParamView soDiaChiParamView)
        {
            var res = _repo.GetAllDiaChi(soDiaChiParamView);
            return res;
        }

        public ResponsePostView UpdateDiaChi(SoDiaChiView soDiaChiView)
        {
            var res = _repo.UpdateDiaChi(soDiaChiView);
            return res;
        }
    }
}
