﻿using Project.Repositories;
using Project.ViewModels;
using System.Collections.Generic;

namespace Project.Services
{
    public interface IGioHangService
    {
        public List<GioHangWithListSanPhamView> GetAllGioHangDauGia(GioHangView gioHangView);
        public List<GioHangWithListSanPhamView> GetAllGioHang(GioHangView gioHangView);
        public ResponsePostView CreateGioHang(GioHangView gioHangView);
        public ResponsePostView UpdateGioHang(GioHangView gioHangView);
        public ResponsePostView DeleteGioHang(GioHangView gioHangView);
    }

    public class GioHangService : IGioHangService
    {
        private readonly IGioHangRepository _repo;

        public GioHangService(IGioHangRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateGioHang(GioHangView gioHangView)
        {
            var res = _repo.CreateGioHang(gioHangView);
            return res;
        }

        public ResponsePostView DeleteGioHang(GioHangView gioHangView)
        {
            var res = _repo.DeleteGioHang(gioHangView);
            return res;
        }

        public List<GioHangWithListSanPhamView> GetAllGioHangDauGia(GioHangView gioHangView)
        {
            var res = _repo.GetAllGioHangDauGia(gioHangView);
            return res;
        }

        public List<GioHangWithListSanPhamView> GetAllGioHang(GioHangView gioHangView)
        {
            var res = _repo.GetAllGioHang(gioHangView);
            return res;
        }

        public ResponsePostView UpdateGioHang(GioHangView gioHangView)
        {
            var res = _repo.UpdateGioHang(gioHangView);
            return res;
        }
    }
}
