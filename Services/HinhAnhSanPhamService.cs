﻿using Project.Repositories;
using Project.ViewModels;
using System;
using System.Collections.Generic;

namespace Project.Services
{
    public interface IHinhAnhSanPhamService
    {
        public List<HinhAnhSanPhamView> GetAllHinhAnhSanPhamBySanPhamId(Guid sanPhamId);
        public ResponsePostView CreateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews);
        public ResponsePostView UpdateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews);
        public ResponsePostView DeleteHinhAnhSanPham(List<Guid> hinhAnhSanPhamId, Guid userId);
    }
    public class HinhAnhSanPhamService : IHinhAnhSanPhamService
    {
        private readonly IHinhAnhSanPhamRepository _repo;

        public HinhAnhSanPhamService(IHinhAnhSanPhamRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews)
        {
            var res = _repo.CreateHinhAnhSanPham(hinhAnhSanPhamViews);
            return res;
        }

        public ResponsePostView DeleteHinhAnhSanPham(List<Guid> hinhAnhSanPhamId, Guid userId)
        {
            var res = _repo.DeleteHinhAnhSanPham(hinhAnhSanPhamId, userId);
            return res;
        }

        public List<HinhAnhSanPhamView> GetAllHinhAnhSanPhamBySanPhamId(Guid sanPhamId)
        {
            var res = _repo.GetAllHinhAnhSanPhamBySanPhamId(sanPhamId);
            return res;
        }

        public ResponsePostView UpdateHinhAnhSanPham(List<HinhAnhSanPhamView> hinhAnhSanPhamViews)
        {
            var res = _repo.UpdateHinhAnhSanPham(hinhAnhSanPhamViews);
            return res;
        }
    }
}
