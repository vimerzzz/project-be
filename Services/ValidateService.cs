﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Project.DataContext;
using Project.Models;
using Project.Utilities.Constants;
using Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface IValidateService
    {
        public Task<bool> ValidateBearerToken(Guid userId, string token);
    }

    public class ValidateService : IValidateService
    {
        private readonly UserManager<User> _userManager;
        private readonly ApplicationDBContext _context;

        public ValidateService(UserManager<User> userManager, ApplicationDBContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<bool> ValidateBearerToken(Guid userId, string token)
        {
            try
            {
                var user = _context.Users.Where(s => s.Id == userId).FirstOrDefault();
                if (user != null)
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var storedToken = await _userManager.GetAuthenticationTokenAsync(user, TokenProviderConstants.LoginProvider, TokenProviderConstants.LoginTokenName);
                    if (storedToken != null)
                    {
                        var jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token.Split("Bearer ")[1]);
                        var jwtStoredToken = (JwtSecurityToken)tokenHandler.ReadToken(storedToken);
                        return jwtToken.Audiences.FirstOrDefault() == jwtStoredToken.Audiences.FirstOrDefault();
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(MessageConstants.SYSTEM_ERROR + "{@error}", ex);
                return false;
            }
        }
    }
}
