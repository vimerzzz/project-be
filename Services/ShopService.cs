﻿using Project.Repositories;
using Project.ViewModels;
using System;
using System.Collections.Generic;

namespace Project.Services
{
    public interface IShopService
    {
        public ResponseWithPaginationView GetAllShop(ShopParamView shopParamView);
        public ShopView GetShopByUserId(Guid userId);
        public ShopView GetShopByShopName(string shopName);
        public ShopView GetShopByShopId(Guid shopId);
        public ResponsePostView CreateShop(ShopView shopView);
        public ResponsePostView UpdateShop(ShopView shopView);
        public ResponsePostView UpdateStatusShop(ShopView shopView);
    }
    public class ShopService : IShopService
    {
        private readonly IShopRepository _repo;

        public ShopService(IShopRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateShop(ShopView shopView)
        {
            var res = _repo.CreateShop(shopView);
            return res;
        }

        public ResponseWithPaginationView GetAllShop(ShopParamView shopParamView)
        {
            var res = _repo.GetAllShop(shopParamView);
            return res;
        }

        public ShopView GetShopByShopName(string shopName)
        {
            var res = _repo.GetShopByShopName(shopName);
            return res;
        }

        public ShopView GetShopByShopId(Guid shopId)
        {
            var res = _repo.GetShopByShopId(shopId);
            return res;
        }

        public ShopView GetShopByUserId(Guid userId)
        {
            var res = _repo.GetShopByUserId(userId);
            return res;
        }

        public ResponsePostView UpdateShop(ShopView shopView)
        {
            var res = _repo.UpdateShop(shopView);
            return res;
        }

        public ResponsePostView UpdateStatusShop(ShopView shopView)
        {
            var res = _repo.UpdateStatusShop(shopView);
            return res;
        }
    }
}
