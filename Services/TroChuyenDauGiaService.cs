﻿using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface ITroChuyenDauGiaService
    {
        public ResponseWithPaginationView GetAllTroChuyenDauGia(TroChuyenDauGiaParamView troChuyenDauGiaParamView);
        public Task<ResponsePostView> CreateTroChuyenDauGia(TroChuyenDauGiaView troChuyenDauGiaView);
    }

    public class TroChuyenDauGiaService : ITroChuyenDauGiaService
    {
        private readonly ITroChuyenDauGiaRepository _repo;
        private readonly IHubContext<SanPhamDauGiaHub> _hub;

        public TroChuyenDauGiaService(ITroChuyenDauGiaRepository repo, IHubContext<SanPhamDauGiaHub> hub)
        {
            _repo = repo;
            _hub = hub;
        }

        public async Task<ResponsePostView> CreateTroChuyenDauGia(TroChuyenDauGiaView troChuyenDauGiaView)
        {
            var res = _repo.CreateTroChuyenDauGia(ref troChuyenDauGiaView);
            if (res.StatusCode == 200)
            {
                await _hub.Clients.All.SendAsync("dauGiaChat", troChuyenDauGiaView);
            }
            return res;
        }

        public ResponseWithPaginationView GetAllTroChuyenDauGia(TroChuyenDauGiaParamView troChuyenDauGiaParamView)
        {
            var res = _repo.GetAllTroChuyenDauGia(troChuyenDauGiaParamView);
            return res;
        }
    }
}
