﻿using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface IBaoCaoViPhamService
    {
        public ResponseWithPaginationView GetAllBaoCao(BaoCaoViPhamParamView baoCaoParamView);
        public List<BaoCaoViPhamView> GetAllNguoiBaoCao();
        public List<BaoCaoViPhamView> GetAllNguoiBiBaoCao();
        public List<BaoCaoViPhamView> GetAllShopBiBaoCao();
        public Task<ResponsePostView> CreateBaoCao(BaoCaoViPhamView baoCaoView);
        public Task<ResponsePostView> UpdateBaoCao(BaoCaoViPhamView baoCaoView);
        public Task<ResponsePostView> DeleteBaoCao(BaoCaoViPhamView baoCaoView);
    }

    public class BaoCaoViPhamService : IBaoCaoViPhamService
    {
        private readonly IBaoCaoViPhamRepository _repo;
        private readonly IHubContext<BaoCaoHub> _hub;

        public BaoCaoViPhamService(IBaoCaoViPhamRepository repo, IHubContext<BaoCaoHub> hub)
        {
            _repo = repo;
            _hub = hub;
        }

        public async Task<ResponsePostView> CreateBaoCao(BaoCaoViPhamView baoCaoView)
        {
            var res = _repo.CreateBaoCao(ref baoCaoView);
            if(res.StatusCode == 200)
            {
                var param = new BaoCaoViPhamParamView
                {
                    DaXem = false,
                    PageIndex = 0,
                    PageSize = 0
                };
                var listBaoCao = _repo.GetAllBaoCao(param);
                await _hub.Clients.All.SendAsync("baoCaoChuaXem", listBaoCao);
                await _hub.Clients.All.SendAsync("baoCao", baoCaoView);
            }
            return res;
        }

        public async Task<ResponsePostView> DeleteBaoCao(BaoCaoViPhamView baoCaoView)
        {
            var res = _repo.DeleteBaoCao(baoCaoView);
            if (res.StatusCode == 200)
            {
                var param = new BaoCaoViPhamParamView
                {
                    DaXem = false,
                    PageIndex = 0,
                    PageSize = 0
                };
                var listBaoCao = _repo.GetAllBaoCao(param);
                await _hub.Clients.All.SendAsync("baoCaoChuaXem", listBaoCao);
            }
            return res;
        }

        public ResponseWithPaginationView GetAllBaoCao(BaoCaoViPhamParamView baoCaoParamView)
        {
            var res = _repo.GetAllBaoCao(baoCaoParamView);
            return res;
        }

        public List<BaoCaoViPhamView> GetAllNguoiBaoCao()
        {
            var res = _repo.GetAllNguoiBaoCao();
            return res;
        }

        public List<BaoCaoViPhamView> GetAllNguoiBiBaoCao()
        {
            var res = _repo.GetAllNguoiBiBaoCao();
            return res;
        }

        public List<BaoCaoViPhamView> GetAllShopBiBaoCao()
        {
            var res = _repo.GetAllShopBiBaoCao();
            return res;
        }

        public async Task<ResponsePostView> UpdateBaoCao(BaoCaoViPhamView baoCaoView)
        {
            var res = _repo.UpdateBaoCao(baoCaoView);
            if (res.StatusCode == 200)
            {
                var param = new BaoCaoViPhamParamView
                {
                    DaXem = false,
                    PageIndex = 0,
                    PageSize = 0
                };
                var listBaoCao = _repo.GetAllBaoCao(param);
                await _hub.Clients.All.SendAsync("baoCaoChuaXem", listBaoCao);
            }
            return res;
        }
    }
}
