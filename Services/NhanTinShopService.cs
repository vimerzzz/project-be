﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface INhanTinShopService
    {
        public ResponseWithPaginationView GetAllNhanTinShop(NhanTinShopParamView nhanTinParamView);
        public Task<ResponsePostView> CreateNhanTinShop(NhanTinShopView nhanTinShopView);
        public ResponseWithPaginationView GetAllShopId(NhanTinShopParamView nhanTinParamView);
        public ResponseWithPaginationView GetAllUserId(NhanTinShopParamView nhanTinParamView);
        public Task<ResponsePostView> UpdateNhanTinShop(List<NhanTinShopView> nhanTinShopView);
        public Task<ResponseNoiDungPostView> UploadFile(NhanTinShopView nhanTinShopView, IFormFileCollection files);
    }

    public class NhanTinShopService : INhanTinShopService
    {
        private readonly INhanTinShopRepository _repo;
        private readonly IHubContext<ChatHub> _hub;

        public NhanTinShopService(INhanTinShopRepository repo, IHubContext<ChatHub> hub)
        {
            _repo = repo;
            _hub = hub;
        }

        public async Task<ResponsePostView> CreateNhanTinShop(NhanTinShopView nhanTinShopView)
        {
            var res = _repo.CreateNhanTinShop(nhanTinShopView);
            if(res.StatusCode == 200)
            {
                nhanTinShopView.Id = res.Id != null ? res.Id.Value : default(Guid);
                await _hub.Clients.All.SendAsync("chatShop", nhanTinShopView);
            }
            return res;
        }

        public ResponseWithPaginationView GetAllNhanTinShop(NhanTinShopParamView nhanTinParamView)
        {
            var res = _repo.GetAllNhanTinShop(nhanTinParamView);
            return res;
        }

        public ResponseWithPaginationView GetAllShopId(NhanTinShopParamView nhanTinParamView)
        {
            var res = _repo.GetAllShopId(nhanTinParamView);
            return res;
        }

        public ResponseWithPaginationView GetAllUserId(NhanTinShopParamView nhanTinParamView)
        {
            var res = _repo.GetAllUserId(nhanTinParamView);
            return res;
        }

        public async Task<ResponsePostView> UpdateNhanTinShop(List<NhanTinShopView> nhanTinShopView)
        {
            var res = _repo.UpdateNhanTinShop(nhanTinShopView);
            if (res.StatusCode == 200)
            {
                await _hub.Clients.All.SendAsync("chatShop", nhanTinShopView[0]);
            }
            return res;
        }

        public async Task<ResponseNoiDungPostView> UploadFile(NhanTinShopView nhanTinShopView, IFormFileCollection files)
        {
            var res = _repo.UploadFile(nhanTinShopView, files);
            if (res.StatusCode == 200)
            {
                nhanTinShopView.Id = res.Id != null ? res.Id.Value : default(Guid);
                nhanTinShopView.NoiDung = res.NoiDung != null ? res.NoiDung : "";
                await _hub.Clients.All.SendAsync("chatShop", nhanTinShopView);
            }
            return res;
        }
    }
}
