﻿using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface IThongBaoService
    {
        public ResponseWithPaginationView GetAllThongBao(ThongBaoParamView thongBaoParamView);
        public ResponseWithPaginationView GetAllThongBaoCoLichHen(ThongBaoParamView thongBaoParamView);
        public ResponsePostView CreateThongBao(List<ThongBaoView> thongBaoView);
        public ResponsePostView UpdateThongBao(List<ThongBaoView> thongBaoView);
        public ResponsePostView DeleteThongBao(List<Guid> id);
        public Task CheckStatusThongBao();
    }

    public class ThongBaoService : IThongBaoService
    {
        private readonly IThongBaoRepository _repo;
        private readonly IHubContext<ThongBaoHub> _hub;

        public ThongBaoService(IThongBaoRepository repo, IHubContext<ThongBaoHub> hub)
        {
            _repo = repo;
            _hub = hub;
        }

        public ResponsePostView CreateThongBao(List<ThongBaoView> thongBaoView)
        {
            var res = _repo.CreateThongBao(ref thongBaoView);
            return res;
        }

        public ResponsePostView DeleteThongBao(List<Guid> id)
        {
            var res = _repo.DeleteThongBao(id);
            return res;
        }

        public ResponseWithPaginationView GetAllThongBao(ThongBaoParamView thongBaoParamView)
        {
            var res = _repo.GetAllThongBao(thongBaoParamView);
            return res;
        }

        public ResponseWithPaginationView GetAllThongBaoCoLichHen(ThongBaoParamView thongBaoParamView)
        {
            var res = _repo.GetAllThongBaoCoLichHen(thongBaoParamView);
            return res;
        }

        public ResponsePostView UpdateThongBao(List<ThongBaoView> thongBaoView)
        {
            var res = _repo.UpdateThongBao(thongBaoView);
            return res;
        }

        public async Task CheckStatusThongBao()
        {
            List<ThongBaoView> listToUpdate = new List<ThongBaoView>();
            var listThongBao = _repo.GetAllThongBaoToCheckStatus();
            var timeNow = DateTime.Now;
            if (listThongBao != null)
            {
                foreach (var item in listThongBao)
                {
                    if(item.ThoiGianHen < timeNow && item.CoLichHen == true)
                    {
                        item.CoLichHen = false;
                        listToUpdate.Add(item);
                    }
                }
                if(listToUpdate.Count > 0)
                {
                    var res = _repo.UpdateThongBao(listToUpdate);
                    if (res.StatusCode == 200)
                    {
                        await _hub.Clients.All.SendAsync("thongBao", listToUpdate);
                    }
                }
            }
        }
    }
}
