﻿using Project.ViewModels;
using Project.Repositories;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface ILogService
    {
        public Task<ResponseLoginView> Login(UserView userView);
    }

    public class LogService : ILogService
    {
        private readonly IUserRepository _repo;

        public LogService(IUserRepository repo)
        {
            _repo = repo;
        }

        public Task<ResponseLoginView> Login(UserView userView)
        {
            var res = _repo.Login(userView);
            return res;
        }
    }
}
