﻿using Project.Repositories;
using Project.ViewModels;
using System;

namespace Project.Services
{
    public interface ILoaiSanPhamService
    {
        public ResponseWithPaginationView GetAllLoaiSanPham(LoaiSanPhamParamView loaiSanPhamParamView);
        public ResponsePostView CreateLoaiSanPham(LoaiSanPhamView loaiSanPhamView);
        public ResponsePostView UpdateLoaiSanPham(LoaiSanPhamView loaiSanPhamView);
        public ResponsePostView DeleteLoaiSanPham(Guid id);
    }

    public class LoaiSanPhamService : ILoaiSanPhamService
    {
        private readonly ILoaiSanPhamRepository _repo;

        public LoaiSanPhamService(ILoaiSanPhamRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateLoaiSanPham(LoaiSanPhamView loaiSanPhamView)
        {
            var res = _repo.CreateLoaiSanPham(loaiSanPhamView);
            return res;
        }

        public ResponsePostView DeleteLoaiSanPham(Guid id)
        {
            var res = _repo.DeleteLoaiSanPham(id);
            return res;
        }

        public ResponseWithPaginationView GetAllLoaiSanPham(LoaiSanPhamParamView loaiSanPhamParamView)
        {
            var res = _repo.GetAllLoaiSanPham(loaiSanPhamParamView);
            return res;
        }

        public ResponsePostView UpdateLoaiSanPham(LoaiSanPhamView loaiSanPhamView)
        {
            var res = _repo.UpdateLoaiSanPham(loaiSanPhamView);
            return res;
        }
    }
}
