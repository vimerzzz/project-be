﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.Converters;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface IUserService
    {
        public ResponseWithPaginationView GetAllUser(UserParamView userParamView);
        public UserView GetUserById(Guid id);
        public Task<ResponsePostView> CreateUser(UserView userView);
        public ResponsePostView UpdateUser(UserView userView);
        public ResponsePostView CheckExistedUserName(UserView userView);
        public ResponsePostView CheckExistedEmail(UserView userView);
        public ResponsePostView CheckPassword(UserView userView);
        public ResponsePostView SetStatusAndRoleForUser(UserView userView);
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository _repo;
        private readonly IUserInfoRepository _infoRepo;
        private readonly IThongBaoRepository _thongBaoRepo;
        private readonly IHubContext<ThongBaoHub> _hub;
        private readonly IntegerToGuid converter = new IntegerToGuid();

        public UserService(IUserRepository repo, IUserInfoRepository infoRepo, IThongBaoRepository thongBaoRepo, IHubContext<ThongBaoHub> hub)
        {
            _repo = repo;
            _infoRepo = infoRepo;
            _thongBaoRepo = thongBaoRepo;
            _hub = hub;
        }
        public ResponseWithPaginationView GetAllUser(UserParamView userParamView)
        {
            var res = _repo.GetAllUser(userParamView);
            return res;
        }
        public UserView GetUserById(Guid id)
        {
            var res = _repo.GetUserById(id);
            return res;
        }
        public async Task<ResponsePostView> CreateUser(UserView userView)
        {
            var pair = _repo.CreateUser(userView);
            if(pair.Value == converter.IntToGuid(0))
            {
                var res = pair.Key;
                return res;
            }
            else
            {
                var id = pair.Value;
                var user = GetUserById(id);
                var res = _infoRepo.CreateUserInfoByUserView(user);
                if (res.StatusCode == 200)
                {
                    var thongBao = new List<ThongBaoView>();
                    thongBao.Add(new ThongBaoView
                    {
                        TieuDeThongBao = "Cần xác thực tài khoản",
                        NoiDungThongBao = "Email của bạn chưa được xác thực, hãy đến phần <a name=\"notify-link\" href=\"/information/setting\">Cài đặt tài khoản</a> để xác thực email của bạn",
                        UserId = id
                    });
                    var ress = _thongBaoRepo.CreateThongBao(ref thongBao);
                    if(ress.StatusCode == 200)
                    {
                        await _hub.Clients.All.SendAsync("thongBao", thongBao);
                    }
                }
                return res;
            }
        }
        public ResponsePostView UpdateUser(UserView userView)
        {
            var res = _repo.UpdateUser(userView);
            return res;
        }
        public ResponsePostView CheckExistedUserName(UserView userView)
        {
            var res = _repo.CheckExistedUserName(userView);
            return res;
        }
        public ResponsePostView CheckExistedEmail(UserView userView)
        {
            var res = _repo.CheckExistedEmail(userView);
            return res;
        }
        public ResponsePostView CheckPassword(UserView userView)
        {
            var res = _repo.CheckPassword(userView);
            return res;
        }
        public ResponsePostView SetStatusAndRoleForUser(UserView userView)
        {
            var res = _repo.SetStatusAndRoleForUser(userView);
            return res;
        }
    }
}
