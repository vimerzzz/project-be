﻿using Project.Repositories;
using Project.ViewModels;
using System;
using System.Collections.Generic;

namespace Project.Services
{
    public interface IDanhGiaShopService
    {
        public ResponseWithPaginationView GetAllDanhGia(DanhGiaShopParamView danhGiaShopParamView);
        public ResponsePostView CreateDanhGia(DanhGiaShopView danhGiaShopView);
        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView);
        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView);
        public List<DanhGiaShopHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId);
    }

    public class DanhGiaShopService : IDanhGiaShopService
    {
        private readonly IDanhGiaShopRepository _repo;

        public DanhGiaShopService(IDanhGiaShopRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateDanhGia(DanhGiaShopView danhGiaShopView)
        {
            var res = _repo.CreateDanhGia(danhGiaShopView);
            return res;
        }

        public ResponsePostView CreateDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView)
        {
            var res = _repo.CreateDanhGiaHuuIch(danhGiaShopHuuIchView);
            return res;
        }

        public ResponsePostView DeleteDanhGiaHuuIch(DanhGiaShopHuuIchView danhGiaShopHuuIchView)
        {
            var res = _repo.DeleteDanhGiaHuuIch(danhGiaShopHuuIchView);
            return res;
        }

        public ResponseWithPaginationView GetAllDanhGia(DanhGiaShopParamView danhGiaShopParamView)
        {
            var res = _repo.GetAllDanhGia(danhGiaShopParamView);
            return res;
        }

        public List<DanhGiaShopHuuIchView> GetDanhGiaHuuIchByUserId(Guid userId)
        {
            var res = _repo.GetDanhGiaHuuIchByUserId(userId);
            return res;
        }
    }
}
