﻿using Project.Repositories;
using Project.ViewModels;
using System;

namespace Project.Services
{
    public interface ISanPhamService
    {
        public ResponseWithPaginationView GetAllSanPhamByShopId(SanPhamParamView sanPhamParamView);
        public SanPhamView GetSanPhamById(SanPhamView sanPhamView);
        public ResponsePostView CreateSanPham(SanPhamView sanPhamView);
        public ResponsePostView UpdateSanPham(SanPhamView sanPhamView);
        public ResponsePostView DeleteSanPham(SanPhamView sanPhamView);
        public ResponseWithPaginationView GetNewestAllSanPhamWithHinhAnhByShopId(SanPhamParamView sanPhamParamView);
        public ResponseWithPaginationView GetAllSanPham(SanPhamParamView sanPhamParamView);
        public SanPhamWithHinhAnhView GetSanPhamWithHinhAnhByName(string tenShop, string tenSanPham);
    }

    public class SanPhamService : ISanPhamService
    {
        private readonly ISanPhamRepository _repo;

        public SanPhamService(ISanPhamRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateSanPham(SanPhamView sanPhamView)
        {
            var res = _repo.CreateSanPham(sanPhamView);
            return res;
        }

        public ResponsePostView DeleteSanPham(SanPhamView sanPhamView)
        {
            var res = _repo.DeleteSanPham(sanPhamView);
            return res;
        }

        public ResponseWithPaginationView GetAllSanPhamByShopId(SanPhamParamView sanPhamParamView)
        {
            var res = _repo.GetAllSanPhamByShopId(sanPhamParamView);
            return res;
        }

        public ResponseWithPaginationView GetNewestAllSanPhamWithHinhAnhByShopId(SanPhamParamView sanPhamParamView)
        {
            var res = _repo.GetNewestAllSanPhamWithHinhAnhByShopId(sanPhamParamView);
            return res;
        }

        public ResponseWithPaginationView GetAllSanPham(SanPhamParamView sanPhamParamView)
        {
            var res = _repo.GetAllSanPham(sanPhamParamView);
            return res;
        }

        public SanPhamView GetSanPhamById(SanPhamView sanPhamView)
        {
            var res = _repo.GetSanPhamById(sanPhamView);
            return res;
        }

        public SanPhamWithHinhAnhView GetSanPhamWithHinhAnhByName(string tenShop, string tenSanPham)
        {
            var res = _repo.GetSanPhamWithHinhAnhByName(tenShop, tenSanPham);
            return res;
        }

        public ResponsePostView UpdateSanPham(SanPhamView sanPhamView)
        {
            var res = _repo.UpdateSanPham(sanPhamView);
            return res;
        }
    }
}
