﻿using Project.Repositories;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface IRoleService
    {
        public List<RoleView> GetAllRole();
        public ResponsePostView CreateRole(RoleView roleView);
        public ResponsePostView UpdateRole(RoleView roleView);
        public ResponsePostView DeleteRole(Guid id);
    }

    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _repo;

        public RoleService(IRoleRepository repo)
        {
            _repo = repo;
        }

        public List<RoleView> GetAllRole()
        {
            var res = _repo.GetAllRole();
            return res;
        }
        public ResponsePostView CreateRole(RoleView roleView)
        {
            var res = _repo.CreateRole(roleView);
            return res;
        }
        public ResponsePostView UpdateRole(RoleView roleView)
        {
            var res = _repo.UpdateRole(roleView);
            return res;
        }
        public ResponsePostView DeleteRole(Guid id)
        {
            var res = _repo.DeleteRole(id);
            return res;
        }
    }
}
