﻿using Project.Repositories;
using Project.ViewModels;

namespace Project.Services
{
    public interface ISoTheoDoiService
    {
        public ResponseWithPaginationView GetAllSoTheoDoi(SoTheoDoiParamView soTheoDoiParamView);
        public ResponsePostView CreateSoTheoDoi(SoTheoDoiView soTheoDoiView);
        public ResponsePostView DeleteSoTheoDoi(SoTheoDoiView soTheoDoiView);
    }

    public class SoTheoDoiService : ISoTheoDoiService
    {
        private readonly ISoTheoDoiRepository _repo;

        public SoTheoDoiService(ISoTheoDoiRepository repo)
        {
            _repo = repo;
        }

        public ResponsePostView CreateSoTheoDoi(SoTheoDoiView soTheoDoiView)
        {
            var res = _repo.CreateSoTheoDoi(soTheoDoiView);
            return res;
        }

        public ResponsePostView DeleteSoTheoDoi(SoTheoDoiView soTheoDoiView)
        {
            var res = _repo.DeleteSoTheoDoi(soTheoDoiView);
            return res;
        }

        public ResponseWithPaginationView GetAllSoTheoDoi(SoTheoDoiParamView soTheoDoiParamView)
        {
            var res = _repo.GetAllSoTheoDoi(soTheoDoiParamView);
            return res;
        }
    }
}
