﻿using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.Converters;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface ISanPhamDauGiaService
    {
        public ResponseWithPaginationView GetAllSanPhamDauGiaByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView);
        public SanPhamDauGiaView GetSanPhamDauGiaById(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView CreateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView UpdateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public Task<ResponsePostView> UpdateTinhHinhDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public Task<ResponsePostView> UpdateTinhTrangDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public Task<ResponsePostView> YeuCauKetThucDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView CancelSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponsePostView DeleteSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView);
        public ResponseWithPaginationView GetNewestAllSanPhamDauGiaWithHinhAnhByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView);
        public ResponseWithPaginationView GetAllSanPhamDauGia(SanPhamDauGiaParamView sanPhamDauGiaParamView);
        public SanPhamDauGiaWithHinhAnhView GetSanPhamDauGiaWithHinhAnhByName(string tenShop, string tenSanPham);
        public Task CheckStatusDauGia();
        public List<TinhTrangDauGiaView> GetAllTinhTrangDauGia();
    }

    public class SanPhamDauGiaService : ISanPhamDauGiaService
    {
        private readonly ISanPhamDauGiaRepository _repo;
        private readonly IUserRepository _userRepo;
        private readonly IShopRepository _shopRepo;
        private readonly ITroChuyenDauGiaRepository _chatRepo;
        private readonly IThongBaoRepository _thongBaoRepo;
        private readonly IHubContext<SanPhamDauGiaHub> _hub;
        private readonly IHubContext<ThongBaoHub> _thongBaoHub;
        private readonly IntegerToGuid converter = new IntegerToGuid();

        public SanPhamDauGiaService(ISanPhamDauGiaRepository repo, IThongBaoRepository thongBaoRepo, IHubContext<ThongBaoHub> thongBaoHub, IUserRepository userRepo, IShopRepository shopRepo, IHubContext<SanPhamDauGiaHub> hub, ITroChuyenDauGiaRepository chatRepo)
        {
            _repo = repo;
            _userRepo = userRepo;
            _shopRepo = shopRepo;
            _hub = hub;
            _thongBaoHub = thongBaoHub;
            _chatRepo = chatRepo;
            _thongBaoRepo = thongBaoRepo;
        }

        public ResponsePostView CancelSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.CancelSanPhamDauGia(sanPhamDauGiaView);
            return res;
        }

        public async Task CheckStatusDauGia()
        {
            var timeNow = DateTime.Now;
            var sanPhamDauGiaView = new SanPhamDauGiaView();
            var param = converter.IntToGuid(1);
            var listSanPhamDauGia = _repo.GetAllSanPhamDauGiaToCheckStatus(param);
            var timeSpan = new TimeSpan(3, 0, 0, 0);
            if (listSanPhamDauGia != null)
            {
                foreach (var sanPhamDauGia in listSanPhamDauGia)
                {
                    if (sanPhamDauGia.CreatedAt + timeSpan <= timeNow)
                    {
                        sanPhamDauGiaView.Id = sanPhamDauGia.Id;
                        sanPhamDauGiaView.ShopId = sanPhamDauGia.ShopId;
                        sanPhamDauGiaView.TinhTrangDauGiaId = converter.IntToGuid(3);
                        var res = _repo.UpdateTinhTrangDauGia(ref sanPhamDauGiaView);
                        if (res.StatusCode == 200)
                        {
                            await _hub.Clients.All.SendAsync("sanPhamDauGia", sanPhamDauGiaView);
                        }
                    }
                }
            }
            param = converter.IntToGuid(4);
            listSanPhamDauGia = _repo.GetAllSanPhamDauGiaToCheckStatus(param);
            if (listSanPhamDauGia != null)
            {
                foreach (var sanPhamDauGia in listSanPhamDauGia)
                {
                    if (sanPhamDauGia.ThoiGianBatDau <= timeNow)
                    {
                        sanPhamDauGiaView.Id = sanPhamDauGia.Id;
                        sanPhamDauGiaView.ShopId = sanPhamDauGia.ShopId;
                        sanPhamDauGiaView.TinhTrangDauGiaId = converter.IntToGuid(5);
                        var res = _repo.UpdateTinhTrangDauGia(ref sanPhamDauGiaView);
                        if (res.StatusCode == 200)
                        {
                            await _hub.Clients.All.SendAsync("sanPhamDauGia", sanPhamDauGiaView);
                        }
                        var troChuyenDauGiaView = new TroChuyenDauGiaView
                        {
                            IsShopOwner = false,
                            IsSystem = true,
                            SanPhamDauGiaId = sanPhamDauGia.Id,
                            NoiDung = "Phiên đấu giá đã bắt đầu!"
                        };
                        var res2 = _chatRepo.CreateTroChuyenDauGia(ref troChuyenDauGiaView);
                        if (res2.StatusCode == 200)
                        {
                            await _hub.Clients.All.SendAsync("dauGiaChat", troChuyenDauGiaView);
                        }
                    }
                }
            }
            param = converter.IntToGuid(5);
            listSanPhamDauGia = _repo.GetAllSanPhamDauGiaToCheckStatus(param);
            if (listSanPhamDauGia != null)
            {
                foreach (var sanPhamDauGia in listSanPhamDauGia)
                {
                    if (sanPhamDauGia.ThoiGianKetThuc <= timeNow)
                    {
                        sanPhamDauGiaView.Id = sanPhamDauGia.Id;
                        sanPhamDauGiaView.ShopId = sanPhamDauGia.ShopId;
                        if (sanPhamDauGia.UserId != null)
                        {
                            sanPhamDauGiaView.TinhTrangDauGiaId = converter.IntToGuid(6);
                        }
                        else
                        {
                            sanPhamDauGiaView.TinhTrangDauGiaId = converter.IntToGuid(7);
                        }
                        var res = _repo.UpdateTinhTrangDauGia(ref sanPhamDauGiaView);
                        if (res.StatusCode == 200)
                        {
                            await _hub.Clients.All.SendAsync("sanPhamDauGia", sanPhamDauGiaView);
                        }
                        var noiDung = "";
                        if (sanPhamDauGia.UserId != null)
                        {
                            var name = "";
                            if ((sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho.Trim() != "") &&
                                ((sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem.Trim() != "") ||
                                (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten.Trim() != "")))
                            {
                                if (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho.Trim() != "")
                                {
                                    name += sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho + " ";
                                }
                                if (sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem.Trim() != "")
                                {
                                    name += sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem + " ";
                                }
                                if (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten.Trim() != "")
                                {
                                    name += sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten + " ";
                                }
                            }
                            else
                            {
                                name = sanPhamDauGiaView.NguoiRaGia.UserName;
                            }
                            noiDung = "Người thắng cuộc là " + name + ".";
                        }
                        else
                        {
                            noiDung = "Không có người thắng cuộc nào trong phiên đấu giá này.";
                        }
                        var troChuyenDauGiaView = new TroChuyenDauGiaView
                        {
                            IsShopOwner = false,
                            IsSystem = true,
                            SanPhamDauGiaId = sanPhamDauGia.Id,
                            NoiDung = "Phiên đấu giá đã kết thúc! " + noiDung
                        };
                        var res2 = _chatRepo.CreateTroChuyenDauGia(ref troChuyenDauGiaView);
                        if (res2.StatusCode == 200)
                        {
                            await _hub.Clients.All.SendAsync("dauGiaChat", troChuyenDauGiaView);
                        }
                    }
                }
            }
            param = converter.IntToGuid(6);
            listSanPhamDauGia = _repo.GetAllSanPhamDauGiaToCheckStatus(param);
            if (listSanPhamDauGia != null)
            {
                foreach (var sanPhamDauGia in listSanPhamDauGia)
                {
                    if (sanPhamDauGia.UpdatedAt + timeSpan <= timeNow)
                    {
                        sanPhamDauGiaView.Id = sanPhamDauGia.Id;
                        sanPhamDauGiaView.ShopId = sanPhamDauGia.ShopId;
                        sanPhamDauGiaView.TinhTrangDauGiaId = converter.IntToGuid(7);
                        var res = _repo.UpdateTinhTrangDauGia(ref sanPhamDauGiaView);
                        if (res.StatusCode == 200)
                        {
                            await _hub.Clients.All.SendAsync("sanPhamDauGia", sanPhamDauGiaView);
                        }
                    }
                }
            }
        }

        public ResponsePostView CreateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.CreateSanPhamDauGia(sanPhamDauGiaView);
            return res;
        }

        public ResponsePostView DeleteSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.DeleteSanPhamDauGia(sanPhamDauGiaView);
            return res;
        }

        public ResponseWithPaginationView GetAllSanPhamDauGia(SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            var res = _repo.GetAllSanPhamDauGia(sanPhamDauGiaParamView);
            return res;
        }

        public ResponseWithPaginationView GetAllSanPhamDauGiaByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            var res = _repo.GetAllSanPhamDauGiaByShopId(sanPhamDauGiaParamView);
            return res;
        }

        public List<TinhTrangDauGiaView> GetAllTinhTrangDauGia()
        {
            var res = _repo.GetAllTinhTrangDauGia();
            return res;
        }

        public ResponseWithPaginationView GetNewestAllSanPhamDauGiaWithHinhAnhByShopId(SanPhamDauGiaParamView sanPhamDauGiaParamView)
        {
            var res = _repo.GetNewestAllSanPhamDauGiaWithHinhAnhByShopId(sanPhamDauGiaParamView);
            return res;
        }

        public SanPhamDauGiaView GetSanPhamDauGiaById(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.GetSanPhamDauGiaById(sanPhamDauGiaView);
            return res;
        }

        public SanPhamDauGiaWithHinhAnhView GetSanPhamDauGiaWithHinhAnhByName(string tenShop, string tenSanPham)
        {
            var res = _repo.GetSanPhamDauGiaWithHinhAnhByName(tenShop, tenSanPham);
            return res;
        }

        public ResponsePostView UpdateSanPhamDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.UpdateSanPhamDauGia(sanPhamDauGiaView);
            return res;
        }

        public async Task<ResponsePostView> UpdateTinhHinhDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.UpdateTinhHinhDauGia(ref sanPhamDauGiaView);
            if (res.StatusCode == 200)
            {
                var troChuyenDauGiaView = new TroChuyenDauGiaView
                {
                    IsShopOwner = false,
                    IsSystem = true,
                    SanPhamDauGiaId = sanPhamDauGiaView.Id
                };
                var name = "";
                if ((sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho.Trim() != "") &&
                    ((sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem.Trim() != "") ||
                    (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten.Trim() != "")))
                {
                    if (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho.Trim() != "")
                    {
                        name += sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho + " ";
                    }
                    if (sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem.Trim() != "")
                    {
                        name += sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem + " ";
                    }
                    if (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten.Trim() != "")
                    {
                        name += sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten + " ";
                    }
                }
                else
                {
                    name = sanPhamDauGiaView.NguoiRaGia.UserName;
                }
                troChuyenDauGiaView.NoiDung = name + " đã ra giá " + sanPhamDauGiaView.GiaHienTai.ToString("N0", CultureInfo.InvariantCulture) + " ₫";
                res = _chatRepo.CreateTroChuyenDauGia(ref troChuyenDauGiaView);
                if (res.StatusCode == 200)
                {
                    await _hub.Clients.All.SendAsync("dauGiaChat", troChuyenDauGiaView);
                    await _hub.Clients.All.SendAsync("sanPhamDauGia", sanPhamDauGiaView);
                }
            }
            return res;
        }

        public async Task<ResponsePostView> UpdateTinhTrangDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.UpdateTinhTrangDauGia(ref sanPhamDauGiaView);
            if (res.StatusCode == 200)
            {
                await _hub.Clients.All.SendAsync("sanPhamDauGia", sanPhamDauGiaView);
            }
            var noiDung = "";
            if (sanPhamDauGiaView.TinhTrangDauGiaId == converter.IntToGuid(6))
            {
                var name = "";
                if ((sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho.Trim() != "") &&
                    ((sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem.Trim() != "") ||
                    (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten.Trim() != "")))
                {
                    if (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho.Trim() != "")
                    {
                        name += sanPhamDauGiaView.NguoiRaGia.UserInfo.Ho + " ";
                    }
                    if (sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem.Trim() != "")
                    {
                        name += sanPhamDauGiaView.NguoiRaGia.UserInfo.TenDem + " ";
                    }
                    if (sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != null && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten != "" && sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten.Trim() != "")
                    {
                        name += sanPhamDauGiaView.NguoiRaGia.UserInfo.Ten + " ";
                    }
                }
                else
                {
                    name = sanPhamDauGiaView.NguoiRaGia.UserName;
                }
                noiDung = "Người thắng cuộc là " + name + ".";
            }
            else if(sanPhamDauGiaView.TinhTrangDauGiaId == converter.IntToGuid(7))
            {
                noiDung = "Không có người thắng cuộc nào trong phiên đấu giá này.";
            }
            var troChuyenDauGiaView = new TroChuyenDauGiaView
            {
                IsShopOwner = false,
                IsSystem = true,
                SanPhamDauGiaId = sanPhamDauGiaView.Id,
                NoiDung = "Phiên đấu giá đã kết thúc! " + noiDung
            };
            var res2 = _chatRepo.CreateTroChuyenDauGia(ref troChuyenDauGiaView);
            if (res2.StatusCode == 200)
            {
                await _hub.Clients.All.SendAsync("dauGiaChat", troChuyenDauGiaView);
            }
            return res;
        }

        public async Task<ResponsePostView> YeuCauKetThucDauGia(SanPhamDauGiaView sanPhamDauGiaView)
        {
            var res = _repo.YeuCauKetThucDauGia(sanPhamDauGiaView);
            if (res.StatusCode == 200)
            {
                await _hub.Clients.All.SendAsync("sanPhamDauGia", sanPhamDauGiaView);
                var shop = _shopRepo.GetShopByShopId(sanPhamDauGiaView.ShopId);
                if (shop != null)
                {
                    var userParamView = new UserParamView
                    {
                        RoleId = converter.IntToGuid(4)
                    };
                    var listTmp = _userRepo.GetAllUser(userParamView);
                    var listUser = (List<UserView>)listTmp.GetType().GetProperty("Data").GetValue(listTmp);
                    if (listUser != null)
                    {
                        var thongBao = new List<ThongBaoView>();
                        foreach (var user in listUser)
                        {
                            var tmp = new ThongBaoView
                            {
                                TieuDeThongBao = "Yêu cầu kết thúc đấu giá",
                                NoiDungThongBao = "<strong>" + shop.TenShop + "</strong> đã gửi yêu cầu kết thúc đấu giá sản phẩm <strong>" + sanPhamDauGiaView.TenSanPham + "</strong>. <a name=\"notify-link\" href=\"/shop/auction?shopName=" + shop.TenShop + "&itemName=" + sanPhamDauGiaView.TenSanPham + "\">Chi tiết sản phẩm xem tại đây</a>.",
                                UserId = user.Id
                            };
                            thongBao.Add(tmp);
                        }
                        var ress = _thongBaoRepo.CreateThongBao(ref thongBao);
                        if (ress.StatusCode == 200)
                        {
                            await _thongBaoHub.Clients.All.SendAsync("thongBao", thongBao);
                        }
                    }
                }
            }
            return res;
        }
    }
}
