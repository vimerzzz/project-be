﻿using Microsoft.AspNetCore.SignalR;
using Project.Repositories;
using Project.Utilities.Constants;
using Project.Utilities.Converters;
using Project.Utilities.HubConfigs;
using Project.ViewModels;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace Project.Services
{
    public interface IDonHangService
    {
        public ResponseWithPaginationView GetAllDonHang(DonHangParamView donHangParamView);
        public DonHangView GetDonHangByMaDonHang(DonHangView donHangView);
        public Task<ResponsePostView> CreateDonHang(DonHangView donHangView);
        public ResponsePostView UpdateDonHang(DonHangView donHangView);
        public Task<ResponsePostView> UpdateTinhTrangDonHang(DonHangView donHangView);
    }

    public class DonHangService : IDonHangService
    {
        private readonly IDonHangRepository _repo;
        private readonly IThongBaoRepository _thongBaoRepo;
        private readonly IHubContext<ThongBaoHub> _hub;
        private readonly IntegerToGuid converter = new IntegerToGuid();

        public DonHangService(IDonHangRepository repo, IThongBaoRepository thongBaoRepo, IHubContext<ThongBaoHub> hub)
        {
            _repo = repo;
            _thongBaoRepo = thongBaoRepo;
            _hub = hub;
        }

        public async Task<ResponsePostView> CreateDonHang(DonHangView donHangView)
        {
            var res = _repo.CreateDonHang(ref donHangView);
            if (res.StatusCode == 200)
            {
                var thongBao = new List<ThongBaoView>();
                var tmp = new ThongBaoView
                {
                    TieuDeThongBao = "Đặt hàng thành công",
                    NoiDungThongBao = "Cảm ơn bạn đã đặt hàng! Đơn hàng số " + donHangView.MaDonHang.ToString() + " bao gồm các sản phẩm: ",
                    UserId = donHangView.UserId
                };
                foreach(var item in donHangView.DonHangSanPham)
                {
                    tmp.NoiDungThongBao += "<strong>" + item.SoLuong.ToString() + " x " + item.TenSanPham + "</strong>";
                    if(donHangView.DonHangSanPham.IndexOf(item) == donHangView.DonHangSanPham.Count - 1)
                    {
                        tmp.NoiDungThongBao += " đang chờ được tiếp nhận và xử lý. Chi tiết đơn hàng xem tại <a name=\"notify-link\" href=\"/information/order/detail?maDonHang=" + donHangView.MaDonHang.ToString() + "\">đây</a>";
                    }
                    else
                    {
                        tmp.NoiDungThongBao += "<strong>, </strong>";
                    }
                }
                thongBao.Add(tmp);
                var ress = _thongBaoRepo.CreateThongBao(ref thongBao);
                if (ress.StatusCode == 200)
                {
                    await _hub.Clients.All.SendAsync("thongBao", thongBao);
                }
            }
            return res;
        }

        public ResponseWithPaginationView GetAllDonHang(DonHangParamView donHangParamView)
        {
            var res = _repo.GetAllDonHang(donHangParamView);
            return res;
        }

        public DonHangView GetDonHangByMaDonHang(DonHangView donHangView)
        {
            var res = _repo.GetDonHangByMaDonHang(donHangView);
            return res;
        }

        public ResponsePostView UpdateDonHang(DonHangView donHangView)
        {
            var res = _repo.UpdateDonHang(donHangView);
            return res;
        }

        public async Task<ResponsePostView> UpdateTinhTrangDonHang(DonHangView donHangView)
        {
            var res = _repo.UpdateTinhTrangDonHang(donHangView);
            if (res.StatusCode == 200)
            {
                var thongBao = new List<ThongBaoView>();
                var tmp = new ThongBaoView
                {
                    TieuDeThongBao = "",
                    NoiDungThongBao = "Đơn hàng số " + donHangView.MaDonHang.ToString() + " bao gồm các sản phẩm: ",
                    UserId = donHangView.UserId
                };
                foreach (var item in donHangView.DonHangSanPham)
                {
                    tmp.NoiDungThongBao += "<strong>" + item.SoLuong.ToString() + " x " + item.TenSanPham + "</strong>";
                    if (donHangView.DonHangSanPham.IndexOf(item) == donHangView.DonHangSanPham.Count - 1)
                    {
                        if (donHangView.TinhTrangDonHangId == converter.IntToGuid(2))
                        {
                            tmp.TieuDeThongBao = "Đơn hàng đã được tiếp nhận";
                            tmp.NoiDungThongBao += " đã được tiếp nhận và đang chờ giao hàng.";
                        }
                        else if (donHangView.TinhTrangDonHangId == converter.IntToGuid(3))
                        {
                            tmp.TieuDeThongBao = "Đơn hàng đang được giao";
                            tmp.NoiDungThongBao += " đang được vận chuyển và sẽ sớm tới tay bạn. Bạn vui lòng chuẩn bị <strong>" + donHangView.ThanhTien.ToString("N0", CultureInfo.InvariantCulture) + " ₫</strong> để thanh toán cho đơn hàng này.";
                        }
                        else if (donHangView.TinhTrangDonHangId == converter.IntToGuid(4))
                        {
                            tmp.TieuDeThongBao = "Đơn hàng hoàn tất";
                            tmp.NoiDungThongBao += " đã được giao và thanh toán thành công.";
                        }
                        else if (donHangView.TinhTrangDonHangId == converter.IntToGuid(5) || donHangView.TinhTrangDonHangId == converter.IntToGuid(6))
                        {
                            tmp.TieuDeThongBao = "Đơn hàng đã bị hủy";
                            tmp.NoiDungThongBao += " đã bị hủy.";
                            if(donHangView.LyDoHuy != null)
                            {
                                tmp.NoiDungThongBao += " Lý do hủy: " + donHangView.LyDoHuy + ".";
                            }
                        }
                        tmp.NoiDungThongBao += " Chi tiết đơn hàng xem tại <a name=\"notify-link\" href=\"/information/order/detail?maDonHang=" + donHangView.MaDonHang.ToString() + "\">đây</a>";
                    }
                    else
                    {
                        tmp.NoiDungThongBao += "<strong>, </strong>";
                    }
                }
                thongBao.Add(tmp);
                var ress = _thongBaoRepo.CreateThongBao(ref thongBao);
                if (ress.StatusCode == 200)
                {
                    await _hub.Clients.All.SendAsync("thongBao", thongBao);
                }
            }
            return res;
        }
    }
}
