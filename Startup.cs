using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Project.Models;
using Project.DataContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Project.Services;
using Project.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Http.Features;
using Hangfire;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using Project.Utilities.HubConfigs;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using Project.Utilities.Constants;

namespace Project
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDBContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<ApplicationDBContext>()
                .AddDefaultTokenProviders();
            services.AddControllers();
            services.AddCors();
            services.AddAutoMapper(typeof(ProjectMapperProfile).Assembly);
            services.Configure<FormOptions>(o => {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            services.AddSignalR();
            services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                options.OperationFilter<SecurityRequirementsOperationFilter>();
            });

            // Hangfire
            services.AddHangfire(configuration => configuration
               .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
               .UseSimpleAssemblyNameTypeSerializer()
               .UseRecommendedSerializerSettings()
               .UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"), new Hangfire.SqlServer.SqlServerStorageOptions
               {
                   CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                   SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                   QueuePollInterval = TimeSpan.Zero,
                   UseRecommendedIsolationLevel = true,
                   DisableGlobalLocks = true
               }));
            services.AddHangfireServer();

            // ===== Add Jwt Authentication ========
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                    .AddJwtBearer(cfg =>
                    {
                        cfg.RequireHttpsMetadata = false;
                        cfg.SaveToken = true;
                        cfg.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidIssuer = TokenProviderConstants.IssuerName,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SXkSqsKyNUyvGbnHs7ke2NCq8zQzNLW7mPmHbnZZ")),
                            ClockSkew = TimeSpan.Zero,
                            ValidateAudience = false
                        };
                    });
            services.AddAuthorization(cfg =>
            {
                cfg.AddPolicy("Admin", policy => policy.RequireClaim("type", "Admin"));
                cfg.AddPolicy("Customer", policy => policy.RequireClaim("type", "Customer"));
                cfg.AddPolicy("Deliver", policy => policy.RequireClaim("type", "Deliver"));
                cfg.AddPolicy("Expert", policy => policy.RequireClaim("type", "Expert"));
            });

            //Register services
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IUserInfoService, UserInfoService>();
            services.AddTransient<ILogService, LogService>();
            services.AddTransient<ISoDiaChiService, SoDiaChiService>();
            services.AddTransient<ILoaiSanPhamService, LoaiSanPhamService>();
            services.AddTransient<ILoaiSanPhamService, LoaiSanPhamService>();
            services.AddTransient<IThongBaoService, ThongBaoService>();
            services.AddTransient<IShopService, ShopService>();
            services.AddTransient<IValidateService, ValidateService>();
            services.AddTransient<ISanPhamService, SanPhamService>();
            services.AddTransient<IHinhAnhSanPhamService, HinhAnhSanPhamService>();
            services.AddTransient<ISoTheoDoiService, SoTheoDoiService>();
            services.AddTransient<IDanhGiaShopService, DanhGiaShopService>();
            services.AddTransient<IGioHangService, GioHangService>();
            services.AddTransient<IDanhGiaSanPhamService, DanhGiaSanPhamService>();
            services.AddTransient<INhanTinShopService, NhanTinShopService>();
            services.AddTransient<INhanTinUserService, NhanTinUserService>();
            services.AddTransient<IDonHangService, DonHangService>();
            services.AddTransient<ISanPhamDauGiaService, SanPhamDauGiaService>();
            services.AddTransient<IHinhAnhSanPhamDauGiaService, HinhAnhSanPhamDauGiaService>();
            services.AddTransient<ITroChuyenDauGiaService, TroChuyenDauGiaService>();
            services.AddTransient<IBaoCaoViPhamService, BaoCaoViPhamService>();

            //Register repositories
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IUserInfoRepository, UserInfoRepository>();
            services.AddTransient<ISoDiaChiRepository, SoDiaChiRepository>();
            services.AddTransient<ILoaiSanPhamRepository, LoaiSanPhamRepository>();
            services.AddTransient<IThongBaoRepository, ThongBaoRepository>();
            services.AddTransient<IShopRepository, ShopRepository>();
            services.AddTransient<ISanPhamRepository, SanPhamRepository>();
            services.AddTransient<IHinhAnhSanPhamRepository, HinhAnhSanPhamRepository>();
            services.AddTransient<ISoTheoDoiRepository, SoTheoDoiRepository>();
            services.AddTransient<IDanhGiaShopRepository, DanhGiaShopRepository>();
            services.AddTransient<IGioHangRepository, GioHangRepository>();
            services.AddTransient<IDanhGiaSanPhamRepository, DanhGiaSanPhamRepository>();
            services.AddTransient<INhanTinShopRepository, NhanTinShopRepository>();
            services.AddTransient<INhanTinUserRepository, NhanTinUserRepository>();
            services.AddTransient<IDonHangRepository, DonHangRepository>();
            services.AddTransient<ISanPhamDauGiaRepository, SanPhamDauGiaRepository>();
            services.AddTransient<IHinhAnhSanPhamDauGiaRepository, HinhAnhSanPhamDauGiaRepository>();
            services.AddTransient<ITroChuyenDauGiaRepository, TroChuyenDauGiaRepository>();
            services.AddTransient<IBaoCaoViPhamRepository, BaoCaoViPhamRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IThongBaoService thongBaoService, ISanPhamDauGiaService dauGiaService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                });
            }
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseWebSockets(new WebSocketOptions
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
            });

            // add Real-time function
            app.UseHangfireDashboard();
            RecurringJob.RemoveIfExists("checkStatusMaGiamGia");
            RecurringJob.RemoveIfExists("checkStatusThongBao");
            RecurringJob.RemoveIfExists("checkStatusDauGia");
            RecurringJob.AddOrUpdate("checkStatusThongBao", () => thongBaoService.CheckStatusThongBao(), Cron.Minutely());
            RecurringJob.AddOrUpdate("checkStatusDauGia", () => dauGiaService.CheckStatusDauGia(), Cron.Minutely());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
                endpoints.MapHub<ThongBaoHub>("/hub/thongBao");
                endpoints.MapHub<SanPhamDauGiaHub>("/hub/sanPhamDauGia");
                endpoints.MapHub<ChatHub>("/hub/chat");
                endpoints.MapHub<BaoCaoHub>("/hub/baoCao");
            });
        }
    }
}
